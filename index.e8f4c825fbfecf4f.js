! function() {
    return function e(t, i, n) {
        function o(a, s) {
            if (!i[a]) {
                if (!t[a]) {
                    var l = "function" == typeof require && require;
                    if (!s && l) return l(a, !0);
                    if (r) return r(a, !0);
                    var c = new Error("Cannot find module '" + a + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var u = i[a] = {
                    exports: {}
                };
                t[a][0].call(u.exports, function(e) {
                    return o(t[a][1][e] || e)
                }, u, u.exports, e, t, i, n)
            }
            return i[a].exports
        }
        for (var r = "function" == typeof require && require, a = 0; a < n.length; a++) o(n[a]);
        return o
    }
}()({
    1: [function(e, t, i) {
        t.exports = {
            version: "00.01.00.041",
            backendUrl: 'http://DESKTOP-U3C2FFN/ccnav_backend',
            backendEndpoints: {
                addClassifier: "/api/datasource/classifier/{modelName}",
                updateClassifier: "/api/datasource/classifier/{modelName}/{elementId}",
                getClassifier: "/api/datasource/classifier/{modelName}/{elementId}",
                getClassifierAsset: "/api/datasource/classifier/{modelName}/{elementId}/asset",
                removeClassifier: "/api/datasource/classifier/{elementId}",
                getClassifiers: "/api/datasource/classifiers/{modelName}",
                getTypes: "/api/datasource/types",
                addAsset: "/api/datasource/asset",
                getAsset: "/api/datasource/asset/{id}",
                getAssetByCode: "/api/datasource/assetByCode/{code}",
                getAssets: "/api/datasource/assets",
                addOrUpdateRelation: "/api/datasource/relation/{elementId}/{assetId}",
                getBimProperties: "/api/tileDataAccess/queryProperties/{modelName}/{elementId}"
            },
            toolbar: {
                id: "Bimium.Toolbar",
                class: "bim-toolzone",
                zones: [{
                    id: "Bim.Bottom.Zone",
                    class: "bim-bottom-zone",
                    include: !0,
                    widgets: []
                }, {
                    id: "Bim.Top.Right.WidgetZone",
                    class: "bim-toolzone-top-right-content",
                    type: "TopRightZone",
                    include: !0,
                    widgets: [{
                        id: "TopRightPanelTabs",
                        visible: !0,
                        autoSizedTabContent: !0,
                        tabs: [{
                            id: "PropertyGridTab",
                            include: !0,
                            order: "4",
                            button: {
                                class: "bim-tab-button",
                                title: "Property Grid",
                                icon: "bim-icon-list"
                            },
                            content: {
                                widget: "Vivo.VivoPropertyGridWidget",
                                wrapper: "LabeledPanel",
                                label: "Element Properties"
                            }
                        }, {
                            id: "'ClassificationSelectionTab",
                            include: !0,
                            order: "5",
                            button: {
                                class: "bim-tab-button",
                                title: "Classification Display",
                                icon: "classificationSelectionTab-icon"
                            },
                            content: {
                                widget: "Vivo.ClassificationDisplayWidget",
                                wrapper: "LabeledPanel",
                                label: "Classification Display"
                            }
                        }],
                        order: "1",
                        include: !0
                    }, {
                        id: "BimGeocoder",
                        widget: "Cesium.Geocoder",
                        include: !1
                    }]
                }, {
                    id: "Bim.CornerZone",
                    type: "CornerZone",
                    include: !0
                }, {
                    id: "Bim.HorizontalDropdownButtonsSettings",
                    include: !0
                }, {
                    id: "Bim.HorizontalDropdownButtons",
                    class: "bim-toolbar bim-toolbar-horizontal",
                    include: !0,
                    buttons: [{
                        id: "ViewLinkTrigger",
                        include: !1
                    }, {
                        id: "VRButton",
                        include: !1
                    }, {
                        id: "ClassifierSelectionTrigger",
                        include: !1
                    }]
                }, {
                    id: "Bim.VerticalToolButtons",
                    class: "bim-toolbar bim-toolbar-vertical",
                    include: !0,
                    buttons: [{
                        id: "IsolateTool",
                        include: !1
                    }, {
                        id: "DisableCategoryTool",
                        include: !1
                    }, {
                        id: "ElementSelectionTool",
                        tool: "Vivo.SelectionTool",
                        class: "bim-toolbar-tool",
                        title: "Element Selection",
                        icon: "bim-icon-cursor",
                        include: !0,
                        order: "1"
                    }, {
                        id: "AddClassificationTool",
                        tool: "Vivo.AddClassificationTool",
                        class: "bim-toolbar-tool",
                        title: "Add Classification",
                        icon: "addClassification-tool-icon",
                        include: !0,
                        order: "2"
                    }, {
                        id: "BrowsePhotoTool",
                        tool: "Vivo.BrowsePhotoTool",
                        class: "bim-toolbar-tool",
                        title: "Photo Navigation",
                        icon: "photoNavigation-tool-icon",
                        include: !0,
                        order: "3"
                    }, {
                        id: "MeasureToolsTrigger",
                        order: "4"
                    }, {
                        id: "FitViewTool",
                        order: "5"
                    }, {
                        id: "WindowAreaTool",
                        order: "6"
                    }, {
                        id: "RotateTool",
                        order: "7"
                    }, {
                        id: "PanTool",
                        order: "8"
                    }, {
                        id: "WalkTool",
                        order: "9"
                    }, {
                        id: "UndoViewTool",
                        order: "10"
                    }, {
                        id: "RedoViewTool",
                        order: "11"
                    }, {
                        id: "GroundFittingTool",
                        order: "12"
                    }]
                }, {
                    id: "Bim.DropdownContent",
                    class: "bim-toolzone-content-container",
                    type: "DropdownContentZone",
                    include: !0,
                    widgets: []
                }]
            },
            tools: [],
            widgets: []
        }
    }, {}],
    2: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        })
    }, {}],
    3: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        })
    }, {}],
    4: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        })
    }, {}],
    5: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../config/config.json"),
            o = function() {
                function e() {}
                return e.GetConfig = function() {
                    return n
                }, e
            }();
        i.ConfigManager = o
    }, {
        "./../config/config.json": 1
    }],
    6: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("jquery"),
            o = function() {
                function e() {}
                return e.createElementTitle = function(t, i, n, o) {
                    var r = document.createElement("div");
                    return r.appendChild(e.createButton(n, "Save")), r.appendChild(e.createButton(o, "Add Category")), r
                }, e.createPropertyDisplay = function(t, i, o, r) {
                    var a = Bentley.TemplateUtility.createElement("div", "bim-properties");
                    Bentley.Utils.makeArray(t).forEach(function(t) {
                        if (Cesium.defined(t.value) && Cesium.defined(t.label)) {
                            var i = Bentley.TemplateUtility.createElement("div", "visops-property-div");
                            e.createInputsForProperty(i, t, r), a.appendChild(i)
                        }
                    });
                    var s = n(".bim-widget-content .bim-groupbox").find(n(".bim-groupbox-header")).last(),
                        l = e.createButton(i, "-");
                    n(l).appendTo(s);
                    var c = Bentley.TemplateUtility.createElement("div", "visops-button-div");
                    return c.appendChild(e.createButton(o, "Add Property")), a.appendChild(c), a
                }, e.createButton = function(e, t) {
                    var i = Bentley.TemplateUtility.createElement("button", "visops-property-button");
                    return i.innerText = t, n(i).click(e), i
                }, e.createInputsForProperty = function(t, i, n) {
                    var o = Bentley.TemplateUtility.createElement("input", "visops-property-input");
                    o.value = i.label;
                    var r = Bentley.TemplateUtility.createElement("input", "visops-property-input");
                    r.value = i.value, t.appendChild(o), t.appendChild(r), t.appendChild(e.createButton(n, "-"))
                }, e
            }();
        i.VisOpsToolbarTemplateFactory = o
    }, {
        jquery: 31
    }],
    7: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./AssetAssignationTool"),
            o = e("./ClassificationEntitiesManager"),
            r = e("./../Utils/HttpRequestHelper"),
            a = function() {
                return function(e, t) {
                    var i = this;
                    this.onPostInstall = function() {
                        r.HttpRequestHelper.IsBackendReachable() || (alert("ContextCapture Services: Server unreachable."), i.viewport.toolAdmin._primitiveTool.exitTool())
                    }, this.refreshUnit = function() {
                        void 0 !== i.minHeightText && (i.minHeightText.value = String(Bentley.Unit.convertUnits(i.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits()))), void 0 !== i.maxHeightText && (i.maxHeightText.value = String(Bentley.Unit.convertUnits(i.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())))
                    }, this.onModelMotion = function(e) {
                        return Bentley.InputEventModifiers.None === e.keyModifiers && (i.updateHeightInProgress ? i.updateClassificationHeight(e) : i.updateRubberBand(e), !0)
                    }, this.onCleanup = function(e) {
                        i.clearTemporaryPolygonCompletion(), null != i.classifPrimitive && i.viewport.scene.primitives.remove(i.classifPrimitive), null != i.polygonEntity && i.entities.remove(i.polygonEntity), i.eventHelper.removeAll(), i.dataSourceDisplay = i.dataSourceDisplay.destroy(), i.dataSourceCollection = i.dataSourceCollection.destroy(), i.pointPositions.length = 0, i.polygonFinished = !1, void 0 !== i.classificationContainer && i.viewport.BimAdmin.viewer._container.removeChild(i.classificationContainer), i.turnCameraOn && (i.viewport.turnCameraOn(Bentley.ViewToolSettings.walkCameraAngle * (Math.PI / 180)), i.turnCameraOn = !1)
                    }, this.onResetButtonUp = function(e) {
                        if (i.polygonFinished) i.viewport.toolAdmin._primitiveTool.exitTool();
                        else if (i.pointPositions.length > 0) {
                            i.pointPositions.pop();
                            var t = i.edgeEntities.pop();
                            void 0 !== t && i.entities.remove(t), i.tmpLineEntity && (i.entities.remove(i.tmpLineEntity), i.tmpLineEntity = void 0), i.updateRubberBand(e), null != i.finishMarkerEntitie && i.entities.remove(i.finishMarkerEntitie)
                        }
                    }, this.getCursor = function() {
                        return i.addPointCursor
                    }, this.onDataButtonDown = function(e) {
                        if (Bentley.InputEventModifiers.None !== e.keyModifiers) return !1;
                        var t = i.viewport.scene;
                        if (!t.pickPositionSupported) return !1;
                        if (i.polygonFinished) i.updateHeightInProgress && (i.updateHeightInProgress = !1, i.maxHeightEditButton.blur(), i.minHeightEditButton.blur());
                        else {
                            t.pickTranslucentDepth = !1;
                            var n = t.pickPosition(e.viewPoint);
                            if (void 0 === n) return !1;
                            if (!t.pick(e.viewPoint)) return !1;
                            var o = i.viewport.worldToNpc(n);
                            if (i.pointPositions.length > 2) {
                                var r = i.viewport.worldToNpc(i.pointPositions[0]);
                                if (o.isEqual(r, .01)) return i.finishPolygon(), !0
                            }
                            i.addPoint(n)
                        }
                        return !0
                    }, this.updateClassificationHeight = function(e) {
                        i.updateHeightInProgress = !0;
                        var t = e.point,
                            n = i.viewport.npcToWorld(new Cesium.Cartesian3(0, 0, 0)),
                            o = i.viewport.npcToWorld(new Cesium.Cartesian3(1, 1, 1)),
                            r = (i.cartesianToCartographic(n), i.cartesianToCartographic(o)),
                            a = i.cartesianToCartographic(i.pointPositions[0]);
                        a.height = i.editMinHeight ? i.maxHeight : i.minHeight;
                        var s = i.cartesianToCartographic(i.pointPositions[0]);
                        s.height = r.height;
                        var l = i.viewport.scene.globe.ellipsoid,
                            c = l.cartographicToCartesian(a),
                            u = l.cartographicToCartesian(s);
                        if (i.viewport.isCameraOn) {
                            var d = i.viewport.worldToNpc(e.point);
                            d.z = i.viewport.worldToNpc(c).z, t = i.viewport.npcToWorld(d)
                        }
                        var p = new Cesium.Cartesian3(0, 0, 0);
                        p = i.editMinHeight ? Cesium.Cartesian3.fromDifferenceOf(c, u) : Cesium.Cartesian3.fromDifferenceOf(u, c), Cesium.Cartesian3.normalize(p, p);
                        var h = new Cesium.Ray(c, p),
                            f = Cesium.Plane.fromPointNormal(t, i.viewport.scene.camera.up),
                            m = Cesium.IntersectionTests.rayPlane(h, f);
                        if (void 0 !== m) {
                            var v = i.cartesianToCartographic(m);
                            i.editMinHeight ? (i.minHeight = i.getMin(v.height, i.maxHeight), void 0 !== i.minHeightText && (i.minHeightText.value = String(Bentley.Unit.convertUnits(i.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())))) : (i.maxHeight = i.getMax(v.height, i.minHeight), void 0 !== i.maxHeightText && (i.maxHeightText.value = String(Bentley.Unit.convertUnits(i.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())))), i.createClassificationPrimitive()
                        }
                    }, this.updateRubberBand = function(e) {
                        var t = i.pointPositions.length;
                        if (i.polygonFinished || t < 1) return !1;
                        var n = i.viewport.scene;
                        if (!n.pickPositionSupported) return !1;
                        var o = n.pickPosition(e.viewPoint);
                        if (void 0 === o) return !1;
                        if (!n.pick(e.viewPoint)) return !1;
                        null != i.finishMarkerEntitie && i.entities.remove(i.finishMarkerEntitie);
                        var r = i.viewport.worldToNpc(o);
                        if (i.pointPositions.length > 2) {
                            var a = i.addPointCursor,
                                s = i.viewport.worldToNpc(i.pointPositions[0]);
                            r.isEqual(s, .01) && (a = i.closeShapeCursor, o = i.pointPositions[0]), i.viewport.toolAdmin.setViewCursor(a)
                        }
                        return i.tmpLineEntity || (i.tmpLineEntity = i.entities.add({
                            polyline: {
                                width: 2,
                                material: Cesium.Color.LIMEGREEN,
                                followSurface: !1,
                                depthFailMaterial: new Cesium.PolylineDashMaterialProperty({
                                    color: Cesium.Color.LIMEGREEN
                                })
                            }
                        })), i.tmpLineEntity.polyline.positions = [i.pointPositions[t - 1], o], !0
                    }, this.addPoint = function(e) {
                        var t = i.cartesianToCartographic(e);
                        0 === i.pointPositions.length ? (i.minHeight = t.height, i.maxHeight = t.height) : (i.minHeight = i.getMin(i.minHeight, t.height), i.maxHeight = i.getMax(i.maxHeight, t.height)), i.pointPositions.push(e);
                        var n = i.pointPositions.length - 1;
                        if (n > 0) {
                            var o = {
                                    polyline: {
                                        positions: [i.pointPositions[n - 1], i.pointPositions[n]],
                                        width: 2,
                                        material: Cesium.Color.STEELBLUE,
                                        depthFailMaterial: new Cesium.PolylineDashMaterialProperty({
                                            color: Cesium.Color.STEELBLUE
                                        }),
                                        followSurface: !1
                                    }
                                },
                                r = i.entities.add(o);
                            i.edgeEntities.push(r)
                        }
                    }, this.clearTemporaryPolygonCompletion = function() {
                        i.tmpLineEntity && (i.entities.remove(i.tmpLineEntity), i.tmpLineEntity = void 0);
                        for (var e = 0, t = i.edgeEntities; e < t.length; e++) {
                            var n = t[e];
                            i.entities.remove(n)
                        }
                        i.edgeEntities.length = 0, null != i.finishMarkerEntitie && i.entities.remove(i.finishMarkerEntitie)
                    }, this.cartesianToCartographic = function(e) {
                        return i.viewport.scene.globe.ellipsoid.cartesianToCartographic(e)
                    }, this.getMin = function(e, t) {
                        return e < t ? e : t
                    }, this.getMax = function(e, t) {
                        return e < t ? t : e
                    }, this.createClassificationPrimitive = function() {
                        null != i.classifPrimitive && i.viewport.scene.primitives.remove(i.classifPrimitive), null != i.polygonEntity && i.entities.remove(i.polygonEntity);
                        var e = new Cesium.PolygonHierarchy(i.pointPositions),
                            t = new Cesium.PolygonGeometry({
                                polygonHierarchy: e,
                                extrudedHeight: i.minHeight,
                                height: i.maxHeight
                            }),
                            n = new Cesium.GeometryInstance({
                                geometry: t,
                                attributes: {
                                    color: Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(0, 1, 0, .5))
                                }
                            });
                        i.classifPrimitive = i.viewport.scene.primitives.add(new Cesium.ClassificationPrimitive({
                            geometryInstances: n,
                            asynchronous: !1,
                            classificationType: Cesium.ClassificationType.BOTH
                        })), i.polygonEntity = i.entities.add({
                            polygon: {
                                extrudedHeight: i.minHeight,
                                height: i.maxHeight,
                                material: new Cesium.Color(0, 0, 1, .1),
                                outline: !0,
                                outlineColor: new Cesium.Color(0, 0, 1)
                            }
                        }), i.polygonEntity.polygon.hierarchy = new Cesium.CallbackProperty(function() {
                            return e
                        }, !1)
                    }, this.finishPolygon = function() {
                        return !(i.polygonFinished || i.pointPositions.length < 2 || (i.clearTemporaryPolygonCompletion(), i.polygonFinished = !0, i.updateHeightInProgress = !0, i.viewport.toolAdmin.setViewCursor(Bentley.Cursor.Arrow), i.createClassificationPrimitive(), i.classificationContainer = document.createElement("div"), i.classificationContainer.className = "classification-tool-container", i.viewport.BimAdmin.viewer._container.appendChild(i.classificationContainer), i.createClassificationToolSettings(i.classificationContainer), 0))
                    }, this.createClassificationToolSettings = function(e) {
                        var t = document.createElement("div");
                        t.className = "classification-tool", e.appendChild(t);
                        var r = document.createElement("div");
                        r.className = "classification-tool-title", r.textContent = "Classification", t.appendChild(r);
                        var a = document.createElement("div");
                        a.className = "classification-height-field", t.appendChild(a), a.appendChild(document.createTextNode("Min. Height")), a.appendChild(document.createElement("br")), i.minHeightText = document.createElement("input"), i.minHeightText.type = "text", i.minHeightText.className = "classification-height-text", i.minHeightText.value = String(Bentley.Unit.convertUnits(i.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), a.appendChild(i.minHeightText), i.minHeightText.onblur = function() {
                            var e = 0;
                            void 0 !== i.minHeightText && (e = Bentley.Unit.convertUnits(Number(i.minHeightText.value), Bentley.Unit.getUnits(), Bentley.Unit.meter)), e < i.maxHeight ? (i.minHeight = e, i.createClassificationPrimitive()) : void 0 !== i.minHeightText && (i.minHeightText.value = String(Bentley.Unit.convertUnits(i.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits()))), i.updateHeightInProgress = !1
                        }, i.minHeightEditButton = document.createElement("button"), i.minHeightEditButton.type = "button", i.minHeightEditButton.className = "classification-heigth-button";
                        var s = document.createElement("i");
                        s.className = "edit-icon", i.minHeightEditButton.appendChild(s), i.minHeightEditButton.onclick = function() {
                            i.editMinHeight = !0, i.updateHeightInProgress = !0
                        }, i.minHeightEditButton.onblur = function() {
                            i.updateHeightInProgress = !1
                        }, a.appendChild(i.minHeightEditButton);
                        var l = document.createElement("div");
                        l.className = "classification-height-field", t.appendChild(l), l.appendChild(document.createTextNode("Max. Height")), l.appendChild(document.createElement("br")), i.maxHeightEditButton = document.createElement("button"), i.maxHeightEditButton.type = "button", i.maxHeightEditButton.className = "classification-heigth-button";
                        var c = document.createElement("i");
                        c.className = "edit-icon", i.maxHeightEditButton.appendChild(c), i.maxHeightEditButton.onclick = function() {
                            i.editMinHeight = !1, i.updateHeightInProgress = !0
                        }, i.maxHeightEditButton.onblur = function() {
                            i.updateHeightInProgress = !1
                        }, l.appendChild(i.maxHeightEditButton), i.maxHeightText = document.createElement("input"), i.maxHeightText.type = "text", i.maxHeightText.className = "classification-height-text", i.maxHeightText.value = String(Bentley.Unit.convertUnits(i.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), i.maxHeightText.onblur = function() {
                            var e = 0;
                            void 0 !== i.maxHeightText && (e = Bentley.Unit.convertUnits(Number(i.maxHeightText.value), Bentley.Unit.getUnits(), Bentley.Unit.meter)), e > i.minHeight ? (i.maxHeight = e, i.createClassificationPrimitive()) : void 0 !== i.maxHeightText && (i.maxHeightText.value = String(Bentley.Unit.convertUnits(i.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits()))), i.updateHeightInProgress = !1
                        }, l.appendChild(i.maxHeightText), t.appendChild(document.createElement("br"));
                        var u = document.createElement("div");
                        u.className = "addClassification-asset-field", t.appendChild(u), u.appendChild(document.createTextNode("Object ID")), u.appendChild(document.createElement("br"));
                        var d = document.createElement("input");
                        d.type = "text", d.className = "addClassification-asset-text", d.value = "", d.onfocus = function() {
                            u.classList.remove("addClassification-asset-field-invalid")
                        }, d.setAttribute("placeholder", "Assign an object"), u.appendChild(d);
                        var p = document.createElement("button");
                        p.type = "button", p.className = "addClassification-asset-button";
                        var h = document.createElement("i");
                        h.className = "assetlink-icon", p.appendChild(h), p.onclick = function() {
                            new n.AssetAssignationToolSetting(i.viewport).getAsset(d)
                        }, u.appendChild(p);
                        var f = document.createElement("input");
                        f.type = "button", f.value = "Add", f.className = "classification-tool-blueButton", f.onclick = function() {
                            n.AssetAssignationToolSetting.getAssetInfoByCode(d.value).then(function(e) {
                                if ("" != d.value && void 0 === e) u.classList.add("addClassification-asset-field-invalid");
                                else {
                                    var t = {
                                        points: i.pointPositions.slice(),
                                        minHeight: i.minHeight,
                                        maxHeight: i.maxHeight,
                                        assetInfo: e,
                                        primitive: void 0,
                                        modelName: i.viewport.bim.name
                                    };
                                    o.ClassificationEntitiesManager.addClassificationEntitie(t), i.viewport.toolAdmin._primitiveTool.exitTool()
                                }
                            })
                        }, t.appendChild(f);
                        var m = document.createElement("input");
                        m.type = "button", m.value = "Cancel", m.className = "classification-tool-button", m.onclick = function() {
                            i.viewport.toolAdmin._primitiveTool.exitTool()
                        }, t.appendChild(m)
                    }, r.HttpRequestHelper.IsBackendActivated() || alert("The backend API is not activated."), Bentley.PrimitiveTool.call(this, e), this.id = "Vivo.AddClassificationTool", this.viewport = e, this.addPointCursor = "cursor: url(" + document.baseURI + "/app/content/images/AddPoint.png), crosshair;", this.closeShapeCursor = "cursor: url(" + document.baseURI + "/app/content/images/CloseShape.png), crosshair;", this.dataSourceCollection = new Cesium.DataSourceCollection, this.dataSourceDisplay = new Cesium.DataSourceDisplay({
                        scene: this.viewport.scene,
                        dataSourceCollection: this.dataSourceCollection
                    }), this.entities = this.dataSourceDisplay.defaultDataSource.entities, this.pointPositions = [], this.edgeEntities = [], this.polygonFinished = !1, this.tmpLineEntity = void 0, this.minHeight = 0, this.maxHeight = 0, this.classifPrimitive = void 0, this.polygonEntity = void 0, this.editMinHeight = !1, this.updateHeightInProgress = !1, this.turnCameraOn = !1, this.eventHelper = new Cesium.EventHelper, this.eventHelper.add(e.BimAdmin.viewer.cesiumWidget.clock.onTick, function(e) {
                        i.dataSourceDisplay.update(e), i.dataSourceDisplay.scene.requestRenderMode && i.dataSourceDisplay.scene.requestRender()
                    }, this), this.viewport.toolAdmin.activeToolChanged.addEventListener(function() {
                        i.tmpLineEntity && (i.entities.remove(i.tmpLineEntity), i.tmpLineEntity = void 0)
                    }), this.viewport.selectionSet.clear()
                }
            }();
        i.AddClassificationTool = a;
        var s = function() {
            return function() {
                this.getToolConstructor = function() {
                    return Bentley.extend(Bentley.PrimitiveTool, a), a
                }
            }
        }();
        i.AddClassificationToolWrapper = s
    }, {
        "./../Utils/HttpRequestHelper": 25,
        "./AssetAssignationTool": 8,
        "./ClassificationEntitiesManager": 9
    }],
    8: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("jquery"),
            o = e("../ConfigManager"),
            r = e("./CreateAssetTool"),
            a = e("./ClassificationEntitiesManager"),
            s = e("./../Utils/DocumentUtils"),
            l = e("../Utils/HttpRequestHelper"),
            c = function() {
                function e(e) {
                    var t = this;
                    this.getAsset = function(e) {
                        var i, n = document.createElement("div");
                        n.className = "assetAssignation-tool-container", t.assignationModal.appendChild(n);
                        var o = document.createElement("div");
                        o.className = "assetAssignation-tool", n.appendChild(o);
                        var a = document.createElement("div");
                        a.className = "assetAssignation-tool-title", a.textContent = "Assign an Object", o.appendChild(a);
                        var l = s.DocumentUtils.createInput("createButton", "button", "assetAssignation-tool-createButton", "Create new Object");
                        o.appendChild(l), o.appendChild(document.createTextNode("Search for existing Object"));
                        var c = document.createElement("div");
                        c.className = "assetAssignation-searchField", o.appendChild(c);
                        var u = document.createElement("div");
                        u.className = "assetAssignation-box", c.appendChild(u), u.appendChild(document.createTextNode("Code")), u.appendChild(document.createElement("br"));
                        var d = s.DocumentUtils.createInput("codeInput", "text", "assetAssignation-tool-text");
                        u.appendChild(d);
                        var p = document.createElement("div");
                        p.className = "assetAssignation-box", c.appendChild(p), p.appendChild(document.createTextNode("Type")), p.appendChild(document.createElement("br"));
                        var h = s.DocumentUtils.createInput("typeInput", "text", "assetAssignation-tool-text");
                        p.appendChild(h);
                        var f = document.createElement("div");
                        f.className = "assetAssignation-box", c.appendChild(f), f.appendChild(document.createTextNode("Name")), f.appendChild(document.createElement("br"));
                        var m = s.DocumentUtils.createInput("nameInput", "text", "assetAssignation-tool-text");
                        f.appendChild(m);
                        var v = document.createElement("div");
                        v.className = "assetAssignation-box", c.appendChild(v), v.appendChild(document.createTextNode("Description")), v.appendChild(document.createElement("br"));
                        var g = s.DocumentUtils.createInput("descriptionInput", "text", "assetAssignation-tool-text");
                        v.appendChild(g), c.appendChild(document.createElement("br"));
                        var y = s.DocumentUtils.createInput("searchButton", "button", "assetAssignation-tool-button", "Search");
                        c.appendChild(y);
                        var C = s.DocumentUtils.createInput("clearButton", "button", "assetAssignation-tool-button", "Clear", function() {
                            d.value = "", m.value = "", g.value = "", h.value = ""
                        });
                        c.appendChild(C);
                        var b = document.createElement("div");
                        b.className = "assetAssignation-tableField", o.appendChild(b);
                        var x = document.createElement("table");
                        x.className = "assetAssignation-table", b.appendChild(x);
                        var w = document.createElement("tr");
                        w.className = "assetAssignation-tr-gray", x.appendChild(w);
                        var T = document.createElement("th");
                        T.className = "assetAssignation-table-cell header", T.appendChild(document.createTextNode("Code")), T.dataset.sortDir = "", T.dataset.sortProperty = "code";
                        var E = document.createElement("i");
                        T.appendChild(E), T.onclick = function() {
                            E.className = N.className = S.className = B.className = "", P.dataset.sortDir = A.dataset.sortDir = M.dataset.sortDir = "", T.className = P.className = A.className = M.className = "assetAssignation-table-cell header", t.updateHeader(E, T), t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, T, 0), i = T
                        }, T.onmouseenter = function() {
                            t.grayHeader(E, T)
                        }, T.onmouseleave = function() {
                            t.restorHeaderColor(E, T)
                        }, w.appendChild(T), i = T;
                        var P = document.createElement("th");
                        P.className = "assetAssignation-table-cell header", P.appendChild(document.createTextNode("Type")), P.dataset.sortDir = "", P.dataset.sortProperty = "type";
                        var N = document.createElement("i");
                        P.appendChild(N), P.onclick = function() {
                            E.className = N.className = S.className = B.className = "", T.dataset.sortDir = A.dataset.sortDir = M.dataset.sortDir = "", T.className = P.className = A.className = M.className = "assetAssignation-table-cell header", t.updateHeader(N, P), t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, P, 0), i = P
                        }, P.onmouseenter = function() {
                            t.grayHeader(N, P)
                        }, P.onmouseleave = function() {
                            t.restorHeaderColor(N, P)
                        }, w.appendChild(P);
                        var A = document.createElement("th");
                        A.className = "assetAssignation-table-cell header", A.appendChild(document.createTextNode("Name")), A.dataset.sortDir = "", A.dataset.sortProperty = "name";
                        var S = document.createElement("i");
                        A.appendChild(S), A.onclick = function() {
                            E.className = N.className = S.className = B.className = "", T.dataset.sortDir = P.dataset.sortDir = M.dataset.sortDir = "", T.className = P.className = A.className = M.className = "assetAssignation-table-cell header", t.updateHeader(S, A), t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, A, 0), i = A
                        }, A.onmouseenter = function() {
                            t.grayHeader(S, A)
                        }, A.onmouseleave = function() {
                            t.restorHeaderColor(S, A)
                        }, w.appendChild(A);
                        var M = document.createElement("th");
                        M.className = "assetAssignation-table-cell header", M.appendChild(document.createTextNode("Description")), M.dataset.sortDir = "", M.dataset.sortProperty = "description";
                        var B = document.createElement("i");
                        M.appendChild(B), M.onclick = function() {
                            E.className = N.className = S.className = B.className = "", T.dataset.sortDir = P.dataset.sortDir = A.dataset.sortDir = "", T.className = P.className = A.className = M.className = "assetAssignation-table-cell header", t.updateHeader(B, M), t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, M, 0), i = M
                        }, M.onmouseenter = function() {
                            t.grayHeader(B, M)
                        }, M.onmouseleave = function() {
                            t.restorHeaderColor(B, M)
                        }, w.appendChild(M), y.onclick = function() {
                            t.lastEntryNumber = 0, t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, i, 0)
                        }, l.onclick = function() {
                            new r.CreateAssetToolSetting(n).createAsset().then(function(e) {
                                if ("" !== e.id) {
                                    var i = document.createElement("tr");
                                    i.className = "assetAssignation-tr", i.onclick = function() {
                                        var e = document.getElementById("assetAssignationSelectButton");
                                        null != e && e.removeAttribute("disabled");
                                        for (var t = 0; t < x.rows.length; t++) x.rows[t] === this ? x.rows[t].classList.add("assetAssignation-tool-selected") : x.rows[t].classList.remove("assetAssignation-tool-selected")
                                    }, i.dataset.code = e.code, i.classList.add("selected"), x.insertBefore(i, x.childNodes[1]);
                                    var n = document.createElement("td");
                                    n.className = "assetAssignation-table-cell", n.appendChild(document.createTextNode("*" + e.code)), i.appendChild(n);
                                    var o = document.createElement("td");
                                    o.className = "assetAssignation-table-cell", o.appendChild(document.createTextNode(e.type)), i.appendChild(o);
                                    var r = document.createElement("td");
                                    r.className = "assetAssignation-table-cell", r.appendChild(document.createTextNode(e.name)), i.appendChild(r);
                                    var a = document.createElement("td");
                                    a.className = "assetAssignation-table-cell", a.appendChild(document.createTextNode(e.description)), i.appendChild(a), t.selectedRow = i;
                                    var s = document.getElementById("assetAssignationSelectButton");
                                    null !== s && s.removeAttribute("disabled");
                                    for (var l = 0; l < x.rows.length; l++) 1 === l ? x.rows[l].classList.add("assetAssignation-tool-selected") : x.rows[l].classList.remove("assetAssignation-tool-selected")
                                }
                            })
                        };
                        var H = document.createElement("div");
                        H.className = "assetAssignation-tool-previousNextField", H.id = "assetAssignationPreviousNextField", o.appendChild(H);
                        var I = s.DocumentUtils.createInput("assetAssignationNextButton", "button", "assetAssignation-tool-previousNext", ">", function() {
                            t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, i, t.lastEntryNumber)
                        });
                        H.appendChild(I);
                        var k = s.DocumentUtils.createInput("assetAssignationPreviousButton", "button", "assetAssignation-tool-previousNext", "<", function() {
                            var e = t.lastEntryNumber - 2 * t.maxNumberRow < 0 ? 0 : t.lastEntryNumber - 2 * t.maxNumberRow;
                            t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, i, e)
                        });
                        H.appendChild(k), H.appendChild(document.createElement("br")), H.appendChild(document.createElement("br"));
                        var _ = s.DocumentUtils.createInput("assetAssignationSelectButton", "button", "assetAssignation-tool-blueButton1", "Select", function() {
                            null !== t.selectedRow && (e.value = t.selectedRow.dataset.code, t.viewport.BimAdmin.viewer._container.removeChild(t.assignationModal))
                        });
                        _.setAttribute("disabled", "disabled"), o.appendChild(_);
                        var D = s.DocumentUtils.createInput("assetAssignationCancelButton", "button", "assetAssignation-tool-button1", "Cancel", function() {
                            t.viewport.BimAdmin.viewer._container.removeChild(t.assignationModal)
                        });
                        o.appendChild(D), t.updateAssetTable(o, x, d.value, m.value, g.value, h.value, i, 0)
                    }, this.grayHeader = function(e, t) {
                        "" === t.dataset.sortDir && (e.className = "assetSortDownGray-icon"), t.className = "assetAssignation-table-cell header gray"
                    }, this.restorHeaderColor = function(e, t) {
                        "" === t.dataset.sortDir && (e.className = "", t.className = "assetAssignation-table-cell header")
                    }, this.updateHeader = function(e, t) {
                        var i = "",
                            n = "";
                        switch (t.dataset.sortDir) {
                            case "desc":
                                i = "assetSortDown-icon", n = "asc";
                                break;
                            case "asc":
                            default:
                                i = "assetSortUp-icon", n = "desc"
                        }
                        e.className = i, t.dataset.sortDir = n, "" !== n && (t.className = "assetAssignation-table-cell header gray")
                    }, this.updateAssetTable = function(e, i, n, r, a, s, c, u) {
                        t._loader = document.createElement("div"), t._loader.className = "assetAssignation-tool-loader", e.appendChild(t._loader);
                        var d = o.ConfigManager.GetConfig().backendUrl + o.ConfigManager.GetConfig().backendEndpoints.getAssets,
                            p = d.length;
                        "" !== s && (d += "?" === d.charAt(p) ? "&" : "?", d += "type=" + s), "" !== n && (d += "?" === d.charAt(p) ? "&" : "?", d += "code=" + n), "" !== r && (d += "?" === d.charAt(p) ? "&" : "?", d += "name=" + r), "" !== a && (d += "?" === d.charAt(p) ? "&" : "?", d += "description=" + a), d += "?" === d.charAt(p) ? "&" : "?", d += "sortProperty=" + c.dataset.sortProperty, d += "&sortOrder=" + c.dataset.sortDir, d += "&skip=" + u, d += "&limit=" + t.maxNumberRow, t._assignationToolElement = e, t._tableToUpdate = i, t._currentSkip = u, l.HttpRequestHelper.GetRequest(d, void 0, t.OnSuccessUpdateAssetTable.bind(t), t.OnErrorUpdateAssetTable.bind(t))
                    }, this.OnSuccessUpdateAssetTable = function(e) {
                        for (var i = t._tableToUpdate.rows.length - 1; i > 0; i--) t._tableToUpdate.deleteRow(i);
                        "string" != typeof e && e.forEach(function(e) {
                            var i = document.createElement("tr");
                            i.className = "assetAssignation-tr";
                            var n = t;
                            i.onclick = function() {
                                n.selectedRow = this;
                                var e = document.getElementById("assetAssignationSelectButton");
                                null !== e && e.removeAttribute("disabled");
                                for (var t = 0; t < n._tableToUpdate.rows.length; t++) n._tableToUpdate.rows[t] === i ? n._tableToUpdate.rows[t].classList.add("assetAssignation-tool-selected") : n._tableToUpdate.rows[t].classList.remove("assetAssignation-tool-selected")
                            }, i.dataset.code = e.code, t._tableToUpdate.appendChild(i);
                            var o = document.createElement("td");
                            o.className = "assetAssignation-table-cell", o.appendChild(document.createTextNode(null !== e.code ? e.code : "")), i.appendChild(o);
                            var r = document.createElement("td");
                            r.className = "assetAssignation-table-cell", r.appendChild(document.createTextNode(null !== e.type ? e.type : "")), i.appendChild(r);
                            var a = document.createElement("td");
                            a.className = "assetAssignation-table-cell", a.appendChild(document.createTextNode(null !== e.name ? e.name : "")), i.appendChild(a);
                            var s = document.createElement("td");
                            s.className = "assetAssignation-table-cell", s.appendChild(document.createTextNode(null !== e.description ? e.description : "")), i.appendChild(s)
                        }), t._assignationToolElement.removeChild(t._loader), t._loader = void 0;
                        var n = null;
                        e.length >= t.maxNumberRow || t._currentSkip > 0 ? (null !== (n = document.getElementById("assetAssignationPreviousNextField")) && n.classList.add("assetAssignation-tool-previousNextField-show"), e.length < t.maxNumberRow ? null !== (n = document.getElementById("assetAssignationNextButton")) && n.setAttribute("disabled", "disabled") : null !== (n = document.getElementById("assetAssignationNextButton")) && n.removeAttribute("disabled"), 0 === t._currentSkip ? null !== (n = document.getElementById("assetAssignationPreviousButton")) && n.setAttribute("disabled", "disabled") : null !== (n = document.getElementById("assetAssignationPreviousButton")) && n.removeAttribute("disabled")) : null !== (n = document.getElementById("assetAssignationPreviousNextField")) && n.classList.remove("assetAssignation-tool-previousNextField-show"), t.lastEntryNumber = t._currentSkip + t.maxNumberRow
                    }, this.OnErrorUpdateAssetTable = function(e, t) {
                        alert("ContextCapture Services: " + t)
                    }, this.viewport = e, this.assignationModal = document.createElement("div"), this.assignationModal.className = "blocker", this.viewport.BimAdmin.viewer._container.appendChild(this.assignationModal), this.selectedRow = null, this.lastEntryNumber = 0, this.maxNumberRow = 15, this._currentSkip = 0
                }
                return e.getAssetInfo = function(e) {
                    var t = n.Deferred();
                    if (null === e) t.resolve(void 0);
                    else {
                        var i = (o.ConfigManager.GetConfig().backendUrl + o.ConfigManager.GetConfig().backendEndpoints.getAsset).replace("{id}", e);
                        l.HttpRequestHelper.GetRequest(i, void 0, function(e) {
                            if (void 0 !== e) {
                                var i = {
                                    id: e.internalId,
                                    type: e.type,
                                    name: e.name,
                                    code: e.code,
                                    description: e.description
                                };
                                t.resolve(i)
                            } else t.resolve(void 0)
                        }, function() {
                            t.resolve(void 0)
                        })
                    }
                    return t.promise()
                }, e.getAssetInfoByCode = function(e) {
                    var t = n.Deferred(),
                        i = (o.ConfigManager.GetConfig().backendUrl + o.ConfigManager.GetConfig().backendEndpoints.getAssetByCode).replace("{code}", e);
                    return l.HttpRequestHelper.GetRequest(i, void 0, function(e) {
                        if (void 0 !== e) {
                            var i = {
                                id: e.internalId,
                                type: e.type,
                                name: e.name,
                                code: e.code,
                                description: e.description
                            };
                            t.resolve(i)
                        } else t.resolve(void 0)
                    }, function() {
                        t.resolve(void 0)
                    }), t.promise()
                }, e
            }();
        i.AssetAssignationToolSetting = c;
        var u = function() {
            return function(e, t) {
                var i = this;
                this.onPostInstall = function() {
                    l.HttpRequestHelper.IsBackendReachable() ? i.assignAsset() : (alert("ContextCapture Services: Server unreachable."), i.viewport.toolAdmin._primitiveTool.exitTool())
                }, this.onCleanup = function(e) {
                    void 0 !== i.assignationContainer && i.viewport.BimAdmin.viewer._container.removeChild(i.assignationContainer)
                }, this.assignAsset = function() {
                    if (void 0 !== i.elementId) {
                        i.assignationContainer = document.createElement("div"), i.assignationContainer.className = "classification-tool-container", i.viewport.BimAdmin.viewer._container.appendChild(i.assignationContainer);
                        var e = document.createElement("div");
                        e.className = "classification-tool", i.assignationContainer.appendChild(e);
                        var t = document.createElement("div");
                        t.className = "classification-tool-title", t.textContent = "Assign", e.appendChild(t);
                        var n = document.createElement("div");
                        n.className = "addClassification-asset-field", e.appendChild(n), n.appendChild(document.createTextNode("Object ID")), n.appendChild(document.createElement("br"));
                        var o = s.DocumentUtils.createInput("assetAssignationTool-assetText", "text", "addClassification-asset-text", "", function() {
                            n.classList.remove("addClassification-asset-field-invalid")
                        });
                        o.setAttribute("placeholder", "Assign an object"), n.appendChild(o);
                        var r = document.createElement("button");
                        r.type = "button", r.className = "addClassification-asset-button";
                        var l = document.createElement("i");
                        l.className = "assetlink-icon", r.appendChild(l), r.onclick = function() {
                            new c(i.viewport).getAsset(o)
                        }, n.appendChild(r);
                        var u = s.DocumentUtils.createInput("addClassification-assignButton", "button", "classification-tool-blueButton", "Assign", function() {
                            c.getAssetInfoByCode(o.value).then(function(e) {
                                if ("" != o.value && void 0 === e) n.classList.add("addClassification-asset-field-invalid");
                                else {
                                    var t = a.ClassificationEntitiesManager.getOneClassificationInfos(i.elementId);
                                    Cesium.defined(t) ? (t.assetInfo = e, a.ClassificationEntitiesManager.updateClassificationEntitie(t)) : a.ClassificationEntitiesManager.updateElementEntity(e, i.elementId), i.viewport.toolAdmin._primitiveTool.exitTool()
                                }
                            })
                        });
                        e.appendChild(u);
                        var d = s.DocumentUtils.createInput("addClassification-cancelButton", "button", "classification-tool-button", "Cancel", function() {
                            i.viewport.toolAdmin._primitiveTool.exitTool()
                        });
                        e.appendChild(d)
                    }
                }, Bentley.PrimitiveTool.call(this, e), this.id = "Vivo.AssetAssignationTool", this.viewport = e, this.elementId = t
            }
        }();
        i.AssetAssignationTool = u;
        var d = function() {
            return function() {
                this.getToolConstructor = function() {
                    return Bentley.extend(Bentley.PrimitiveTool, u), u
                }
            }
        }();
        i.AssetAssignationToolWrapper = d
    }, {
        "../ConfigManager": 5,
        "../Utils/HttpRequestHelper": 25,
        "./../Utils/DocumentUtils": 24,
        "./ClassificationEntitiesManager": 9,
        "./CreateAssetTool": 10,
        jquery: 31
    }],
    9: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("../ConfigManager"),
            o = e("./AssetAssignationTool"),
            r = e("./../Utils/HttpRequestHelper"),
            a = function() {
                function e() {}
                return e.Init = function() {
                    this.onAddClassifierEvent = new Cesium.Event, this.onEraseClassifierEvent = new Cesium.Event, this.onLoadedClassifierEvent = new Cesium.Event
                }, e.PostInit = function(e) {
                    this._viewer = e, this.GetExistingClassifications(), this.SubscribeEvents()
                }, e.SubscribeEvents = function() {
                    var t = this;
                    this._viewer.bimAdmin.changeEvent.addEventListener(e.onTilesetChanged.bind(this));
                    var i = this._viewer.bimAdmin.viewport;
                    i.toolAdmin.selectionSet.changed.addEventListener(this.onSelectionSetChanged.bind(this)), this._mouseHandler = new Cesium.ScreenSpaceEventHandler(i.scene.canvas), this._mouseHandler.setInputAction(function(e) {
                        var n = i.scene.pick(e.position);
                        void 0 !== n && (void 0 !== t._selectedObject && (t._selectedObject.primitive.getGeometryInstanceAttributes(t._selectedObject.id).color = t._normalColor.toBytes(), t._selectedObject = void 0), Cesium.defined(n) && Cesium.defined(n.id) && Cesium.defined(n.elementId) && Cesium.defined(n.primitive) && Cesium.defined(n.primitive.getGeometryInstanceAttributes) && (n.primitive.getGeometryInstanceAttributes(n.id).color = t._highlightColor, t._selectedObject = n), i.scene.requestRenderMode && i.scene.requestRender())
                    }, Cesium.ScreenSpaceEventType.LEFT_CLICK)
                }, e.setClassificationColor = function(e, t) {
                    void 0 !== e && void 0 !== e.primitive && void 0 !== e.primitive._primitive && void 0 !== e.primitive._primitive._instanceIds ? e.primitive._primitive._instanceIds.forEach(function(i) {
                        var n = e.primitive.getGeometryInstanceAttributes(i);
                        void 0 !== n && n.color && (n.color = new Cesium.ColorGeometryInstanceAttribute.toValue(t))
                    }) : console.log("Unable to set classification color")
                }, e.getClassificationColor = function(e) {
                    var t = void 0;
                    return void 0 !== e && void 0 !== e.primitive && void 0 !== e.primitive._primitive && void 0 !== e.primitive._primitive._instanceIds && e.primitive._primitive._instanceIds.forEach(function(i) {
                        var n = e.primitive.getGeometryInstanceAttributes(i);
                        if (void 0 != n && n.color) {
                            var o = n.color;
                            t = Cesium.Color.fromBytes(o.red, o.green, o.blue, o.alpha)
                        }
                    }), t
                }, e.onTilesetChanged = function(e) {
                    switch (e) {
                        case Bentley.BimiumEvents.BeforeTileSetChanged:
                            this.classificationInfos = [];
                            break;
                        case Bentley.BimiumEvents.TileSetChanged:
                            this.GetExistingClassifications()
                    }
                }, e.onSelectionSetChanged = function() {
                    void 0 !== this._selectedObject && (this._selectedObject.primitive.getGeometryInstanceAttributes(this._selectedObject.id).color = this._normalColor.toBytes(), this._selectedObject = void 0)
                }, e.addClassificationEntitie = function(e) {
                    this._currentClassificationInfo = e, this._currentClassificationInfo.assetId = void 0 !== e.assetInfo ? e.assetInfo.id : void 0;
                    var t = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.addClassifier).replace("{modelName}", this._viewer.bimAdmin.bim.name);
                    r.HttpRequestHelper.PostRequest(t, void 0, this._currentClassificationInfo, this.OnSuccessAddClassificationEntitie.bind(this), this.OnErrorAddClassificationEntitie.bind(this))
                }, e.OnSuccessAddClassificationEntitie = function(e) {
                    console.log(e), this._currentClassificationInfo.id = e, this.CreateClassificationPolygon(this._currentClassificationInfo, this._viewer.bimAdmin.viewport), this.classificationInfos.push(this._currentClassificationInfo), this._viewer.bimAdmin.viewer.viewport.scene.requestRender(), this.onAddClassifierEvent.raiseEvent(this._currentClassificationInfo)
                }, e.OnErrorAddClassificationEntitie = function(e, t) {
                    alert("ContextCapture Services: " + t)
                }, e.getClassificationInfos = function() {
                    return this.classificationInfos
                }, e.getOneClassificationInfos = function(e) {
                    for (var t, i = 0, n = this.classificationInfos; i < n.length; i++) {
                        var o = n[i];
                        if (o.id === e) {
                            t = o;
                            break
                        }
                    }
                    return t
                }, e.deleteClassificationEntitie = function(e) {
                    if (this._currentRemovedClassifier = this.classificationInfos.filter(function(t) {
                            return t.id === e
                        })[0], void 0 !== this._currentRemovedClassifier) {
                        var t = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.removeClassifier).replace("{elementId}", e);
                        r.HttpRequestHelper.GetRequest(t, void 0, this.OnSuccessDeleteClassificationEntitie.bind(this), this.OnErrorDeleteClassificationEntitie.bind(this))
                    }
                }, e.OnSuccessDeleteClassificationEntitie = function(e) {
                    if (e) {
                        this._viewer.bimAdmin.viewer.viewport.scene.primitives.remove(this._currentRemovedClassifier.primitive), this._viewer.bimAdmin.viewer.viewport.scene.requestRender();
                        var t = this.classificationInfos.indexOf(this._currentRemovedClassifier);
                        this.classificationInfos.splice(t, 1), this.onEraseClassifierEvent.raiseEvent(this._currentRemovedClassifier)
                    } else console.log("Unable to delete the classification from the server.")
                }, e.OnErrorDeleteClassificationEntitie = function(e, t) {
                    alert("ContextCapture Services: " + t)
                }, e.updateClassificationEntitie = function(e) {
                    this._currentClassificationInfo = e;
                    var t = e.assetInfo,
                        i = {
                            id: e.id,
                            points: e.points,
                            minHeight: e.minHeight,
                            maxHeight: e.maxHeight,
                            assetId: void 0 !== t ? t.id : void 0
                        },
                        o = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.updateClassifier).replace("{modelName}", this._viewer.bimAdmin.bim.name).replace("{elementId}", e.id);
                    r.HttpRequestHelper.PutRequest(o, void 0, i, this.OnSuccessUpdateClassificationEntitie.bind(this), this.OnErrorUpdateClassificationEntitie.bind(this))
                }, e.OnSuccessUpdateClassificationEntitie = function(e) {
                    var t = this.classificationInfos.filter(function(t) {
                        return t.id === e
                    })[0];
                    void 0 !== t && (t = this._currentClassificationInfo), this._viewer.bimAdmin.viewport.selectionSet.raiseChanged()
                }, e.OnErrorUpdateClassificationEntitie = function(e, t) {
                    alert("ContextCapture Services: " + t)
                }, e.updateElementEntity = function(e, t) {
                    var i = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.addOrUpdateRelation).replace("{elementId}", t).replace("{assetId}", e.id);
                    r.HttpRequestHelper.PutRequest(i, void 0, void 0, this.OnSuccessUpdateElementEntitiy.bind(this), this.OnErrorUpdateElementEntitiy.bind(this))
                }, e.OnSuccessUpdateElementEntitiy = function(e) {
                    this._viewer.bimAdmin.viewport.selectionSet.raiseChanged()
                }, e.OnErrorUpdateElementEntitiy = function(e, t) {
                    alert("ContextCapture Services: " + t)
                }, e.CreateClassificationPolygon = function(e, t) {
                    e.primitive = new Cesium.ClassificationPrimitive({
                        geometryInstances: new Cesium.GeometryInstance({
                            geometry: new Cesium.PolygonGeometry({
                                polygonHierarchy: new Cesium.PolygonHierarchy(e.points),
                                extrudedHeight: e.minHeight,
                                height: e.maxHeight
                            }),
                            attributes: {
                                color: Cesium.ColorGeometryInstanceAttribute.fromColor(this._normalColor),
                                show: new Cesium.ShowGeometryInstanceAttribute(!t.scene.invertClassification)
                            },
                            id: e.id,
                            elementId: e.id
                        }),
                        asynchronous: !1,
                        classificationType: Cesium.ClassificationType.BOTH
                    }), t.scene.primitives.add(e.primitive)
                }, e.GetExistingClassifications = function() {
                    var e = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.getClassifiers).replace("{modelName}", this._viewer.bimAdmin.bim.name);
                    r.HttpRequestHelper.GetRequest(e, void 0, this.OnSuccessGetExistingClassifications.bind(this), this.OnErrorGetExistingClassifications.bind(this))
                }, e.OnSuccessGetExistingClassifications = function(e) {
                    var t = this;
                    if (e.length > 0) {
                        for (var i = [], n = function(e) {
                                if (null !== e) {
                                    var n = $.Deferred();
                                    o.AssetAssignationToolSetting.getAssetInfo(e.assetId).then(function(i) {
                                        var o = {
                                            points: e.points,
                                            minHeight: e.minHeight,
                                            maxHeight: e.maxHeight,
                                            id: e.internalId,
                                            assetInfo: i,
                                            primitive: null
                                        };
                                        t.CreateClassificationPolygon(o, t._viewer.bimAdmin.viewport), t.classificationInfos.push(o), n.resolve()
                                    }), i.push(n)
                                }
                            }, r = 0, a = e; r < a.length; r++) {
                            n(a[r])
                        }
                        $.when.apply($, i).then(function() {
                            t.onLoadedClassifierEvent.raiseEvent()
                        })
                    }
                }, e.OnErrorGetExistingClassifications = function(e, t) {
                    alert("ContextCapture Services: " + t)
                }, e.getAssetNameFromElement = function(e, t) {
                    this._getNamePromise = Cesium.when.defer();
                    var i = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.getClassifierAsset).replace("{modelName}", e).replace("{elementId}", t);
                    return r.HttpRequestHelper.GetRequest(i, void 0, this.OnSuccessGetAssetNameFromElement.bind(this), this.OnErrorGetAssetNameFromElement.bind(this)), this._getNamePromise
                }, e.OnSuccessGetAssetNameFromElement = function(e) {
                    this._getNamePromise.resolve(e.categories[0].properties[0].label)
                }, e.OnErrorGetAssetNameFromElement = function(e, t) {
                    alert("ContextCapture Services: " + t), this._getNamePromise.resolve("")
                }, e._normalColor = Cesium.Color.LIMEGREEN.withAlpha(.5), e._highlightColor = [75, 144, 245, 155], e.classificationInfos = [], e
            }();
        i.ClassificationEntitiesManager = a
    }, {
        "../ConfigManager": 5,
        "./../Utils/HttpRequestHelper": 25,
        "./AssetAssignationTool": 8
    }],
    10: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("jquery"),
            o = e("../ConfigManager"),
            r = e("../Utils/HttpRequestHelper"),
            a = function() {
                return function(e) {
                    var t = this;
                    this.createAsset = function() {
                        var e = n.Deferred(),
                            i = document.createElement("div");
                        i.className = "createAsset-tool-container", t.createModal.appendChild(i);
                        var a = document.createElement("div");
                        a.className = "createAsset-tool", i.appendChild(a);
                        var s = document.createElement("div");
                        s.className = "createAsset-tool-title", s.textContent = "Create Object", a.appendChild(s);
                        var l = document.createElement("div");
                        l.className = "createAsset-createField", a.appendChild(l);
                        var c = document.createElement("div");
                        c.className = "createAsset-box", l.appendChild(c), c.appendChild(document.createTextNode("Type")), c.appendChild(document.createElement("br"));
                        var u = document.createElement("select");
                        u.className = "createAsset-tool-listBox", u.required = !0, u.onchange = function() {
                            if ("" !== h.value && u.selectedIndex > 0) {
                                var e = document.getElementById("createAssetCreateButton");
                                null !== e && e.removeAttribute("disabled")
                            } else v.setAttribute("disabled", "disabled")
                        };
                        var d = o.ConfigManager.GetConfig().backendUrl + o.ConfigManager.GetConfig().backendEndpoints.getTypes;
                        r.HttpRequestHelper.GetRequest(d, void 0, function(e) {
                            var t = document.createElement("option");
                            t.value = "", t.text = "", u.appendChild(t);
                            for (var i = 0, n = e; i < n.length; i++) {
                                var o = n[i];
                                (t = document.createElement("option")).value = o.id, t.text = o.name, u.appendChild(t)
                            }
                        }, function(e) {}), c.appendChild(u);
                        var p = document.createElement("div");
                        p.className = "createAsset-box", l.appendChild(p), p.appendChild(document.createTextNode("Name")), p.appendChild(document.createElement("br"));
                        var h = document.createElement("input");
                        h.type = "text", h.className = "createAsset-tool-text", h.required = !0, h.classList.add("required"), h.onblur = function() {
                            if ("" !== h.value && u.selectedIndex > 0) {
                                var e = document.getElementById("createAssetCreateButton");
                                null !== e && e.removeAttribute("disabled")
                            } else v.setAttribute("disabled", "disabled")
                        }, p.appendChild(h);
                        var f = document.createElement("div");
                        f.className = "createAsset-box", l.appendChild(f), f.appendChild(document.createTextNode("Description")), f.appendChild(document.createElement("br"));
                        var m = document.createElement("input");
                        m.type = "text", m.className = "createAsset-tool-text", f.appendChild(m);
                        var v = document.createElement("input");
                        v.type = "button", v.value = "Create", v.className = "assetAssignation-tool-blueButton1", v.id = "createAssetCreateButton", v.setAttribute("disabled", "disabled"), v.onclick = function() {
                            var i = document.createElement("div");
                            i.className = "assetAssignation-tool-loader", a.appendChild(i);
                            var n = {
                                    id: "",
                                    type: u.children[u.selectedIndex].text,
                                    name: h.value,
                                    description: m.value
                                },
                                s = o.ConfigManager.GetConfig().backendUrl + o.ConfigManager.GetConfig().backendEndpoints.addAsset;
                            r.HttpRequestHelper.PostRequest(s, void 0, n, function(n) {
                                a.removeChild(i), t.container.removeChild(t.createModal), e.resolve(n)
                            }, function(e, t) {
                                alert("ContextCapture Services: " + t)
                            })
                        }, a.appendChild(v);
                        var g = document.createElement("input");
                        return g.type = "button", g.value = "Cancel", g.className = "assetAssignation-tool-button1", g.onclick = function() {
                            t.container.removeChild(t.createModal)
                        }, a.appendChild(g), e.promise()
                    }, this.container = e, this.createModal = document.createElement("div"), this.createModal.className = "blocker", this.container.appendChild(this.createModal)
                }
            }();
        i.CreateAssetToolSetting = a
    }, {
        "../ConfigManager": 5,
        "../Utils/HttpRequestHelper": 25,
        jquery: 31
    }],
    11: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../Tools/ClassificationEntitiesManager"),
            o = function() {
                return function(e) {
                    var t = this;
                    this.deleteClassification = function() {
                        return void 0 !== t.classificationId && n.ClassificationEntitiesManager.deleteClassificationEntitie(t.classificationId), !0
                    }, this.classificationId = e
                }
            }();
        i.DeleteClassificationTool = o
    }, {
        "./../Tools/ClassificationEntitiesManager": 9
    }],
    12: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../Tools/ClassificationEntitiesManager"),
            o = function() {
                return function(e, t, i) {
                    var o = this;
                    if (this.refreshUnit = function() {
                            o.minHeightText.value = String(Bentley.Unit.convertUnits(o.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), o.maxHeightText.value = String(Bentley.Unit.convertUnits(o.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits()))
                        }, this.onModelMotion = function(e) {
                            return Bentley.InputEventModifiers.None === e.keyModifiers && (o.updateHeightInProgress && o.updateClassificationHeight(e), !0)
                        }, this.onCleanup = function(e) {
                            null != o.classifPrimitive && o.viewport.scene.primitives.remove(o.classifPrimitive), null != o.polygonEntity && o.entities.remove(o.polygonEntity), o.eventHelper.removeAll(), o.dataSourceDisplay = o.dataSourceDisplay.destroy(), o.dataSourceCollection = o.dataSourceCollection.destroy(), o.pointPositions.length = 0, void 0 !== o.classificationContainer && o.viewport.BimAdmin.viewer._container.removeChild(o.classificationContainer), o.turnCameraOn && (o.viewport.turnCameraOn(Bentley.ViewToolSettings.walkCameraAngle * (Math.PI / 180)), o.turnCameraOn = !1)
                        }, this.onDataButtonDown = function(e) {
                            return Bentley.InputEventModifiers.None === e.keyModifiers && !!o.viewport.scene.pickPositionSupported && (o.updateHeightInProgress && (o.updateHeightInProgress = !1, o.maxHeightEditButton.blur(), o.minHeightEditButton.blur()), !0)
                        }, this.updateClassificationHeight = function(e) {
                            o.updateHeightInProgress = !0;
                            var t = e.point,
                                i = o.viewport.npcToWorld(new Cesium.Cartesian3(0, 0, 0)),
                                n = o.viewport.npcToWorld(new Cesium.Cartesian3(1, 1, 1)),
                                r = (o.cartesianToCartographic(i), o.cartesianToCartographic(n)),
                                a = o.cartesianToCartographic(o.pointPositions[0]);
                            a.height = o.editMinHeight ? o.maxHeight : o.minHeight;
                            var s = o.cartesianToCartographic(o.pointPositions[0]);
                            s.height = r.height;
                            var l = o.viewport.scene.globe.ellipsoid,
                                c = l.cartographicToCartesian(a),
                                u = l.cartographicToCartesian(s);
                            if (o.viewport.isCameraOn) {
                                var d = o.viewport.worldToNpc(e.point);
                                d.z = o.viewport.worldToNpc(c).z, t = o.viewport.npcToWorld(d)
                            }
                            var p = new Cesium.Cartesian3(0, 0, 0);
                            p = o.editMinHeight ? Cesium.Cartesian3.fromDifferenceOf(c, u) : Cesium.Cartesian3.fromDifferenceOf(u, c), Cesium.Cartesian3.normalize(p, p);
                            var h = new Cesium.Ray(c, p),
                                f = Cesium.Plane.fromPointNormal(t, o.viewport.scene.camera.up),
                                m = Cesium.IntersectionTests.rayPlane(h, f);
                            if (void 0 !== m) {
                                var v = o.cartesianToCartographic(m);
                                o.editMinHeight ? (o.minHeight = o.getMin(v.height, o.maxHeight), void 0 !== o.minHeightText && (o.minHeightText.value = String(Bentley.Unit.convertUnits(o.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())))) : (o.maxHeight = o.getMax(v.height, o.minHeight), void 0 !== o.maxHeightText && (o.maxHeightText.value = String(Bentley.Unit.convertUnits(o.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())))), o.createClassification()
                            }
                        }, this.cartesianToCartographic = function(e) {
                            return o.viewport.scene.globe.ellipsoid.cartesianToCartographic(e)
                        }, this.getMin = function(e, t) {
                            return e < t ? e : t
                        }, this.getMax = function(e, t) {
                            return e < t ? t : e
                        }, this.createClassification = function() {
                            null != o.classifPrimitive && o.viewport.scene.primitives.remove(o.classifPrimitive), null != o.polygonEntity && o.entities.remove(o.polygonEntity);
                            var e = new Cesium.PolygonHierarchy(o.pointPositions),
                                t = new Cesium.PolygonGeometry({
                                    polygonHierarchy: e,
                                    extrudedHeight: o.minHeight,
                                    height: o.maxHeight
                                }),
                                i = new Cesium.GeometryInstance({
                                    geometry: t,
                                    attributes: {
                                        color: Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(0, 1, 0, .5))
                                    }
                                });
                            o.classifPrimitive = o.viewport.scene.primitives.add(new Cesium.ClassificationPrimitive({
                                geometryInstances: i,
                                asynchronous: !1,
                                classificationType: Cesium.ClassificationType.BOTH
                            })), o.polygonEntity = o.entities.add({
                                polygon: {
                                    extrudedHeight: o.minHeight,
                                    height: o.maxHeight,
                                    material: new Cesium.Color(0, 0, 1, .1),
                                    outline: !0,
                                    outlineColor: new Cesium.Color(0, 0, 1)
                                }
                            }), o.polygonEntity.polygon.hierarchy = new Cesium.CallbackProperty(function() {
                                return e
                            }, !1)
                        }, this.createClassificationToolSettings = function(e) {
                            var t = document.createElement("div");
                            t.className = "classification-tool", e.appendChild(t);
                            var i = document.createElement("div");
                            i.className = "classification-tool-title", i.textContent = "Edit Height", t.appendChild(i);
                            var r = document.createElement("div");
                            r.className = "classification-height-field", t.appendChild(r), r.appendChild(document.createTextNode("Min. Height")), r.appendChild(document.createElement("br")), o.minHeightText = document.createElement("input"), o.minHeightText.type = "text", o.minHeightText.className = "classification-height-text", o.minHeightText.value = String(Bentley.Unit.convertUnits(o.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), r.appendChild(o.minHeightText), o.minHeightText.onblur = function() {
                                var e = Bentley.Unit.convertUnits(Number(o.minHeightText.value), Bentley.Unit.getUnits(), Bentley.Unit.meter);
                                e < o.maxHeight ? (o.minHeight = e, o.createClassification()) : o.minHeightText.value = String(Bentley.Unit.convertUnits(o.minHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), o.updateHeightInProgress = !1
                            }, o.minHeightEditButton = document.createElement("button"), o.minHeightEditButton.type = "button", o.minHeightEditButton.className = "classification-heigth-button";
                            var a = document.createElement("i");
                            a.className = "edit-icon", o.minHeightEditButton.appendChild(a), o.minHeightEditButton.onclick = function() {
                                o.editMinHeight = !0, o.updateHeightInProgress = !0
                            }, o.minHeightEditButton.onblur = function() {
                                o.updateHeightInProgress = !1
                            }, r.appendChild(o.minHeightEditButton);
                            var s = document.createElement("div");
                            s.className = "classification-height-field", t.appendChild(s), s.appendChild(document.createTextNode("Max. Height")), s.appendChild(document.createElement("br")), o.maxHeightEditButton = document.createElement("button"), o.maxHeightEditButton.type = "button", o.maxHeightEditButton.className = "classification-heigth-button";
                            var l = document.createElement("i");
                            l.className = "edit-icon", o.maxHeightEditButton.appendChild(l), o.maxHeightEditButton.onclick = function() {
                                o.editMinHeight = !1, o.updateHeightInProgress = !0
                            }, o.maxHeightEditButton.onblur = function() {
                                o.updateHeightInProgress = !1
                            }, s.appendChild(o.maxHeightEditButton), o.maxHeightText = document.createElement("input"), o.maxHeightText.type = "text", o.maxHeightText.className = "classification-height-text", o.maxHeightText.value = String(Bentley.Unit.convertUnits(o.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), o.maxHeightText.onblur = function() {
                                var e = Bentley.Unit.convertUnits(Number(o.maxHeightText.value), Bentley.Unit.getUnits(), Bentley.Unit.meter);
                                e > o.minHeight ? (o.maxHeight = e, o.createClassification()) : o.maxHeightText.value = String(Bentley.Unit.convertUnits(o.maxHeight, Bentley.Unit.meter, Bentley.Unit.getUnits())), o.updateHeightInProgress = !1
                            }, s.appendChild(o.maxHeightText), t.appendChild(document.createElement("br"));
                            var c = document.createElement("input");
                            c.type = "button", c.value = "Done", c.className = "classification-tool-blueButton", c.onclick = function() {
                                o.classifInfo.minHeight = o.minHeight, o.classifInfo.maxHeight = o.maxHeight, o.viewport.scene.primitives.remove(o.classifInfo.primitive), n.ClassificationEntitiesManager.CreateClassificationPolygon(o.classifInfo, o.viewport), n.ClassificationEntitiesManager.updateClassificationEntitie(o.classifInfo), o.viewport.toolAdmin._primitiveTool.exitTool()
                            }, t.appendChild(c);
                            var u = document.createElement("input");
                            u.type = "button", u.value = "Cancel", u.className = "classification-tool-button", u.onclick = function() {
                                o.viewport.toolAdmin._primitiveTool.exitTool()
                            }, t.appendChild(u)
                        }, Bentley.PrimitiveTool.call(this, e), this.id = "EditClassificationTool", this.viewport = e, this.dataSourceCollection = new Cesium.DataSourceCollection, this.dataSourceDisplay = new Cesium.DataSourceDisplay({
                            scene: this.viewport.scene,
                            dataSourceCollection: this.dataSourceCollection
                        }), this.entities = this.dataSourceDisplay.defaultDataSource.entities, this.pointPositions = [], this.minHeight = 0, this.maxHeight = 0, this.classifPrimitive = null, this.polygonEntity = null, this.editMinHeight = !1, this.updateHeightInProgress = !1, this.turnCameraOn = !1, this.classifInfo = n.ClassificationEntitiesManager.getOneClassificationInfos(i), void 0 !== this.classifInfo) {
                        for (var r = 0, a = this.classifInfo.points; r < a.length; r++) {
                            var s = a[r];
                            this.pointPositions.push(s)
                        }
                        this.minHeight = this.classifInfo.minHeight, this.maxHeight = this.classifInfo.maxHeight, this.createClassification()
                    }
                    this.eventHelper = new Cesium.EventHelper, this.eventHelper.add(e.BimAdmin.viewer.cesiumWidget.clock.onTick, function(e) {
                        o.dataSourceDisplay.update(e), o.dataSourceDisplay.scene.requestRenderMode && o.dataSourceDisplay.scene.requestRender()
                    }, this), this.classificationContainer = document.createElement("div"), this.classificationContainer.className = "classification-tool-container", this.viewport.BimAdmin.viewer._container.appendChild(this.classificationContainer), this.createClassificationToolSettings(this.classificationContainer)
                }
            }();
        i.EditClassificationTool = o;
        var r = function() {
            return function() {
                this.getToolConstructor = function() {
                    return Bentley.extend(Bentley.PrimitiveTool, o), o
                }
            }
        }();
        i.EditClassificationToolWrapper = r
    }, {
        "./../Tools/ClassificationEntitiesManager": 9
    }],
    13: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./PhotoNavigation"),
            o = e("./Object/PhotoCameraFrustum"),
            r = e("./ThumbNailPanel"),
            a = function() {
                function e(t, i) {
                    var r = this;
                    this.interestPointPosition = void 0, this.onPostInstall = function() {
                        var e = r.getTilesetName();
                        void 0 !== e ? (r.createThreePanelView(), n.PhotoNavigation.loadFromXML(e, r.viewport, r), r.openToolSettingsPanel()) : (alert("No photo available for this TileSet"), r.viewport.toolAdmin._primitiveTool.exitTool())
                    }, this.handleKeyPress = function(e) {
                        r.thumbNailPanel.handleKeyPress(e)
                    }, Bentley.PrimitiveTool.call(this, t), this.id = "Vivo.BrowsePhotoTool", this.viewport = t, this._selectedFrustum = new o.PhotoCameraFrustum(t), this._temporaryFrustum = new o.PhotoCameraFrustum(t), this.dataSourceCollection = new Cesium.DataSourceCollection, this.dataSourceDisplay = new Cesium.DataSourceDisplay({
                        scene: this.viewport.scene,
                        dataSourceCollection: this.dataSourceCollection
                    }), this.entities = this.dataSourceDisplay.defaultDataSource.entities, this.eventHelper = new Cesium.EventHelper, this.firstLoad = !0, this.panInProgress = !1, this.eventHelper.add(t.BimAdmin.viewer.cesiumWidget.clock.onTick, function(e) {
                        r.dataSourceDisplay.update(e)
                    }, this), this.viewport.cameraToggled.addEventListener(function() {
                        if (r.viewport.isCameraOn && e.orientToPhoto) {
                            window.alert("The option Rotate with photo is not allow when camera is ON. This option will be turn OFF!"), e.orientToPhoto = !1;
                            var t = document.getElementById("photonavOrientCheckbox");
                            null != t && (t.checked = e.orientToPhoto)
                        }
                    })
                }
                return e.prototype.pickFrustum = function(e) {
                    var t = this.viewport.pickEntity(e.viewPoint);
                    if (Cesium.defined(t) && Cesium.defined(t.primitive) && Cesium.defined(t.id) && Cesium.defined(t.id.split)) {
                        var i = t.id.split(" ");
                        if (2 === i.length && ("frustum" === i[0] || "outline" === i[0])) {
                            var o = parseFloat(i[1]);
                            return n.PhotoNavigation.getPhotoInfo(o)
                        }
                    }
                }, e.prototype.onCleanup = function(e) {
                    this.clearHighlightFrustum(), this.closeImage(), this.closeToolSettingsPanel(), n.PhotoNavigation.cleanup(), this.eventHelper.removeAll(), this.dataSourceDisplay = this.dataSourceDisplay.destroy(), this.dataSourceCollection = this.dataSourceCollection.destroy(), this.removeThreePanelView(), this.viewport.BimAdmin.viewer.scene.requestRender()
                }, e.prototype.getCursor = function() {
                    return Bentley.Cursor.CrossHair
                }, e.prototype.onModelMotion = function(e) {
                    var t = this.pickFrustum(e);
                    if (void 0 !== t) this._temporaryFrustum.CreateFrustum(t), this.openThumbNail(t, e), this.viewport.BimAdmin.viewer.scene.requestRender();
                    else {
                        var i = !1;
                        Cesium.defined(this.thumbNailContainer) && (this.viewport.BimAdmin.viewer._container.removeChild(this.thumbNailContainer), this.thumbNailContainer = void 0, i = !0), this._temporaryFrustum.IsDefined() && (this._temporaryFrustum.DeleteFrustum(), i = !0), i && this.viewport.BimAdmin.viewer.scene.requestRender()
                    }
                    return !0
                }, e.prototype.onDataButtonDown = function(t) {
                    if (Bentley.InputEventModifiers.None !== t.keyModifiers) return !1;
                    var i = this.viewport.scene;
                    if (!i.pickPositionSupported) return !1;
                    var o = this.pickFrustum(t);
                    if (void 0 !== o) return this.openImage(o), this.thumbNailPanel.highlightThumbnail(o), this.flash(o), e.orientToPhoto && (this.viewport.isCameraOn ? this.orientCameraPerspectiveProjection(o) : this.orientCamera(o)), !0;
                    var r = i.pickPosition(t.viewPoint);
                    if (!Cesium.defined(r)) return !1;
                    var a = this.approximateNormal(t, r.clone());
                    return !!Cesium.defined(a) && (Cesium.Cartesian3.normalize(a, a), Cesium.defined(this.interestPointEntity) && this.entities.remove(this.interestPointEntity), this.interestPointEntity = this.entities.add({
                        position: r,
                        point: {
                            color: new Cesium.Color(0, .5451, 1, 1),
                            pixelSize: 8
                        }
                    }), n.PhotoNavigation.activateInterestPoint(r.clone(), a), this.thumbNailPanel.updateGrid(r, a), n.PhotoNavigation.isActivePhotos() ? this.thumbNailPanel.highlightThumbnail(o) : (this.clearHighlightFrustum(), this.thumbNailPanel.unhighlightThumbnail()), !0)
                }, e.prototype.onResetButtonDown = function(e) {
                    return Cesium.defined(this.interestPointEntity) && this.entities.remove(this.interestPointEntity), Cesium.defined(this.interestPointCanvas) && (this.imageCanvas.removeChild(this.interestPointCanvas), this.interestPointCanvas = void 0), n.PhotoNavigation.deactivateInterestPoint(), this.clearHighlightFrustum(), this.closeImage(), this.thumbNailPanel.generateGrid(), this.thumbNailPanel.unhighlightThumbnail(), this.viewport.scene.requestRender(), !0
                }, e.prototype.samplePoints = function(e) {
                    for (var t = this.viewport.scene, i = new Cesium.Cartesian2(e.viewPoint.x, e.viewPoint.y), n = [], o = 0; o < 3; ++o)
                        for (var r = 0; r < 3; ++r) {
                            i.x = 0 === o ? e.viewPoint.x + 1 : 2 === o ? e.viewPoint.x - 1 : e.viewPoint.x, i.y = 0 === r ? e.viewPoint.y + 1 : 2 === r ? e.viewPoint.y - 1 : e.viewPoint.y;
                            var a = t.pickPosition(i);
                            Cesium.defined(a) && n.push(a)
                        }
                    return n
                }, e.prototype.approximateNormal = function(e, t) {
                    var i = this.viewport.scene,
                        n = this.samplePoints(e),
                        o = n.length;
                    if (!(o < 3)) {
                        for (var r = new Cesium.Cartesian3, a = 0; a < o; ++a) Cesium.defined(n[a]) && (r.x += n[a].x, r.y += n[a].y, r.z += n[a].z);
                        r.x /= o, r.y /= o, r.z /= o;
                        for (var s = 0, l = n; s < l.length; s++) {
                            var c = l[s];
                            Cesium.defined(c) && Cesium.Cartesian3.subtract(c, r, c)
                        }
                        for (var u = 0, d = 0, p = 0, h = 0, f = 0, m = 0, v = 0, g = n; v < g.length; v++) {
                            var y = g[v];
                            u += y.x * y.x, d += y.x * y.y, p += y.y * y.y, h += y.x * y.z, f += y.y * y.z, m += y.z * y.z
                        }
                        var C = p * m - f * f,
                            b = u * m - h * h,
                            x = u * p - d * d,
                            w = Math.max(C, b, x);
                        if (!(w <= 0)) {
                            var T;
                            T = w === C ? new Cesium.Cartesian3(C, h * f - d * m, d * f - h * p) : w === b ? new Cesium.Cartesian3(h * f - d * m, b, d * h - f * u) : new Cesium.Cartesian3(d * f - h * p, d * h - f * u, x);
                            var E = new Cesium.Cartesian3;
                            return Cesium.Cartesian3.subtract(i.camera.positionWC, t, E), Cesium.Cartesian3.dot(E, T) < 0 && Cesium.Cartesian3.negate(T, T), T
                        }
                    }
                }, e.prototype.flash = function(e) {
                    this._selectedFrustum.CreateFrustum(e)
                }, e.prototype.createTemporaryFrustum = function(e) {
                    this._temporaryFrustum.CreateFrustum(e), this.viewport.BimAdmin.viewer.scene.requestRender()
                }, e.prototype.deleteTemporaryFrustum = function() {
                    this._temporaryFrustum.DeleteFrustum(), this.viewport.BimAdmin.viewer.scene.requestRender()
                }, e.prototype.clearHighlightFrustum = function() {
                    this._selectedFrustum.IsDefined() && this._selectedFrustum.DeleteFrustum()
                }, e.prototype.orientCameraPerspectiveProjection = function(e) {
                    for (var t = new Bentley.Frustum, i = e.frustumCorners(1), n = e.frustumCorners(10), o = 0; o < 4; ++o) Cesium.Cartesian3.add(i[o], e.origin, i[o]), Cesium.Cartesian3.add(n[o], e.origin, n[o]), t.pts[o] = i[o], t.pts[o + 4] = n[o];
                    this.viewport.setupFromFrustum(t)
                }, e.prototype.orientCamera = function(e) {
                    for (var t = new Bentley.Frustum, i = n.PhotoNavigation.getInterestPoint(), o = Cesium.defined(i) ? Cesium.Cartesian3.distance(i, e.origin) : 50, r = e.frustumCorners(o), a = 0; a < 4; ++a) Cesium.Cartesian3.add(r[a], e.origin, r[a]), t.pts[a + 4] = r[a], t.pts[a] = r[a].clone(), t.pts[a].x += o * e.rotation.M20, t.pts[a].y += o * e.rotation.M21, t.pts[a].z += o * e.rotation.M22;
                    this.viewport.setupFromFrustum(t)
                }, e.prototype.getTilesetName = function() {
                    return this.viewport.BimAdmin.bim.blocksExchangeFile
                }, e.prototype.markInterestPoint = function() {
                    this.interestPointPosition = n.PhotoNavigation.projectInterestPointToPhoto(e.imageId)
                }, e.prototype.openImage = function(t) {
                    var i = this;
                    e.imageId = t.id, Cesium.defined(this.imageCanvas) ? this.imageElement.src = t.url : (Cesium.defined(this.thumbNailContainer) && (this.viewport.BimAdmin.viewer._container.removeChild(this.thumbNailContainer), this.thumbNailContainer = void 0), this.addKeyDownEventListener(), this.imageCanvas = document.createElement("canvas"), this.imageCanvas.width = this.threePanelImage.clientWidth, this.imageCanvas.height = this.threePanelImage.clientHeight, this.trackTransforms(this.imageCanvas.getContext("2d")), this.imageCanvas.onmousedown = function(e) {
                        if (!0 === i.panInProgress || 1 === e.button) {
                            var t = i.imageCanvas.getContext("2d");
                            i.lastX = e.offsetX || e.pageX - i.imageCanvas.offsetLeft, i.lastY = e.offsetY || e.pageY - i.imageCanvas.offsetTop, i.dragStart = t.transformedPoint(i.lastX, i.lastY), i.dragged = !1
                        }
                        return !1
                    }, this.imageCanvas.onmousemove = function(e) {
                        e.preventDefault(), e.stopPropagation(), i.lastX = e.offsetX || e.pageX - i.imageCanvas.offsetLeft, i.lastY = e.offsetY || e.pageY - i.imageCanvas.offsetTop, i.dragged = !0;
                        var t = i.imageCanvas.getContext("2d");
                        if (i.dragStart) {
                            var n = t.transformedPoint(i.lastX, i.lastY);
                            t.translate(n.x - i.dragStart.x, n.y - i.dragStart.y), i.redraw()
                        }
                        return !1
                    }, this.imageCanvas.onmouseup = function(e) {
                        return i.dragStart = null, !1
                    }, this.imageCanvas.onmousewheel = function(t) {
                        if (void 0 !== n.PhotoNavigation.getPhotoInfo(e.imageId)) {
                            var o = t.wheelDelta ? t.wheelDelta / 40 : t.detail ? -t.detail : 0;
                            if (o) {
                                var r = i.imageCanvas.getContext("2d"),
                                    a = r.transformedPoint(i.lastX, i.lastY);
                                r.translate(a.x, a.y);
                                var s = Math.pow(1.1, o);
                                r.scale(s, s), r.translate(-a.x, -a.y), i.redraw()
                            }
                            return t.preventDefault() && !1
                        }
                        return !1
                    }, this.imageCanvas.onclick = function(e) {
                        i.markInterestPoint(), i.redraw()
                    }, this.toolElement = document.createElement("div"), this.imageElement = document.createElement("img"), this.imageElement.onload = function() {
                        i.markInterestPoint(), !0 === i.firstLoad ? (i.fitImage(), i.firstLoad = !1) : i.redraw()
                    }, this.imageElement.src = t.url, this.threePanelImage.appendChild(this.imageCanvas))
                }, e.prototype.redraw = function() {
                    if (this.imageCanvas) {
                        var t = this.imageCanvas.getContext("2d");
                        if (null === t) return;
                        var i = t.transformedPoint(0, 0),
                            o = t.transformedPoint(this.imageCanvas.width, this.imageCanvas.height);
                        if (t.clearRect(i.x, i.y, o.x - i.x, o.y - i.y), t.save(), t.setTransform(1, 0, 0, 1, 0, 0), t.clearRect(this.imageElement, 0, 0, this.imageCanvas.width, this.imageCanvas.height), t.restore(), t.drawImage(this.imageElement, 0, 0, this.imageElement.width, this.imageElement.height, 0, 0, this.imageElement.width, this.imageElement.height), void 0 !== this.interestPointPosition) {
                            var r = n.PhotoNavigation.getPhotoInfo(e.imageId);
                            if (void 0 !== r) {
                                var a = 500 * t.canvas.width / r.pixelWidth;
                                t.beginPath(), t.lineWidth = 10, t.strokeStyle = "#008be1", t.arc(this.interestPointPosition.x, this.interestPointPosition.y, a, 0, 2 * Math.PI), t.stroke(), t.beginPath(), t.moveTo(this.interestPointPosition.x - 1.5 * a, this.interestPointPosition.y), t.lineTo(this.interestPointPosition.x - a / 2, this.interestPointPosition.y), t.stroke(), t.beginPath(), t.moveTo(this.interestPointPosition.x + 1.5 * a, this.interestPointPosition.y), t.lineTo(this.interestPointPosition.x + a / 2, this.interestPointPosition.y), t.stroke(), t.beginPath(), t.moveTo(this.interestPointPosition.x, this.interestPointPosition.y - 1.5 * a), t.lineTo(this.interestPointPosition.x, this.interestPointPosition.y - a / 2), t.stroke(), t.beginPath(), t.moveTo(this.interestPointPosition.x, this.interestPointPosition.y + 1.5 * a), t.lineTo(this.interestPointPosition.x, this.interestPointPosition.y + a / 2), t.stroke()
                            }
                        }
                    }
                }, e.prototype.trackTransforms = function(e) {
                    var t = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
                        i = t.createSVGMatrix();
                    e.getTransform = function() {
                        return i
                    };
                    var n = [],
                        o = e.save;
                    e.save = function() {
                        return n.push(i.translate(0, 0)), o.call(e)
                    };
                    var r = e.restore;
                    e.restore = function() {
                        return i = n.pop(), r.call(e)
                    };
                    var a = e.scale;
                    e.scale = function(t, n) {
                        return i = i.scaleNonUniform(t, n), a.call(e, t, n)
                    };
                    var s = e.rotate;
                    e.rotate = function(t) {
                        return i = i.rotate(180 * t / Math.PI), s.call(e, t)
                    };
                    var l = e.translate;
                    e.translate = function(t, n) {
                        return i = i.translate(t, n), l.call(e, t, n)
                    };
                    var c = e.transform;
                    e.transform = function(n, o, r, a, s, l) {
                        var u = t.createSVGMatrix();
                        return u.a = n, u.b = o, u.c = r, u.d = a, u.e = s, u.f = l, i = i.multiply(u), c.call(e, n, o, r, a, s, l)
                    };
                    var u = e.setTransform;
                    e.setTransform = function(t, n, o, r, a, s) {
                        return i.a = t, i.b = n, i.c = o, i.d = r, i.e = a, i.f = s, u.call(e, t, n, o, r, a, s)
                    };
                    var d = t.createSVGPoint();
                    e.transformedPoint = function(e, t) {
                        return d.x = e, d.y = t, d.matrixTransform(i.inverse())
                    }
                }, e.prototype.addKeyDownEventListener = function() {
                    document.addEventListener("keydown", this.handleKeyPress)
                }, e.prototype.closeImage = function() {
                    void 0 !== this.imageCanvas && (this.threePanelImage.removeChild(this.imageCanvas), this.imageCanvas = void 0), this.interestPointCanvas = void 0, this.firstLoad = !0, document.removeEventListener("keydown", this.handleKeyPress)
                }, e.prototype.openThumbNail = function(e, t) {
                    Cesium.defined(this.thumbNailContainer) && (this.viewport.BimAdmin.viewer._container.removeChild(this.thumbNailContainer), this.thumbNailContainer = void 0), this.thumbNailContainer = document.createElement("div"), this.thumbNailContainer.className = "photonavigation-thumbnail-container", this.thumbNailContainer.style.top = t.viewPoint.y + 20 + "px", this.thumbNailContainer.style.left = t.viewPoint.x + 20 + "px";
                    var i = document.createElement("div");
                    i.className = "photonavigation-tool", this.thumbNailImageElement = document.createElement("img"), this.thumbNailImageElement.src = e.url_tn, this.thumbNailImageElement.className = "photonavigation-thumbnail-canvas", i.appendChild(this.thumbNailImageElement), this.thumbNailContainer.appendChild(i), this.viewport.BimAdmin.viewer._container.appendChild(this.thumbNailContainer)
                }, e.prototype.addToggleSetting = function(e) {
                    var t = document.createElement("p"),
                        i = document.createElement("input");
                    i.id = e.id, i.type = "checkbox", i.className = "photonavigation-toggle", i.checked = e.checked, i.onclick = e.onclick, t.appendChild(i);
                    var n = document.createElement("label");
                    n.htmlFor = e.id, n.innerText = e.labelText, n.className = "photonavigation-label", t.appendChild(n), e.parent.appendChild(t)
                }, e.prototype.openToolSettingsPanel = function() {
                    var t = this;
                    this.settingsContainer = document.createElement("div"), this.settingsContainer.className = "photonavigation-settings-container2";
                    var i = document.createElement("div");
                    i.className = "photonavigation-tool", this.settingsContainer.appendChild(i), this.addToggleSetting({
                        id: "photonavOrientCheckbox",
                        checked: e.orientToPhoto,
                        labelText: "Rotate with photo",
                        onclick: function() {
                            if (t.viewport.isCameraOn)
                                if (window.confirm("The option Rotate with photo is not allow when camera is ON.  We will turn OFF the camera?")) t.viewport.BimAdmin.widgetStore.getAssetById("Bim.Settings").children["Camera.Checkbox"].viewModel.active = !1, t.viewport.BimAdmin.viewer.scene.requestRender(), e.orientToPhoto = !e.orientToPhoto;
                                else {
                                    var i = document.getElementById("photonavOrientCheckbox");
                                    null != i && (i.checked = e.orientToPhoto)
                                }
                            else e.orientToPhoto = !e.orientToPhoto
                        },
                        parent: i
                    }), this.addToggleSetting({
                        id: "photonavShowCheckbox",
                        checked: n.PhotoNavigation.isFrustumDisplay(),
                        labelText: "Show all cameras",
                        onclick: function() {
                            n.PhotoNavigation.toggleFrustumDisplay(t.viewport)
                        },
                        parent: i
                    });
                    var o = document.createElement("input");
                    o.type = "button", o.className = "photonavigation-setting-button", o.value = "+", o.id = "photonavGrowButton", o.onclick = function() {
                        n.PhotoNavigation.growFrustums(t.viewport)
                    }, i.appendChild(o);
                    var r = document.createElement("input");
                    r.type = "button", r.className = "photonavigation-setting-button", r.value = "-", r.id = "photonavShrinkButton", r.onclick = function() {
                        n.PhotoNavigation.shrinkFrustums(t.viewport)
                    }, i.appendChild(r);
                    var a = document.createElement("label");
                    a.htmlFor = "photonavShrinkButton", a.innerText = "Camera size", a.className = "photonavigation-label", i.appendChild(a), this.viewport.BimAdmin.viewer._container.appendChild(this.settingsContainer)
                }, e.prototype.closeToolSettingsPanel = function() {
                    void 0 !== this.settingsContainer && (this.viewport.BimAdmin.viewer._container.removeChild(this.settingsContainer), this.settingsContainer = void 0)
                }, e.prototype.createThreePanelView = function() {
                    var e = this,
                        t = this.viewport.BimAdmin.viewer._container;
                    if (null !== t) {
                        this.photonavigationWrapper = document.createElement("div"), this.photonavigationWrapper.className = "photonavigation-tool-container";
                        var i = document.createElement("div");
                        i.id = "threePanel", i.style.height = "100%", i.style.width = "100%", this.photonavigationWrapper.appendChild(i);
                        var n = t.parentNode;
                        if (null !== n) {
                            n.replaceChild(this.photonavigationWrapper, t), this.threePanelTop = document.createElement("div"), this.threePanelTop.id = "topPanel", this.threePanelTop.style.width = "100%", this.threePanelTop.style.height = "50%", this.threePanelImage = document.createElement("div"), this.threePanelImage.id = "imagePanel", this.threePanelImage.className = "photonavigation-image-panel", this.threePanelBimium = document.createElement("div"), this.threePanelBimium.id = "bimiumPanel", this.threePanelBimium.className = "photonavigation-bimium-panel", this.threePanelThumbnail = document.createElement("div"), this.threePanelThumbnail.className = "photonavigation-thumbnail-panel", this.threePanelThumbnail.id = "thumbnailPanel", this.thumbNailPanel = new r.PhotoNavThumbnailPanel(this.threePanelThumbnail, this), this.threePanelBimium.appendChild(t), this.threePanelTop.appendChild(this.threePanelBimium), this.threePanelTop.appendChild(this.threePanelImage), i.appendChild(this.threePanelTop), i.appendChild(this.threePanelThumbnail), this.verticalSplitter = $("#threePanel").kendoSplitter({
                                orientation: "vertical",
                                panes: [{
                                    collapsible: !0,
                                    resizable: !0
                                }, {
                                    collapsible: !0,
                                    resizable: !0,
                                    size: "30%"
                                }],
                                resize: function() {
                                    e.thumbNailPanel.highlightThumbnail(), void 0 !== e.threePanelImage && void 0 !== e.imageCanvas && (e.imageCanvas.height = e.threePanelImage.clientHeight, e.fitImage())
                                }
                            }), this.horizontalSplitter = $("#topPanel").kendoSplitter({
                                orientation: "horizontal",
                                panes: [{
                                    collapsible: !0,
                                    resizable: !0
                                }, {
                                    collapsible: !0,
                                    resizable: !0
                                }],
                                resize: function() {
                                    e.markInterestPoint(), void 0 !== e.threePanelImage && void 0 !== e.imageCanvas && (e.imageCanvas.width = e.threePanelImage.clientWidth, e.fitImage())
                                }
                            });
                            var o = document.createElement("div");
                            o.className = "photonavigation-settings-photo-container", this.photonavigationWrapper.appendChild(o);
                            var a = document.createElement("button");
                            a.type = "button", a.className = "photonavigation-button2", a.title = "Previous";
                            var s = document.createElement("i");
                            s.className = "bim-icon-arrow-left", a.appendChild(s), a.onclick = function() {
                                e.thumbNailPanel.getPrevPhoto()
                            }, o.appendChild(a);
                            var l = document.createElement("button");
                            l.type = "button", l.className = "photonavigation-button2", l.title = "Next";
                            var c = document.createElement("i");
                            c.className = "bim-icon-arrow-right", l.appendChild(c), l.onclick = function() {
                                e.thumbNailPanel.getNextPhoto()
                            }, o.appendChild(l);
                            var u = document.createElement("button");
                            u.type = "button", u.className = "photonavigation-button2", u.title = "Fit";
                            var d = document.createElement("i");
                            d.className = "bim-icon-fitview", u.appendChild(d), u.onclick = function() {
                                e.fitImage()
                            }, o.appendChild(u);
                            var p = document.createElement("button");
                            p.type = "button", p.className = "photonavigation-button2", p.title = "Pan";
                            var h = document.createElement("i");
                            h.className = "bim-icon-pan", p.appendChild(h), p.onclick = function() {
                                e.panInProgress = !0
                            }, p.onblur = function() {
                                e.panInProgress = !1
                            }, o.appendChild(p), $("#bimiumPanel").css({
                                "overflow-x": "hidden",
                                "overflow-y": "hidden"
                            })
                        }
                    }
                }, e.prototype.fitImage = function() {
                    var t = n.PhotoNavigation.getPhotoInfo(e.imageId);
                    if (void 0 !== t) {
                        var i = this.imageCanvas.width / t.pixelWidth,
                            o = this.imageCanvas.height / t.pixelHeight,
                            r = Math.min(i, o),
                            a = (this.imageCanvas.width - t.pixelWidth * r) / 2,
                            s = (this.imageCanvas.height - t.pixelHeight * r) / 2;
                        this.imageCanvas.getContext("2d").setTransform(r, 0, 0, r, a, s), this.redraw()
                    }
                }, e.prototype.openThreePanelLayoutSettingsPanel = function() {
                    var e = this;
                    this.layoutSettingsContainer = document.createElement("div"), this.layoutSettingsContainer.className = "photonavigation-settings-container", this.layoutSettingsContainer.style.right = this.layoutButton.offsetWidth + this.layoutButton.offsetLeft + "px", this.layoutSettingsContainer.style.top = this.layoutButton.offsetHeight + this.layoutButton.offsetTop + "px";
                    var t = document.createElement("div");
                    t.className = "photonavigation-tool", this.layoutSettingsContainer.appendChild(t);
                    for (var i = function(i) {
                            var n = document.createElement("input");
                            n.type = "button", n.className = "photonavigation-button";
                            var o = "";
                            i.bimium ? o += "On, " : o += "Off, ", i.image ? o += "On, " : o += "Off, ", i.thumbnail ? o += "On" : o += "Off", n.value = o, n.onclick = function() {
                                e.toggleLayoutThreePanelView(i.bimium, i.image, i.thumbnail)
                            }, t.appendChild(n)
                        }, n = 0, o = [{
                            bimium: !0,
                            image: !0,
                            thumbnail: !0
                        }, {
                            bimium: !1,
                            image: !0,
                            thumbnail: !0
                        }, {
                            bimium: !1,
                            image: !1,
                            thumbnail: !0
                        }, {
                            bimium: !0,
                            image: !1,
                            thumbnail: !1
                        }, {
                            bimium: !1,
                            image: !0,
                            thumbnail: !1
                        }, {
                            bimium: !0,
                            image: !0,
                            thumbnail: !1
                        }]; n < o.length; n++) {
                        i(o[n])
                    }
                    this.photonavigationWrapper.appendChild(this.layoutSettingsContainer)
                }, e.prototype.closeThreePanelLayoutSettingsPanel = function() {
                    Cesium.defined(this.layoutSettingsContainer) && (this.photonavigationWrapper.removeChild(this.layoutSettingsContainer), this.layoutSettingsContainer = void 0)
                }, e.prototype.toggleLayoutThreePanelView = function(e, t, i) {
                    var n = this.verticalSplitter.data("kendoSplitter");
                    n.expand(this.threePanelThumbnail), n.expand(this.threePanelTop);
                    var o = this.horizontalSplitter.data("kendoSplitter");
                    o.expand(this.threePanelImage), o.expand(this.threePanelBimium), i || n.collapse(this.threePanelThumbnail), e || t ? (t || o.collapse(this.threePanelImage), e || o.collapse(this.threePanelBimium)) : n.collapse(this.threePanelTop)
                }, e.prototype.removeThreePanelView = function() {
                    void 0 !== this.photonavigationWrapper && this.photonavigationWrapper.parentElement.replaceChild(this.viewport.BimAdmin.viewer._container, this.photonavigationWrapper), this.photonavigationWrapper = void 0, this.threePanelBimium = void 0, this.threePanelImage = void 0, this.threePanelThumbnail = void 0, this.threePanelTop = void 0
                }, e.prototype.updateImageSelection = function(t) {
                    e.imageId = t;
                    var i = n.PhotoNavigation.getPhotoInfo(t);
                    void 0 === i ? (this.clearHighlightFrustum(), this.closeImage()) : (e.orientToPhoto && this.orientCamera(i), this._selectedFrustum.CreateFrustum(i), this.openImage(i)), this.viewport.BimAdmin.viewer.scene.requestRender()
                }, e.prototype.onFileLoaded = function() {
                    this.thumbNailPanel.generateGrid()
                }, e.orientToPhoto = !1, e.imageId = -1, e
            }();
        i.BrowsePhotoTool = a;
        var s = function() {
            return function() {
                this.getToolConstructor = function() {
                    return Bentley.extend(Bentley.PrimitiveTool, a), a
                }
            }
        }();
        i.BrowsePhotoToolWrapper = s
    }, {
        "./Object/PhotoCameraFrustum": 16,
        "./PhotoNavigation": 19,
        "./ThumbNailPanel": 20
    }],
    14: [function(e, t, i) {
        "use strict"
    }, {}],
    15: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = function() {
            function e(e, t, i) {
                this.planes = Cesium.defined(t) ? t : [new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0)];
                try {
                    var n = Cesium.defined(i) ? e.frustumCorners(1) : e.frustumCorners(1, i),
                        o = new Cesium.Cartesian3(0, 0, 0);
                    Cesium.Cartesian3.cross(n[3], n[1], o), Cesium.Cartesian3.normalize(o, o), Cesium.Plane.fromPointNormal(e.origin, o, this.planes[0]), Cesium.Cartesian3.cross(n[0], n[2], o), Cesium.Cartesian3.normalize(o, o), Cesium.Plane.fromPointNormal(e.origin, o, this.planes[1]), Cesium.Cartesian3.cross(n[2], n[3], o), Cesium.Cartesian3.normalize(o, o), Cesium.Plane.fromPointNormal(e.origin, o, this.planes[2]), Cesium.Cartesian3.cross(n[1], n[0], o), Cesium.Cartesian3.normalize(o, o), Cesium.Plane.fromPointNormal(e.origin, o, this.planes[3])
                } catch (e) {
                    this.planes = []
                }
            }
            return e.prototype.PointInside = function(e) {
                if (0 === this.planes.length) return !1;
                for (var t = 0, i = this.planes; t < i.length; t++) {
                    var n = i[t];
                    if (Cesium.Plane.getPointDistance(n, e) < 0) return !1
                }
                return !0
            }, e
        }();
        i.ClipFrustum = n
    }, {}],
    16: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = function() {
            function e(e) {
                this._frustum = null, this._frustumOutline = null, this._viewport = null, this._viewport = e
            }
            return Object.defineProperty(e.prototype, "frustum", {
                get: function() {
                    return this._frustum
                },
                set: function(e) {
                    this._frustum = e
                },
                enumerable: !0,
                configurable: !0
            }), Object.defineProperty(e.prototype, "frustumOutline", {
                get: function() {
                    return this._frustumOutline
                },
                set: function(e) {
                    this._frustumOutline = e
                },
                enumerable: !0,
                configurable: !0
            }), e.prototype.IsDefined = function() {
                return void 0 !== this._frustum && null !== this._frustum && void 0 !== this._frustumOutline && null !== this._frustumOutline
            }, e.prototype.DeleteFrustum = function() {
                this._viewport.scene.primitives.remove(this._frustum), this._viewport.scene.primitives.remove(this._frustumOutline), this._frustum = void 0, this._frustumOutline = void 0
            }, e.prototype.CreateFrustum = function(e) {
                this.IsDefined() && this.DeleteFrustum();
                var t = e.rotation,
                    i = new Cesium.Matrix3(t.M00, t.M01, t.M02, t.M10, t.M11, t.M12, t.M20, t.M21, t.M22),
                    n = new Cesium.Matrix3;
                Cesium.Matrix3.inverse(i, n);
                var o = Cesium.Quaternion.fromRotationMatrix(n),
                    r = new Cesium.PerspectiveFrustum({
                        fov: e.fieldOfViewX > e.fieldOfViewY ? 2 * e.fieldOfViewX : 2 * e.fieldOfViewY,
                        aspectRatio: e.pixelWidth / e.pixelHeight,
                        near: .1,
                        far: 100
                    }),
                    a = new Cesium.FrustumGeometry({
                        frustum: r,
                        origin: e.origin,
                        orientation: o,
                        vertexFormat: Cesium.VertexFormat.POSITION_ONLY
                    }),
                    s = new Cesium.GeometryInstance({
                        geometry: a,
                        attributes: {
                            color: Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(0, .5451, 1, .05))
                        }
                    });
                this._frustum = this._viewport.scene.primitives.add(new Cesium.Primitive({
                    geometryInstances: s,
                    appearance: new Cesium.PerInstanceColorAppearance({
                        closed: !0,
                        flat: !0
                    }),
                    asynchronous: !1
                })), this.frustumOutline = this._viewport.scene.primitives.add(new Cesium.Primitive({
                    geometryInstances: new Cesium.GeometryInstance({
                        geometry: new Cesium.FrustumOutlineGeometry({
                            frustum: r,
                            origin: e.origin,
                            orientation: o
                        }),
                        attributes: {
                            color: Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(1, 1, 0, 1))
                        }
                    }),
                    appearance: new Cesium.PerInstanceColorAppearance({
                        flat: !0
                    }),
                    asynchronous: !1
                }))
            }, e
        }();
        i.PhotoCameraFrustum = n
    }, {}],
    17: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../PhotoNavigation"),
            o = function() {
                function e(e, t, i, n, o, r, a, s, l, c) {
                    this.id = e, this.url = t, this.url_tn = i, this.origin = n, this.rotation = o, this.focalLength = r, this.sensorSize = a, this.pixelWidth = s, this.pixelHeight = l, this.distortion = c, this.rank = NaN, s > l ? (this.fieldOfViewX = Math.atan2(a, 2 * r), this.fieldOfViewY = Math.atan2(a * (l / s), 2 * r)) : (this.fieldOfViewX = Math.atan2(a * (s / l), 2 * r), this.fieldOfViewY = Math.atan2(a, 2 * r))
                }
                return e.prototype.frustumCorners = function(e, t) {
                    var i = void 0 !== t ? t : [new Cesium.Cartesian3, new Cesium.Cartesian3, new Cesium.Cartesian3, new Cesium.Cartesian3],
                        n = e * this.rotation.M00,
                        o = e * this.rotation.M01,
                        r = e * this.rotation.M02,
                        a = -1 * e * this.rotation.M10,
                        s = -1 * e * this.rotation.M11,
                        l = -1 * e * this.rotation.M12,
                        c = -1 * e * this.rotation.M20,
                        u = -1 * e * this.rotation.M21,
                        d = -1 * e * this.rotation.M22,
                        p = Math.tan(this.fieldOfViewX),
                        h = Math.tan(this.fieldOfViewY);
                    return Cesium.Cartesian3.fromElements(c - p * n - h * a, u - p * o - h * s, d - p * r - h * l, i[0]), Cesium.Cartesian3.fromElements(c + p * n - h * a, u + p * o - h * s, d + p * r - h * l, i[1]), Cesium.Cartesian3.fromElements(c - p * n + h * a, u - p * o + h * s, d - p * r + h * l, i[2]), Cesium.Cartesian3.fromElements(c + p * n + h * a, u + p * o + h * s, d + p * r + h * l, i[3]), i
                }, e.prototype.rankPhotoByInterestPoint = function(e, t, i) {
                    if (!Cesium.defined(e) || !Cesium.defined(t)) return this.rank = NaN, this.rank;
                    var o = Cesium.defined(i) && Cesium.defined(i.offset) ? i.offset : new Cesium.Cartesian3;
                    Cesium.Cartesian3.subtract(this.origin, e, o);
                    var r = Cesium.Cartesian3.magnitude(o),
                        a = Cesium.Cartesian3.dot(t, o) / r,
                        s = n.PhotoNavigation.projectInterestPointToPhoto(this.id, i);
                    if (null === s) return this.rank = NaN, this.rank;
                    var l = {
                            x: this.pixelWidth / 2,
                            y: this.pixelHeight / 2
                        },
                        c = Math.sqrt(Math.pow(s.x - l.x, 2) + Math.pow(s.y - l.y, 2)),
                        u = 5 / Math.sqrt(Math.pow(l.x, 2) + Math.pow(l.y, 2)) * c;
                    return this.rank = u * r / ((.5 + a) * this.focalLength), this.rank
                }, e.prototype.openPhoto = function() {
                    window.open(this.url, "_blank")
                }, e
            }();
        i.PhotoInfo = o
    }, {
        "./../PhotoNavigation": 19
    }],
    18: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = function() {
            return function(e, t, i, n, o) {
                var r = this;
                this._photo = t, this._element = document.createElement("div"), this._element.className = "photonavigation-grid-element", this._element.id = t.id.toString(), e.appendChild(this._element), this._element.onclick = function() {
                    i(r._photo, r._element)
                }, this._element.onmouseenter = function() {
                    n(r._photo)
                }, this._element.onmouseleave = function() {
                    o()
                };
                var a = document.createElement("img");
                a.src = this._photo.url_tn, a.className = "photonavigation-grid-thumbnail", a.title = this._photo.url, this._element.appendChild(a)
            }
        }();
        i.ThumbnailGridElement = n
    }, {}],
    19: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./Object/ClipFrustum"),
            o = e("./Object/PhotoInfo"),
            r = function() {
                function e() {}
                return e.parseBlockXML = function(t) {
                    if (t.evaluate)
                        if (e.isCorrectCoordinateSystem(t))
                            for (var i = t.evaluate("/BlocksExchange/Block/Photogroups/Photogroup", t, null, XPathResult.ANY_TYPE, null), n = i.iterateNext(); n;) {
                                var r = void 0,
                                    a = 0,
                                    s = 0,
                                    l = 0,
                                    c = 0,
                                    u = "",
                                    d = "";
                                r = {
                                    k1: 0,
                                    k2: 0,
                                    k3: 0,
                                    p1: 0,
                                    p2: 0,
                                    principalPointX: 0,
                                    principalPointY: 0
                                };
                                for (var p = 0, h = n.childNodes; p < h.length; p++) {
                                    var f = h[p];
                                    switch (f.nodeName) {
                                        case "PrincipalPoint":
                                            for (var m = 0, v = f.childNodes; m < v.length; m++) {
                                                var g = v[m];
                                                "x" === g.nodeName ? r.principalPointX = parseFloat(g.innerHTML) : "y" === g.nodeName && (r.principalPointY = parseFloat(g.innerHTML))
                                            }
                                            break;
                                        case "Distortion":
                                            for (var y = 0, C = f.childNodes; y < C.length; y++) {
                                                var b = C[y];
                                                "K1" === b.nodeName ? r.k1 = parseFloat(b.innerHTML) : "K2" === b.nodeName ? r.k2 = parseFloat(b.innerHTML) : "K3" === b.nodeName ? r.k3 = parseFloat(b.innerHTML) : "P1" === b.nodeName ? r.p1 = parseFloat(b.innerHTML) : "P2" === b.nodeName && (r.p2 = parseFloat(b.innerHTML))
                                            }
                                            break;
                                        case "FocalLength":
                                            a = parseFloat(f.innerHTML);
                                            break;
                                        case "SensorSize":
                                            s = parseFloat(f.innerHTML);
                                            break;
                                        case "ImageDimensions":
                                            for (var x = 0, w = f.childNodes; x < w.length; x++) {
                                                var T = w[x];
                                                "Width" === T.nodeName ? l = parseFloat(T.innerHTML) : "Height" === T.nodeName && (c = parseFloat(T.innerHTML))
                                            }
                                            break;
                                        case "Photo":
                                            for (var E = 0, P = void 0, N = 0, A = f.childNodes; N < A.length; N++) {
                                                var S = A[N];
                                                if ("Pose" === S.nodeName) P = e.parseCameraPose(S);
                                                else if ("Id" === S.nodeName) E = parseFloat(S.innerHTML);
                                                else if ("ImagePath" === S.nodeName) {
                                                    var M = S.innerHTML;
                                                    u = S.innerHTML, d = M.substr(0, M.indexOf(".")) + "_tn.jpg"
                                                }
                                            }
                                            if (void 0 === P || void 0 === P.rotation) continue;
                                            e.photos.push(new o.PhotoInfo(E, u, d, P.origin, P.rotation, a, s, l, c, r))
                                    }
                                }
                                n = i.iterateNext()
                            } else alert("Blocks Exchange file is in incorrect coordinate system, need EPSG:4978");
                        else alert("browser not yet supported")
                }, e.isCorrectCoordinateSystem = function(e) {
                    for (var t = e.evaluate("/BlocksExchange/SpatialReferenceSystems/SRS", e, null, XPathResult.ANY_TYPE, null), i = t.iterateNext(); i;) {
                        for (var n = 0, o = i.childNodes; n < o.length; n++) {
                            var r = o[n];
                            if ("Definition" === r.nodeName && "EPSG:4978" === r.innerHTML) return !0
                        }
                        i = t.iterateNext()
                    }
                    return !1
                }, e.parseCameraPose = function(e) {
                    var t, i, n = 0,
                        o = 0,
                        r = 0;
                    t = {
                        origin: null,
                        rotation: void 0
                    }, i = {
                        M00: 1,
                        M01: 0,
                        M02: 0,
                        M10: 0,
                        M11: 1,
                        M12: 0,
                        M20: 0,
                        M21: 0,
                        M22: 1
                    };
                    for (var a = 0, s = e.childNodes; a < s.length; a++) {
                        var l = s[a];
                        if ("Rotation" === l.nodeName) {
                            for (var c = 0, u = l.childNodes; c < u.length; c++) {
                                var d = u[c];
                                "M_00" === d.nodeName ? i.M00 = parseFloat(d.innerHTML) : "M_01" === d.nodeName ? i.M01 = parseFloat(d.innerHTML) : "M_02" === d.nodeName ? i.M02 = parseFloat(d.innerHTML) : "M_10" === d.nodeName ? i.M10 = parseFloat(d.innerHTML) : "M_11" === d.nodeName ? i.M11 = parseFloat(d.innerHTML) : "M_12" === d.nodeName ? i.M12 = parseFloat(d.innerHTML) : "M_20" === d.nodeName ? i.M20 = parseFloat(d.innerHTML) : "M_21" === d.nodeName ? i.M21 = parseFloat(d.innerHTML) : "M_22" === d.nodeName && (i.M22 = parseFloat(d.innerHTML))
                            }
                            t.rotation = i
                        } else if ("Center" === l.nodeName) {
                            for (var p = 0, h = l.childNodes; p < h.length; p++) {
                                var f = h[p];
                                "x" === f.nodeName ? n = parseFloat(f.innerHTML) : "y" === f.nodeName ? o = parseFloat(f.innerHTML) : "z" === f.nodeName && (r = parseFloat(f.innerHTML))
                            }
                            t.origin = new Cesium.Cartesian3(n, o, r)
                        }
                    }
                    return null != t.origin && void 0 === t.rotation && (t.rotation = i), t
                }, e.initialize = function(t) {
                    t.bimAdmin.changeEvent.addEventListener(e.onTilesetChanged.bind(this))
                }, e.onTilesetChanged = function(e) {
                    switch (e) {
                        case Bentley.BimiumEvents.TileSetChanged:
                            this.loadedXML = !1, this.photos = [], this.activePhotos = []
                    }
                }, e.loadFromXML = function(t, i, n) {
                    var o = this;
                    if (this.loadedXML) return e.showActiveCameraEntities(), void n.onFileLoaded();
                    var r = {
                        url: t
                    };
                    Cesium.Resource.fetchXML(r).then(function(t) {
                        e.parseBlockXML(t), e.addFrustumPrimitives(i), e.filterPhotosByInterestPoint(), e.showActiveCameraEntities(), o.loadedXML = !0, n.onFileLoaded()
                    }).otherwise(function(e) {
                        alert(e)
                    })
                }, e.removeFrustumPrimitives = function(e) {
                    for (var t = 0, i = this.photos; t < i.length; t++) {
                        var n = i[t];
                        n && (e.scene.primitives.remove(n.frustumOutlineEntity), e.scene.primitives.remove(n.frustumEntity))
                    }
                }, e.addFrustumPrimitives = function(e) {
                    for (var t = new Cesium.Color(0, .5451, 1, .2), i = new Cesium.Color(1, 1, 0, 1), n = new Cesium.Matrix3, o = new Cesium.Matrix3, r = new Cesium.Quaternion, a = Cesium.ColorGeometryInstanceAttribute.fromColor(t), s = Cesium.ColorGeometryInstanceAttribute.fromColor(i), l = new Cesium.PerInstanceColorAppearance({
                            closed: !0,
                            flat: !0
                        }), c = 0, u = this.photos; c < u.length; c++) {
                        var d = u[c];
                        if (d) {
                            n[Cesium.Matrix3.COLUMN0ROW0] = d.rotation.M00, n[Cesium.Matrix3.COLUMN1ROW0] = d.rotation.M01, n[Cesium.Matrix3.COLUMN2ROW0] = d.rotation.M02, n[Cesium.Matrix3.COLUMN0ROW1] = d.rotation.M10, n[Cesium.Matrix3.COLUMN1ROW1] = d.rotation.M11, n[Cesium.Matrix3.COLUMN2ROW1] = d.rotation.M12, n[Cesium.Matrix3.COLUMN0ROW2] = d.rotation.M20, n[Cesium.Matrix3.COLUMN1ROW2] = d.rotation.M21, n[Cesium.Matrix3.COLUMN2ROW2] = d.rotation.M22, Cesium.Matrix3.inverse(n, o), Cesium.Quaternion.fromRotationMatrix(o, r);
                            var p = new Cesium.PerspectiveFrustum({
                                    fov: d.fieldOfViewX > d.fieldOfViewY ? 2 * d.fieldOfViewX : 2 * d.fieldOfViewY,
                                    aspectRatio: d.pixelWidth / d.pixelHeight,
                                    near: .1,
                                    far: this.frustumSize
                                }),
                                h = new Cesium.FrustumGeometry({
                                    frustum: p,
                                    origin: d.origin,
                                    orientation: r,
                                    vertexFormat: Cesium.VertexFormat.POSITION_ONLY
                                }),
                                f = new Cesium.GeometryInstance({
                                    geometry: h,
                                    attributes: {
                                        color: a
                                    },
                                    id: "frustum " + d.id
                                });
                            d.frustumEntity = e.scene.primitives.add(new Cesium.Primitive({
                                geometryInstances: f,
                                appearance: l,
                                asynchronous: !1
                            })), d.frustumOutlineEntity = e.scene.primitives.add(new Cesium.Primitive({
                                geometryInstances: new Cesium.GeometryInstance({
                                    geometry: new Cesium.FrustumOutlineGeometry({
                                        frustum: p,
                                        origin: d.origin,
                                        orientation: r
                                    }),
                                    attributes: {
                                        color: s
                                    },
                                    id: "outline " + d.id
                                }),
                                appearance: new Cesium.PerInstanceColorAppearance({
                                    flat: !0
                                }),
                                asynchronous: !1
                            }))
                        }
                    }
                }, e.filterPhotosByInterestPoint = function() {
                    this.activePhotos.length = 0;
                    for (var e = [new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0), new Cesium.Plane(Cesium.Cartesian3.UNIT_X, 0)], t = [new Cesium.Cartesian3, new Cesium.Cartesian3, new Cesium.Cartesian3, new Cesium.Cartesian3], i = new Cesium.Cartesian3, o = 0, r = this.photos; o < r.length; o++) {
                        var a = r[o];
                        if (a)
                            if (!1 === this.hasInterestPoint) this.activePhotos.push(a);
                            else {
                                if (Cesium.Cartesian3.subtract(a.origin, this.interestPoint, i), Cesium.Cartesian3.normalize(i, i), Cesium.Cartesian3.dot(i, this.interestPointNormal) < .5) continue;
                                new n.ClipFrustum(a, e, t).PointInside(this.interestPoint) && this.activePhotos.push(a)
                            }
                    }
                }, e.showActiveCameraEntities = function() {
                    if (this.hasInterestPoint) {
                        for (var e = 0, t = this.photos; e < t.length; e++) {
                            (a = t[e]) && (a.frustumEntity.show = !1, a.frustumOutlineEntity.show = !1)
                        }
                        for (var i = 0, n = this.activePhotos; i < n.length; i++) {
                            (a = n[i]).frustumEntity.show = !0, a.frustumOutlineEntity.show = !0
                        }
                    } else
                        for (var o = 0, r = this.photos; o < r.length; o++) {
                            var a;
                            (a = r[o]) && (a.frustumEntity.show = this.displayFrustums, a.frustumOutlineEntity.show = this.displayFrustums)
                        }
                }, e.cleanup = function() {
                    for (var t = 0, i = this.photos; t < i.length; t++) {
                        var n = i[t];
                        n && (n.frustumEntity.show = !1, n.frustumOutlineEntity.show = !1)
                    }
                    this.hasInterestPoint = !1, e.filterPhotosByInterestPoint()
                }, e.activateInterestPoint = function(t, i) {
                    this.hasInterestPoint = !0, this.interestPoint = t, this.interestPointNormal = i, e.filterPhotosByInterestPoint(), e.showActiveCameraEntities()
                }, e.deactivateInterestPoint = function() {
                    this.hasInterestPoint = !1, e.filterPhotosByInterestPoint(), e.showActiveCameraEntities()
                }, e.getPhotoInfo = function(e) {
                    for (var t = 0, i = this.photos; t < i.length; t++) {
                        var n = i[t];
                        if (e === n.id) return n
                    }
                }, e.getInterestPoint = function() {
                    return this.hasInterestPoint ? this.interestPoint.clone() : void 0
                }, e.getPhotos = function() {
                    return this.photos.slice(0)
                }, e.getActivePhotos = function() {
                    return this.activePhotos.slice(0)
                }, e.isActivePhotos = function() {
                    return this.activePhotos.length > 0
                }, e.toggleFrustumDisplay = function(t) {
                    e.displayFrustums = !e.displayFrustums, e.showActiveCameraEntities(), t.BimAdmin.viewer.scene.requestRender()
                }, e.isFrustumDisplay = function() {
                    return e.displayFrustums
                }, e.growFrustums = function(t) {
                    e.frustumSize += .5, e.removeFrustumPrimitives(t), e.addFrustumPrimitives(t), e.showActiveCameraEntities(), t.BimAdmin.viewer.scene.requestRender()
                }, e.shrinkFrustums = function(t) {
                    this.frustumSize <= .5 || (this.frustumSize -= .5, e.removeFrustumPrimitives(t), e.addFrustumPrimitives(t), e.showActiveCameraEntities(), t.BimAdmin.viewer.scene.requestRender())
                }, e.projectInterestPointToPhoto = function(t, i) {
                    var n = e.getPhotoInfo(t);
                    if (Cesium.defined(n) && e.hasInterestPoint && void 0 !== n) {
                        var o, r = Cesium.defined(i) && Cesium.defined(i.offset) ? i.offset : new Cesium.Cartesian3;
                        Cesium.Cartesian3.subtract(e.interestPoint, n.origin, r), Cesium.defined(i) && Cesium.defined(i.matrix) ? ((o = i.matrix)[Cesium.Matrix3.COLUMN0ROW0] = n.rotation.M00, o[Cesium.Matrix3.COLUMN1ROW0] = n.rotation.M01, o[Cesium.Matrix3.COLUMN2ROW0] = n.rotation.M02, o[Cesium.Matrix3.COLUMN0ROW1] = n.rotation.M10, o[Cesium.Matrix3.COLUMN1ROW1] = n.rotation.M11, o[Cesium.Matrix3.COLUMN2ROW1] = n.rotation.M12, o[Cesium.Matrix3.COLUMN0ROW2] = n.rotation.M20, o[Cesium.Matrix3.COLUMN1ROW2] = n.rotation.M21, o[Cesium.Matrix3.COLUMN2ROW2] = n.rotation.M22) : o = new Cesium.Matrix3(n.rotation.M00, n.rotation.M01, n.rotation.M02, n.rotation.M10, n.rotation.M11, n.rotation.M12, n.rotation.M20, n.rotation.M21, n.rotation.M22), Cesium.Matrix3.multiplyByVector(o, r, r);
                        var a = (n.pixelWidth > n.pixelHeight ? n.pixelWidth : n.pixelHeight) / n.sensorSize * n.focalLength,
                            s = {
                                x: r.x / r.z,
                                y: r.y / r.z
                            };
                        return e.applyDistortion(n.distortion, s), s.x = a * s.x + n.distortion.principalPointX, s.y = a * s.y + n.distortion.principalPointY, s
                    }
                }, e.applyDistortion = function(e, t) {
                    var i = t.x,
                        n = t.y,
                        o = Math.pow(i, 2) + Math.pow(n, 2),
                        r = 1 + e.k1 * o + e.k2 * Math.pow(o, 2) + e.k3 * Math.pow(o, 3);
                    t.x = r * i + 2 * e.p2 * i * n + e.p1 * (o + 2 * i * i), t.y = r * n + 2 * e.p1 * i * n + e.p2 * (o + 2 * n * n)
                }, e.calculatePhotoFrustumExtents = function(e, t) {
                    this.photos.forEach(function(i) {
                        e.x = Math.min(e.x, i.origin.x), e.y = Math.min(e.y, i.origin.y), e.z = Math.min(e.z, i.origin.z), t.x = Math.max(t.x, i.origin.x), t.y = Math.max(t.y, i.origin.y), t.z = Math.max(t.z, i.origin.z)
                    })
                }, e.hasInterestPoint = !1, e.loadedXML = !1, e.photos = [], e.activePhotos = [], e.frustumSize = 1.5, e.displayFrustums = !1, e
            }();
        i.PhotoNavigation = r;
        var a = {
            fitView: {
                before: new Bentley.Frustum
            }
        };
        Bentley.ViewManip.fitView = function(e, t, i) {
            var n = e.computeViewRange(),
                o = e.getViewRect().aspect,
                s = e.getWorldFrustum(a.fitView.before);
            if ("Vivo.BrowsePhotoTool" === e.toolAdmin._primitiveTool.id) {
                var l = {
                        x: Number.MAX_VALUE,
                        y: Number.MAX_VALUE,
                        z: Number.MAX_VALUE
                    },
                    c = {
                        x: Number.MIN_VALUE,
                        y: Number.MIN_VALUE,
                        z: Number.MIN_VALUE
                    };
                r.calculatePhotoFrustumExtents(l, c);
                var u = [new Cesium.Cartesian3(-1, -1, -1), new Cesium.Cartesian3(1, -1, -1), new Cesium.Cartesian3(-1, 1, -1), new Cesium.Cartesian3(1, 1, -1), new Cesium.Cartesian3(-1, -1, 1), new Cesium.Cartesian3(1, -1, 1), new Cesium.Cartesian3(-1, 1, 1), new Cesium.Cartesian3(1, 1, 1)],
                    d = new Cesium.Matrix4,
                    p = Cesium.Matrix4.IDENTITY,
                    h = Cesium.Matrix4.fromRotationTranslation(e.rotMatrix),
                    f = Cesium.Matrix4.IDENTITY.clone(),
                    m = new Cesium.Matrix4;
                if (l.x !== Number.MAX_VALUE && l.y !== Number.MAX_VALUE && l.z !== Number.MAX_VALUE) {
                    f[12] = l.x, f[13] = l.y, f[14] = l.z, d.initProduct(p, f), d.initProduct(h, d);
                    for (var v = 0; v < 8; v++) Cesium.Matrix4.multiplyByPoint(d, u[v], m), n.extend(m)
                }
                if (c.x !== Number.MIN_VALUE && c.y !== Number.MIN_VALUE && c.z !== Number.MIN_VALUE) {
                    f[12] = c.x, f[13] = c.y, f[14] = c.z, d.initProduct(p, f), d.initProduct(h, d);
                    for (v = 0; v < 8; v++) Cesium.Matrix4.multiplyByPoint(d, u[v], m), n.extend(m)
                }
            }
            e.view.lookAtViewAlignedVolume(n, o, Bentley.ViewToolSettings.fitExpandsClipping), e.synchWithView(!1), e.moveViewToSurfaceIfRequired(), t && Bentley.ViewTool.animateFrustumChange(e, s, e.getFrustum(3, !1)), e.synchWithView(!0)
        }
    }, {
        "./Object/ClipFrustum": 15,
        "./Object/PhotoInfo": 17
    }],
    20: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./BrowsePhotoTool"),
            o = e("./PhotoNavigation"),
            r = e("./Object/ThumbnailGridElement"),
            a = function() {
                function e(e, t) {
                    var i = this;
                    this.handleClick = function(e, t) {
                        n.BrowsePhotoTool.imageId !== e.id ? (i.browsePhotoTool.updateImageSelection(e.id), i.highlightThumbnail(e)) : (i.browsePhotoTool.updateImageSelection(-1), i.unhighlightThumbnail())
                    }, this.onMouseEnter = function(e) {
                        i.browsePhotoTool.createTemporaryFrustum(e)
                    }, this.onMouseExit = function() {
                        i.browsePhotoTool.deleteTemporaryFrustum()
                    }, this.photos = [], this.activePhotos = [], this.gridElements = [], this.highlightElement = document.createElement("div"), this.highlightElement.className = "photonavigation-grid-highlight", this.parentElement = e, this.browsePhotoTool = t, this.gridContainer = document.createElement("div"), this.gridContainer.className = "photonavigation-grid-container", this.gridContainer.appendChild(this.highlightElement), this.parentElement.appendChild(this.gridContainer)
                }
                return e.prototype.highlightThumbnail = function(e) {
                    if (void 0 !== e) {
                        var t = e.id.toString(),
                            i = $("#" + t)[0];
                        return this.highlightElement.style.top = i.offsetTop + "px", this.highlightElement.style.left = i.offsetLeft + "px", this.highlightElement.style.width = i.clientWidth + "px", this.highlightElement.style.height = i.clientHeight + "px", i.getBoundingClientRect().top
                    }
                    return 0
                }, e.prototype.unhighlightThumbnail = function() {
                    return n.BrowsePhotoTool.imageId = -1, this.highlightElement.style.top = "", this.highlightElement.style.left = "", this.highlightElement.style.width = "", this.highlightElement.style.height = "", !0
                }, e.prototype.handleKeyPress = function(e) {
                    switch (e.keyCode) {
                        case 39:
                            this.getNextPhoto();
                            break;
                        case 37:
                            this.getPrevPhoto();
                            break;
                        default:
                            return
                    }
                }, e.prototype.getNextPhoto = function() {
                    var e = n.BrowsePhotoTool.imageId;
                    if (-1 !== e) {
                        for (var t = 0, i = 0, o = this.activePhotos; i < o.length; i++) {
                            if (t += 1, o[i].id === e) break
                        }
                        if (t < this.activePhotos.length) {
                            var r = this.activePhotos[t].id;
                            this.browsePhotoTool.updateImageSelection(r), this.highlightThumbnail(this.activePhotos[t])
                        }
                    }
                }, e.prototype.getPrevPhoto = function() {
                    var e = n.BrowsePhotoTool.imageId;
                    if (-1 !== e) {
                        for (var t = 0, i = 0, o = this.activePhotos; i < o.length; i++) {
                            if (o[i].id === e) {
                                t -= 1;
                                break
                            }
                            t += 1
                        }
                        if (t >= 0) {
                            var r = this.activePhotos[t].id;
                            this.browsePhotoTool.updateImageSelection(r), this.highlightThumbnail(this.activePhotos[t])
                        }
                    }
                }, e.prototype.clearGrid = function() {
                    for (; this.gridContainer.lastChild !== this.gridContainer.firstChild;) {
                        var e = this.gridContainer.lastChild;
                        void 0 != e && this.gridContainer.removeChild(e)
                    }
                }, e.prototype.generateGrid = function() {
                    this.photos = this.activePhotos = o.PhotoNavigation.getPhotos();
                    for (var e = 0, t = this.photos; e < t.length; e++) {
                        var i = t[e];
                        Cesium.defined(i) ? this.gridElements.push(new r.ThumbnailGridElement(this.gridContainer, i, this.handleClick, this.onMouseEnter, this.onMouseExit)) : this.gridElements.push(void 0)
                    }
                }, e.prototype.updateGrid = function(e, t) {
                    if (this.clearGrid(), this.activePhotos = o.PhotoNavigation.getActivePhotos(), Cesium.defined(e)) {
                        for (var i = new Cesium.Matrix3, n = {
                                offset: new Cesium.Cartesian3,
                                matrix: i
                            }, r = 0, a = this.activePhotos; r < a.length; r++) {
                            (c = a[r]).rankPhotoByInterestPoint(e, t, n)
                        }
                        this.activePhotos.sort(function(e, t) {
                            return e.rank - t.rank
                        })
                    }
                    for (var s = 0, l = this.activePhotos; s < l.length; s++) {
                        var c = l[s],
                            u = this.gridElements[c.id];
                        void 0 !== u && this.gridContainer.appendChild(u._element)
                    }
                    this.activePhotos.length > 0 && this.browsePhotoTool.updateImageSelection(this.activePhotos[0].id)
                }, e
            }();
        i.PhotoNavThumbnailPanel = a
    }, {
        "./BrowsePhotoTool": 13,
        "./Object/ThumbnailGridElement": 18,
        "./PhotoNavigation": 19
    }],
    21: [function(e, t, i) {
        "use strict";
        Bentley.PropertyDataTool.prototype.getPropertyData = function() {
            this.onBeforeStart.raiseEvent();
            var e = this.viewport.selectionSet.getSingleSelectedElement();
            Bentley.Element.isValidElement(e) && this.active && this.loadData(e.elementId)
        }, Bentley.PropertyDataTool.prototype.loadData = function(e) {
            var t = this;
            this.onStart.raiseEvent(), this.getData(this.bim.name, e).then(function(e) {
                t.onChanged.raiseEvent(e)
            }, function(e) {
                t.onError.raiseEvent(e)
            })
        }
    }, {}],
    22: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = function() {
            return function(e, t) {
                var i = this;
                this.requestRender = function() {
                    i.viewport.scene.requestRenderMode && i.viewport.scene.requestRender()
                }, this.onPick = function(e, t) {
                    var n = !0;
                    if (Cesium.defined(e)) n = Bentley.SelectionTool.prototype.onPick.call(i, e, t);
                    else {
                        var o = i.viewport.scene.pick(t.viewPoint);
                        Cesium.defined(o) && Cesium.defined(o.primitive) && Cesium.defined(o.id) ? (o.elementId = o.id, n = Bentley.SelectionTool.prototype.onPick.call(i, o, t)) : i.viewport.selectionSet.clear()
                    }
                    return i.requestRender(), n
                }, Bentley.SelectionTool.call(this, e), this.id = "Vivo.SelectionTool", this.viewport = e
            }
        }();
        i.VivoSelectionTool = n;
        var o = function() {
            return function() {
                this.getToolConstructor = function() {
                    return Bentley.extend(Bentley.SelectionTool, n), n
                }
            }
        }();
        i.VivoSelectionToolWrapper = o
    }, {}],
    23: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./AddClassificationTool"),
            o = e("../Widgets/ClassificationDisplayWidget"),
            r = e("./VivoSelectionTool"),
            a = e("../Widgets/VivoPropertyGridWidget"),
            s = e("./PhotoNavigation/BrowsePhotoTool"),
            l = e("./../ConfigManager"),
            c = e("./../Utils/MergeDeep"),
            u = e("jquery"),
            d = e("../Utils/HttpRequestHelper"),
            p = function() {
                function e() {}
                return e.Initialize = function() {
                    this.MergeJSON(), this.InitializeTools(), this.InitializeWidgets(), this.ShowOrHideBackendBasedTools()
                }, e.PostInitialize = function(e) {
                    this.AddEAPIcon(), this.AddBentleyIconModal(e)
                }, e.AddEAPIcon = function() {
                    var e = document.getElementById("bim-app-button");
                    if (null !== e) {
                        var t = document.createElement("div");
                        t.className = "application-eap-tooltip", e.appendChild(t);
                        var i = Bentley.TemplateUtility.createIcon("bim-icon-technical-preview-badge");
                        i.classList.add("application-eap"), t.appendChild(i);
                        var n = document.createElement("div");
                        n.innerText = "Early Adopter Program", n.className = "application-eap-tooltiptext", t.appendChild(n)
                    }
                }, e.AddBentleyIconModal = function(e) {
                    var t = this,
                        i = document.getElementById("bim-app-button");
                    null !== i && (i.onclick = function(i) {
                        t._modal = document.createElement("div"), t._modal.id = "BentleyIconModal", t._modal.className = "blocker", e._container.appendChild(t._modal);
                        var n = document.createElement("div");
                        n.className = "modal-window center", t._modal.appendChild(n);
                        var o = document.createElement("div"),
                            r = document.createElement("button");
                        r.className = "button-top-right", r.onclick = function() {
                            t.CloseBentleyIconModal()
                        }, r.innerText = "X", o.appendChild(r), n.appendChild(o);
                        var a = document.createElement("img");
                        a.className = "image-center", a.src = "app/content/images/bentley-logo.png", n.appendChild(a);
                        var s = document.createElement("div"),
                            c = document.createElement("div");
                        c.className = "container-with-border";
                        var u = document.createElement("label");
                        u.className = "modal-text text", u.innerText = "Copyright (c) 2018 Bentley Systems, Incorporated. All rights reserved.                 Including software, file formats, and audiovisual displays; may only be used pursuant to applicable software license agreement;                 contains confidential and proprietary information of Bentley Systems, Incorporated and/or third parties which is protected by                 copyright and trade secret law and may not be provided or otherwise made available without proper authorization.", c.appendChild(u), s.appendChild(c), n.appendChild(s);
                        var d = document.createElement("img");
                        d.className = "image-center bing", d.src = "app/content/images/bing-maps.png", n.appendChild(d), (s = document.createElement("div")).className = "container-with-border";
                        var p = document.createElement("span");
                        p.className = "modal-text text", p.innerHTML = "                Background Map data is supplied by Microsoft Bing, subject to the                 <a href='https://www.microsoft.com/en-us/maps/product/'>Microsoft Bing Terms of Use</a>                  which can be viewed by clicking on the link. In particular, printing Background Map data is subject to the                 <a href='https://www.microsoft.com/en-us/maps/product/print-rights/'>Print Rights</a>                  which can found by clicking on the link.                 ", s.appendChild(p), n.appendChild(s);
                        var h = document.createElement("label");
                        (h = document.createElement("label")).className = "modal-text text right", h.innerText = "ContextCapture Navigator " + l.ConfigManager.GetConfig().version, n.appendChild(h)
                    })
                }, e.CloseBentleyIconModal = function() {
                    u("#BentleyIconModal").remove()
                }, e.MergeJSON = function() {
                    this.MergeToolbar()
                }, e.MergeToolbar = function() {
                    for (var e = l.ConfigManager.GetConfig().toolbar, t = 0; t < Bentley.ToolbarJSON.toolbar.zones.length; t++) e.zones[t].id === Bentley.ToolbarJSON.toolbar.zones[t].id && (Bentley.ToolbarJSON.toolbar.zones[t] = c.MergeDeep.Merge(Bentley.ToolbarJSON.toolbar.zones[t], e.zones[t]))
                }, e.InitializeTools = function() {
                    var e = {
                        id: "Vivo.SelectionTool",
                        object: (new r.VivoSelectionToolWrapper).getToolConstructor(),
                        type: "Bim.ViewportTool"
                    };
                    if (Bentley.ToolsJSON.tools.push(e), d.HttpRequestHelper.IsBackendActivated()) {
                        var t = {
                            id: "Vivo.AddClassificationTool",
                            object: (new n.AddClassificationToolWrapper).getToolConstructor(),
                            type: "Bim.ViewportTool"
                        };
                        Bentley.ToolsJSON.tools.push(t)
                    }
                    var i = {
                        id: "Vivo.BrowsePhotoTool",
                        object: (new s.BrowsePhotoToolWrapper).getToolConstructor(),
                        type: "Bim.ViewportTool"
                    };
                    Bentley.ToolsJSON.tools.push(i)
                }, e.InitializeWidgets = function() {
                    if (d.HttpRequestHelper.IsBackendActivated()) {
                        var e = {
                            id: "Vivo.VivoPropertyGridWidget",
                            object: (new a.VivoPropertyGridWidgetWrapper).getWidgetConstructor()
                        };
                        Bentley.WidgetsJSON.widgets.push(e)
                    }
                    var t = {
                        id: "Vivo.ClassificationDisplayWidget",
                        object: (new o.ClassificationDisplayWidgetWrapper).getWidgetConstructor()
                    };
                    Bentley.WidgetsJSON.widgets.push(t)
                }, e.ShowOrHideBackendBasedTools = function() {
                    Bentley.ToolbarJSON.toolbar.zones.filter(function(e) {
                        return "Bim.Top.Right.WidgetZone" === e.id
                    })[0].widgets.filter(function(e) {
                        return "TopRightPanelTabs" === e.id
                    })[0].tabs.filter(function(e) {
                        return "PropertyGridTab" === e.id
                    })[0].include = d.HttpRequestHelper.IsBackendActivated(), Bentley.ToolbarJSON.toolbar.zones.filter(function(e) {
                        return "Bim.VerticalToolButtons" === e.id
                    })[0].buttons.filter(function(e) {
                        return "AddClassificationTool" === e.id
                    })[0].include = d.HttpRequestHelper.IsBackendActivated()
                }, e._modal = void 0, e
            }();
        i.VivoTools = p
    }, {
        "../Utils/HttpRequestHelper": 25,
        "../Widgets/ClassificationDisplayWidget": 28,
        "../Widgets/VivoPropertyGridWidget": 29,
        "./../ConfigManager": 5,
        "./../Utils/MergeDeep": 26,
        "./AddClassificationTool": 7,
        "./PhotoNavigation/BrowsePhotoTool": 13,
        "./VivoSelectionTool": 22,
        jquery: 31
    }],
    24: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = function() {
            function e() {}
            return e.createInput = function(e, t, i, n, o) {
                var r = document.createElement("input");
                return r.id = e, r.type = t, r.className = i, void 0 !== n && (r.value = n), void 0 !== o && (r.onclick = o), r
            }, e
        }();
        i.DocumentUtils = n
    }, {}],
    25: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("jquery"),
            o = e("../ConfigManager"),
            r = function() {
                function e() {}
                return e.GetRequest = function(e, t, i, o) {
                    var r = this;
                    this.IsBackendActivated() && n.ajax({
                        url: e,
                        type: "GET",
                        contentType: "application/json",
                        headers: t,
                        success: function(e) {
                            i(e)
                        },
                        error: function(e, t) {
                            0 === e.status ? (o(e, "Server unreachable."), r._serverReachable = !1) : o(e, e.responseText)
                        }
                    })
                }, e.PostRequest = function(e, t, i, o, r) {
                    this.IsBackendActivated() && n.ajax({
                        url: e,
                        type: "POST",
                        data: void 0 !== i ? JSON.stringify(i) : void 0,
                        contentType: "application/json",
                        headers: t,
                        success: function(e) {
                            o(e)
                        },
                        error: function(e, t) {
                            0 === e.status ? r(e, "Server unreachable.") : r(e, e.responseText)
                        }
                    })
                }, e.PutRequest = function(e, t, i, o, r) {
                    this.IsBackendActivated() && n.ajax({
                        url: e,
                        type: "PUT",
                        data: JSON.stringify(i),
                        contentType: "application/json",
                        headers: t,
                        success: function(e) {
                            o(e)
                        },
                        error: function(e, t) {
                            0 === e.status ? r(e, "Server unreachable.") : r(e, e.responseText)
                        }
                    })
                }, e.IsBackendActivated = function() {
                    return o.ConfigManager.GetConfig().backendUrl.length > 0
                }, e.IsBackendReachable = function() {
                    return this._serverReachable
                }, e._serverReachable = !0, e
            }();
        i.HttpRequestHelper = r
    }, {
        "../ConfigManager": 5,
        jquery: 31
    }],
    26: [function(e, t, i) {
        "use strict";
        var n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var o = function() {
            function e() {}
            return e.isObject = function(e) {
                return e && "object" === (void 0 === e ? "undefined" : n(e)) && !Array.isArray(e)
            }, e.Merge = function(e) {
                for (var t = [], i = 1; i < arguments.length; i++) t[i - 1] = arguments[i];
                if (!t.length) return e;
                var n, o, r = t.shift();
                if (this.isObject(e) && this.isObject(r))
                    for (var a in r) this.isObject(r[a]) ? (e[a] || Object.assign(e, ((n = {})[a] = {}, n)), this.Merge(e[a], r[a])) : Object.assign(e, ((o = {})[a] = r[a], o));
                return this.Merge.apply(this, [e].concat(t))
            }, e
        }();
        i.MergeDeep = o
    }, {}],
    27: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./ConfigManager"),
            o = e("./Utils/HttpRequestHelper"),
            r = function() {
                function e() {}
                return e.FetchProperties = function(t) {
                    var i = Cesium.when.defer(),
                        n = this;
                    return $.ajax({
                        url: "https://localhost:8443/getList/" + t,
                        type: "GET",
                        beforeSend: function(t) {
                            t.setRequestHeader("Authorization", e.accessToken)
                        },
                        success: function(e) {
                            e.childNodes[0].childNodes.forEach(function(e) {
                                "entry" === e.tagName && e.childNodes.forEach(function(e) {
                                    if ("content" === e.tagName) {
                                        var t = "",
                                            i = "",
                                            o = [];
                                        e.childNodes[0].childNodes.forEach(function(e) {
                                            "d:Title" === e.tagName ? t = e.textContent : "d:elementId" === e.tagName ? i = e.textContent : -1 !== e.tagName.indexOf("d:Attr") && (console.log(e), o.push({
                                                label: e.localName,
                                                value: e.textContent
                                            }))
                                        }), void 0 === n.propertiesList[i] && (n.propertiesList[i] = [], n.propertiesList[i].categories = []), n.propertiesList[i].categories.push({
                                            category: t,
                                            properties: o
                                        }), console.log("---------------------------------------")
                                    }
                                    console.log("=====================================")
                                })
                            }), i.resolve()
                        },
                        error: function() {
                            i.resolve()
                        }
                    }), i
                }, e.GetProperties = function(e, t) {
                    var i = this;
                    this.currentBimName = e, this.currentElemId = t;
                    var r = Cesium.when.defer();
                    this._classifDeferred = Cesium.when.defer(), this._dgnDeferred = Cesium.when.defer();
                    var a = [this._classifDeferred, this._dgnDeferred],
                        s = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.getClassifierAsset).replace("{modelName}", e).replace("{elementId}", t);
                    return o.HttpRequestHelper.GetRequest(s, void 0, this.OnSuccessGetProperties.bind(this), this.OnErrorGetProperties.bind(this)), s = (n.ConfigManager.GetConfig().backendUrl + n.ConfigManager.GetConfig().backendEndpoints.getBimProperties).replace("{modelName}", e).replace("{elementId}", t), o.HttpRequestHelper.GetRequest(s, void 0, function(e) {
                        void 0 !== e ? (i._bimAndAssetData.bim = JSON.parse(e), i._dgnDeferred.resolve()) : (i._bimAndAssetData.bim = "", i._dgnDeferred.resolve())
                    }, function() {
                        i._bimAndAssetData.bim = "", i._dgnDeferred.resolve()
                    }), $.when.apply($, a).then(function() {
                        r.resolve(i._bimAndAssetData)
                    }), r
                }, e.OnSuccessGetProperties = function(e) {
                    null !== e.categories ? (this._bimAndAssetData.asset = e, this._classifDeferred.resolve()) : (this._bimAndAssetData.asset = "", this._classifDeferred.resolve())
                }, e.OnErrorGetProperties = function(e) {
                    this._bimAndAssetData.asset = "", this._classifDeferred.resolve()
                }, e.propertiesList = {}, e._bimAndAssetData = {}, e
            }();
        i.VisualOperationPropertyService = r
    }, {
        "./ConfigManager": 5,
        "./Utils/HttpRequestHelper": 25
    }],
    28: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../Tools/ClassificationEntitiesManager"),
            o = function() {
                function e() {}
                return e.Override = function() {
                    Bentley.SelectionSetStyler.prototype.applySceneSettings = function() {}
                }, e
            }();
        i.SelectionSetStyler = o;
        var r = function() {
            function e(e, t) {
                var i = this;
                this.loadWidgetConfigs = function(e) {
                    return {
                        id: "Vivo.ClassificationDisplayWidget"
                    }
                }, this.loadToolWidgets = function() {
                    i.defaultButtonsLoaded || (i.loadHighlightButton(), i.defaultButtonsLoaded = !0)
                }, this.loadHighlightButton = function() {
                    var e = i;
                    i.addToolButton({
                        id: "highlightClassifications",
                        click: function() {
                            this.active = !this.active, e.scene.invertClassification = !e.scene.invertClassification, e.scene.invertClassificationColor = new Cesium.Color(.25, .25, .25, 1);
                            for (var t = n.ClassificationEntitiesManager.getClassificationInfos(), i = 0; i < t.length; i++) t[i].primitive.getGeometryInstanceAttributes(t[i].id).show = Cesium.ShowGeometryInstanceAttribute.toValue(!e.scene.invertClassification);
                            e.scene.requestRender()
                        },
                        label: "Highlight",
                        title: "Highlight visible Classification",
                        class: "bim-toggle-entry-ruled",
                        active: i.scene.invertClassification
                    })
                }, this.getAssetTypeList = function() {
                    var e;
                    e = [];
                    for (var t = 0, i = n.ClassificationEntitiesManager.getClassificationInfos(); t < i.length; t++) {
                        var o = i[t];
                        void 0 !== o.assetInfo && !1 === e.includes(o.assetInfo.type) ? e.push(o.assetInfo.type) : void 0 === o.assetInfo && !1 === e.includes("Unassigned") && e.push("Unassigned")
                    }
                    return e
                }, this.loadAssetTypeList = function() {
                    var e, t = i,
                        o = i.getAssetTypeList();
                    (e = []).push({
                        id: "selectAllClassifications",
                        click: function() {
                            for (var e = n.ClassificationEntitiesManager.getClassificationInfos(), i = 0; i < e.length; i++) e[i].primitive.show = !0;
                            t.loadSections(), t.scene.requestRender()
                        },
                        label: "All",
                        title: "Activate All",
                        class: "bim-toggle-entry",
                        visible: !0,
                        visibility: !0
                    }), e.push({
                        id: "selectNoneClassifications",
                        click: function() {
                            for (var e = n.ClassificationEntitiesManager.getClassificationInfos(), i = 0; i < e.length; i++) e[i].primitive.show = !1;
                            t.loadSections(), t.scene.requestRender()
                        },
                        label: "None",
                        title: "Desactivate All",
                        class: "bim-toggle-entry",
                        visible: !0,
                        visibility: !0
                    }), e.push({
                        id: "selectInvertClassifications",
                        click: function() {
                            for (var e = n.ClassificationEntitiesManager.getClassificationInfos(), i = 0; i < e.length; i++) e[i].primitive.show = !e[i].primitive.show;
                            t.loadSections(), t.scene.requestRender()
                        },
                        label: "Invert",
                        title: "Invert Selection",
                        class: "bim-toggle-entry",
                        visible: !0,
                        visibility: !0
                    });
                    for (var r = 0; r < o.length; r++) {
                        var a = n.ClassificationEntitiesManager.getClassificationInfos().filter(function(e) {
                            return "Unassigned" === o[r] ? void 0 === e.assetInfo : void 0 !== e.assetInfo && e.assetInfo.type === o[r]
                        });
                        e.push({
                            id: "VisOps.ClassificationVisibility." + o[r],
                            click: function() {
                                var e = this;
                                this.active = !this.active;
                                for (var i = n.ClassificationEntitiesManager.getClassificationInfos().filter(function(t) {
                                        return "Unassigned" === e.element.innerText ? void 0 === t.assetInfo : void 0 !== t.assetInfo && t.assetInfo.type === e.element.innerText
                                    }), o = 0; o < i.length; o++) i[o].primitive.show = !i[o].primitive.show;
                                t.scene.requestRender()
                            },
                            label: "" !== o[r] ? o[r] : "Unassigned",
                            title: "Click to toggle visibility",
                            class: "bim-toggle-entry",
                            visible: !0,
                            visibility: !0,
                            active: a[0].primitive.show
                        })
                    }
                    i.classificationSection = Bentley.ToolWidget.prototype.addSection.call(i, {
                        id: "Classification",
                        label: "Classifications",
                        expanded: i.expandClassification
                    }, e, i.addToolButton)
                }, this.loadMSClassifiers = function() {
                    if (i.MSClassifiers.length > 0) {
                        var e = i,
                            t = [];
                        i.MSClassifiers.forEach(function(i) {
                            t.push({
                                id: "VisOps.ClassificationVisibility." + i.name,
                                click: function() {
                                    this.active = !this.active, e.bimadmin.viewport.activeClassifier = this.active ? e.bimadmin.viewport.geometry._primitives[i.id] : void 0, e.bimadmin.viewport.geometry.setModelInScene(i.id, this.active), i.isActive = this.active, !0 === this.active && e.MSClassifiers.forEach(function(t) {
                                        t != i && (t.isActive = !1, e.bimadmin.viewport.geometry.setModelInScene(t.id, !1))
                                    }), e.bimadmin.viewport.styler.update(), e.loadSections(), e.scene.requestRender()
                                },
                                label: i.name,
                                title: "Click to toggle visibility",
                                class: "bim-toggle-entry",
                                visible: !0,
                                visibility: !0,
                                active: i.isActive
                            })
                        }), i.MSClassifiersSection = Bentley.ToolWidget.prototype.addSection.call(i, {
                            id: "MSClassfiers",
                            label: "MicroStation Classfiers",
                            expanded: i.expandMSClassifiers
                        }, t, i.addToolButton)
                    }
                }, Bentley.ToolWidget.call(this, e, t), this.defaultButtonsLoaded = !1, n.ClassificationEntitiesManager.onLoadedClassifierEvent.addEventListener(this.loadSections.bind(this)), n.ClassificationEntitiesManager.onAddClassifierEvent.addEventListener(this.onAddClassifier.bind(this)), n.ClassificationEntitiesManager.onEraseClassifierEvent.addEventListener(this.loadSections.bind(this)), this.scene.invertClassification = !1, this.scene.requestRender(), o.Override(), e.changeEvent.addEventListener(this.onTilesetChanged.bind(this)), this.bimadmin = e, this.expandMSClassifiers = !1, this.expandClassification = !1, this.MSClassifiersSection = void 0, this.classificationSection = void 0, this.MSClassifiers = []
            }
            return e.prototype.onTilesetChanged = function() {
                var e = this.bimadmin.bim.getClassifierModels();
                this.MSClassifiers = Object.keys(e).map(function(t) {
                    return Object.assign(e[t], {
                        id: t
                    })
                }), this.loadSections()
            }, e.prototype.onAddClassifier = function(e) {
                for (var t = n.ClassificationEntitiesManager.getClassificationInfos().filter(function(t) {
                        return void 0 !== t.assetInfo && void 0 !== e.assetInfo && t.assetInfo.type === e.assetInfo.type
                    }), i = 0; i < t.length; i++) t[i].primitive.show = !0;
                this.loadSections()
            }, e.prototype.reset = function() {}, e.prototype.loadSections = function() {
                this.resetSections(["highlightClassifications"]), this.loadAssetTypeList(), this.loadMSClassifiers()
            }, e.prototype.resetSections = function(e) {
                void 0 === e && (e = []), void 0 != this.MSClassifiersSection && (this.expandMSClassifiers = this.MSClassifiersSection.expanded), void 0 != this.classificationSection && (this.expandClassification = this.classificationSection.expanded), Bentley.ToolWidget.prototype.clearToolWidgets.call(this, e), this.MSClassifiersSection = void 0, this.classificationSection = void 0
            }, e
        }();
        i.ClassificationDisplayWidget = r;
        var a = function() {
            return function() {
                this.getWidgetConstructor = function() {
                    return Bentley.extend(Bentley.ToolWidget, r), r
                }
            }
        }();
        i.ClassificationDisplayWidgetWrapper = a
    }, {
        "./../Tools/ClassificationEntitiesManager": 9
    }],
    29: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./../Tools/VivoSelectionTool"),
            o = e("./../Tools/DeleteClassificationTool"),
            r = e("./../Tools/EditClassificationTool"),
            a = e("./../Tools/AssetAssignationTool"),
            s = e("./../Tools/ClassificationEntitiesManager"),
            l = e("./../Utils/HttpRequestHelper"),
            c = function() {
                function e(e, t) {
                    var i = this;
                    this.customCreateTitleBox = function(e, t) {
                        var n = document.createElement("div");
                        return s.ClassificationEntitiesManager.getAssetNameFromElement(i.bimAdmin.bim.name, i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId).then(function(o) {
                            var r = document.createElement("button");
                            r.type = "button", r.className = "elementProperties-assignButton-button";
                            var s = document.createElement("i");
                            s.className = "edit-icon", r.appendChild(s), r.onclick = function() {
                                new((new a.AssetAssignationToolWrapper).getToolConstructor())(i.bimAdmin.viewport, i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId).installTool()
                            }, n.appendChild(r);
                            var l = document.createElement("div");
                            n.appendChild(l), l.className = "bim-element-title", l.appendChild(document.createTextNode(o)), void 0 !== Bentley.PropertyGridWidget.prototype.createTitleBox && Bentley.PropertyGridWidget.prototype.createTitleBox.call(e, t)
                        }), n
                    }, this.loadWidgetConfigs = function(e) {
                        var t = Bentley.PropertyGridWidget.prototype.loadWidgetConfigs.call(i, e);
                        return t.id = "Vivo.VivoPropertyGridWidget", t
                    }, this.loadToolWidgets = function() {
                        Bentley.PropertyGridWidget.prototype.loadToolWidgets.call(i);
                        new n.VivoSelectionToolWrapper;
                        void 0 !== i.selectTool.element && (i.element.removeChild(i.selectTool.element), i.selectTool = void 0), i.selectTool = i.addToolButton({
                            id: [i.id, "Vivo.SelectionTool"],
                            tool: "Vivo.SelectionTool",
                            label: "Select Element",
                            class: "bim-panel-select-button"
                        })
                    }, this.loadElementTitleBox = function(e, t, n) {
                        i.titleBox = document.createElement("div");
                        var o = i.createTitleBox(e, t);
                        i.titleBox.appendChild(o), i.titleBox.className = "bim-title-box", n.appendChild(i.titleBox)
                    }, this.loadClassificationSection = function() {
                        if (!Cesium.defined(i.classifButtonsField)) {
                            i.classifButtonsField = document.createElement("div"), i.classifButtonsField.className = "elementProperties-Classif-box", i.element.appendChild(i.classifButtonsField);
                            var e = document.createElement("input");
                            e.type = "button", e.value = "Delete", e.className = "elementProperties-tool-button", e.onclick = function() {
                                window.confirm("The classification will be deleted!") && (void 0 !== i.bimAdmin.viewport.selectionSet.getSingleSelectedElement() && (new o.DeleteClassificationTool(i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId).deleteClassification(), i.bimAdmin.viewport.selectionSet.clear()))
                            }, i.classifButtonsField.appendChild(e);
                            var t = document.createElement("input");
                            t.type = "button", t.value = "Edit Height", t.className = "elementProperties-tool-button", t.onclick = function() {
                                var e = (new r.EditClassificationToolWrapper).getToolConstructor();
                                void 0 !== i.bimAdmin.viewport.selectionSet.getSingleSelectedElement() && new e(i.bimAdmin.viewport, i.config, i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId).installTool()
                            }, i.classifButtonsField.appendChild(t)
                        }
                    }, this.loadEmptyAssetSection = function(e) {
                        if (!Cesium.defined(i.emptyAssetField)) {
                            i.emptyAssetField = document.createElement("div"), i.emptyAssetField.className = "elementProperties-Assign-box", e.appendChild(i.emptyAssetField);
                            var t = document.createElement("div");
                            t.textContent = "No object associated", i.emptyAssetField.appendChild(t);
                            var n = document.createElement("input");
                            n.type = "button", n.value = "Assign", n.className = "elementProperties-tool-button", n.onclick = function() {
                                var e = (new a.AssetAssignationToolWrapper).getToolConstructor();
                                void 0 !== i.bimAdmin.viewport.selectionSet.getSingleSelectedElement() && new e(i.bimAdmin.viewport, i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId).installTool()
                            }, i.emptyAssetField.appendChild(n)
                        }
                    }, this.loadEmptyBimSection = function(e) {
                        if (void 0 === i.emptyBimField) {
                            i.emptyBimField = document.createElement("div"), i.emptyBimField.className = "elementProperties-Assign-box", e.appendChild(i.emptyBimField);
                            var t = document.createElement("div");
                            t.textContent = "No properties available.", i.emptyBimField.appendChild(t)
                        }
                    }, this.loadServerUnavailable = function(e) {
                        if (void 0 === i.serverUnavailableField) {
                            i.serverUnavailableField = document.createElement("div"), i.serverUnavailableField.className = "elementProperties-Assign-box";
                            var t = document.createElement("div");
                            t.textContent = "Server unavailable.", i.serverUnavailableField.appendChild(t)
                        }
                        e.appendChild(i.serverUnavailableField)
                    }, this.clearSections = function() {
                        Cesium.defined(i.emptyAssetField) && i._collapsableAsset.widgetContainer.contains(i.emptyAssetField) && (i._collapsableAsset.widgetContainer.removeChild(i.emptyAssetField), i.emptyAssetField = void 0), Cesium.defined(i.emptyBimField) && i._collapsableBimProperties.widgetContainer.contains(i.emptyBimField) && (i._collapsableBimProperties.widgetContainer.removeChild(i.emptyBimField), i.emptyBimField = void 0), Cesium.defined(i.classifButtonsField) && i.element.contains(i.classifButtonsField) && (i.element.removeChild(i.classifButtonsField), i.classifButtonsField = void 0), i._collapsableAsset && (i.element.removeChild(i._collapsableAsset.element), i._collapsableAsset = void 0), i._collapsableBimProperties && (i.element.removeChild(i._collapsableBimProperties.element), i._collapsableBimProperties = void 0), i.bimAdmin.viewport.scene.requestRender()
                    }, this.onPropertyDataChanged = function(e) {
                        (e.asset = Bentley.PropertyGridWidget.prototype.formatProperties.call(i, e.asset), e.bim = Bentley.PropertyGridWidget.prototype.formatProperties.call(i, e.bim), i.loadPropertySections(e, void 0), void 0 !== i.bimAdmin.viewport.selectionSet.getSingleSelectedElement()) && (void 0 !== s.ClassificationEntitiesManager.getOneClassificationInfos(i.bimAdmin.viewport.selectionSet.getSingleSelectedElement().elementId) && i.loadClassificationSection());
                        i.spinner.visible = !1, i.errorBox.visible = !1, i.emptyBox.visible = !1
                    }, this.loadPropertySections = function(e, t) {
                        l.HttpRequestHelper.IsBackendReachable() ? (void 0 === i._collapsableAsset && (i._collapsableAsset = Bentley.ToolWidget.prototype.addSection.call(i, {
                            id: ["0", "Object"],
                            label: "Object"
                        }), i.element.appendChild(i._collapsableAsset.element)), void 0 === i._collapsableBimProperties && (i._collapsableBimProperties = Bentley.ToolWidget.prototype.addSection.call(i, {
                            id: ["0", "Bim Properties"],
                            label: "Bim Properties"
                        }), i.element.appendChild(i._collapsableBimProperties.element)), void 0 === e.asset.categories ? i.loadEmptyAssetSection(i._collapsableAsset.widgetContainer) : (i.loadElementTitleBox(e.asset, t, i._collapsableAsset.widgetContainer), e.asset.categories.forEach(function(e) {
                            var t = Bentley.ToolWidget.prototype.addSection.call(i, {
                                id: e.category,
                                label: e.category
                            });
                            i._collapsableAsset.widgetContainer.appendChild(t.element);
                            var n = Bentley.ToolbarTemplateFactory.createPropertyDisplay.call(i, e.properties);
                            t.widgetContainer.appendChild(n)
                        })), void 0 === e.bim.categories ? i.loadEmptyBimSection(i._collapsableBimProperties.widgetContainer) : e.bim.categories.forEach(function(e) {
                            var t = Bentley.ToolWidget.prototype.addSection.call(i, {
                                id: e.category,
                                label: e.category
                            });
                            i._collapsableBimProperties.widgetContainer.appendChild(t.element);
                            var n = Bentley.ToolbarTemplateFactory.createPropertyDisplay.call(i, e.properties);
                            t.widgetContainer.appendChild(n)
                        })) : (void 0 === i._collapsableAsset && (i._collapsableAsset = Bentley.ToolWidget.prototype.addSection.call(i, {
                            id: ["0", "Object"],
                            label: "Error"
                        }), i.element.appendChild(i._collapsableAsset.element)), i.loadServerUnavailable(i._collapsableAsset.widgetContainer))
                    }, t.createTitleBox = this.customCreateTitleBox, this.classifButtonsField = void 0, this.emptyAssetField = void 0, this.emptyBimField = void 0, this.config = t, Bentley.PropertyGridWidget.call(this, e, t), this.widgetConfig = t
                }
                return e.prototype.reset = function() {}, e.prototype.refreshData = function() {}, e
            }();
        i.VivoPropertyGridWidget = c;
        var u = function() {
            return function() {
                this.getWidgetConstructor = function() {
                    return Bentley.extend(Bentley.PropertyGridWidget, c), c
                }
            }
        }();
        i.VivoPropertyGridWidgetWrapper = u
    }, {
        "./../Tools/AssetAssignationTool": 8,
        "./../Tools/ClassificationEntitiesManager": 9,
        "./../Tools/DeleteClassificationTool": 11,
        "./../Tools/EditClassificationTool": 12,
        "./../Tools/VivoSelectionTool": 22,
        "./../Utils/HttpRequestHelper": 25
    }],
    30: [function(e, t, i) {
        "use strict";
        Object.defineProperty(i, "__esModule", {
            value: !0
        });
        var n = e("./VisualOperationPropertyService"),
            o = e("./Tools/VivoTools"),
            r = e("./Tools/ClassificationEntitiesManager"),
            a = e("./Tools/PhotoNavigation/PhotoNavigation");
        new(function() {
            return function() {
                r.ClassificationEntitiesManager.Init(), o.VivoTools.Initialize();
                var e = $("#vivoscript").attr("tilesets"),
                    t = void 0 !== e ? e.split(",") : [],
                    i = new Bentley.TileSetViewer({
                        bimiumPath: "./",
                        toolbar: Bentley.ToolbarJSON.toolbar,
                        tools: Bentley.ToolsJSON.tools,
                        widgets: Bentley.WidgetsJSON.widgets,
                        tilesets: t,
                        propertyDataService: {
                            object: {
                                getPropertyData: function(e, t) {
                                    return n.VisualOperationPropertyService.GetProperties(e, t)
                                }
                            }
                        }
                    }, document.getElementsByTagName("body")[0]);
                i.activate().then(function() {
                    r.ClassificationEntitiesManager.PostInit(i), a.PhotoNavigation.initialize(i), o.VivoTools.PostInitialize(i)
                })
            }
        }())
    }, {
        "./Tools/ClassificationEntitiesManager": 9,
        "./Tools/PhotoNavigation/PhotoNavigation": 19,
        "./Tools/VivoTools": 23,
        "./VisualOperationPropertyService": 27
    }],
    31: [function(e, t, i) {
        ! function(e, i) {
            "use strict";
            "object" == typeof t && "object" == typeof t.exports ? t.exports = e.document ? i(e, !0) : function(e) {
                if (!e.document) throw new Error("jQuery requires a window with a document");
                return i(e)
            } : i(e)
        }("undefined" != typeof window ? window : this, function(e, t) {
            "use strict";
            var i = [],
                n = e.document,
                o = Object.getPrototypeOf,
                r = i.slice,
                a = i.concat,
                s = i.push,
                l = i.indexOf,
                c = {},
                u = c.toString,
                d = c.hasOwnProperty,
                p = d.toString,
                h = p.call(Object),
                f = {},
                m = function(e) {
                    return "function" == typeof e && "number" != typeof e.nodeType
                },
                v = function(e) {
                    return null != e && e === e.window
                },
                g = {
                    type: !0,
                    src: !0,
                    noModule: !0
                };

            function y(e, t, i) {
                var o, r = (t = t || n).createElement("script");
                if (r.text = e, i)
                    for (o in g) i[o] && (r[o] = i[o]);
                t.head.appendChild(r).parentNode.removeChild(r)
            }

            function C(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? c[u.call(e)] || "object" : typeof e
            }
            var b = function(e, t) {
                    return new b.fn.init(e, t)
                },
                x = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

            function w(e) {
                var t = !!e && "length" in e && e.length,
                    i = C(e);
                return !m(e) && !v(e) && ("array" === i || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
            }
            b.fn = b.prototype = {
                jquery: "3.3.1",
                constructor: b,
                length: 0,
                toArray: function() {
                    return r.call(this)
                },
                get: function(e) {
                    return null == e ? r.call(this) : e < 0 ? this[e + this.length] : this[e]
                },
                pushStack: function(e) {
                    var t = b.merge(this.constructor(), e);
                    return t.prevObject = this, t
                },
                each: function(e) {
                    return b.each(this, e)
                },
                map: function(e) {
                    return this.pushStack(b.map(this, function(t, i) {
                        return e.call(t, i, t)
                    }))
                },
                slice: function() {
                    return this.pushStack(r.apply(this, arguments))
                },
                first: function() {
                    return this.eq(0)
                },
                last: function() {
                    return this.eq(-1)
                },
                eq: function(e) {
                    var t = this.length,
                        i = +e + (e < 0 ? t : 0);
                    return this.pushStack(i >= 0 && i < t ? [this[i]] : [])
                },
                end: function() {
                    return this.prevObject || this.constructor()
                },
                push: s,
                sort: i.sort,
                splice: i.splice
            }, b.extend = b.fn.extend = function() {
                var e, t, i, n, o, r, a = arguments[0] || {},
                    s = 1,
                    l = arguments.length,
                    c = !1;
                for ("boolean" == typeof a && (c = a, a = arguments[s] || {}, s++), "object" == typeof a || m(a) || (a = {}), s === l && (a = this, s--); s < l; s++)
                    if (null != (e = arguments[s]))
                        for (t in e) i = a[t], a !== (n = e[t]) && (c && n && (b.isPlainObject(n) || (o = Array.isArray(n))) ? (o ? (o = !1, r = i && Array.isArray(i) ? i : []) : r = i && b.isPlainObject(i) ? i : {}, a[t] = b.extend(c, r, n)) : void 0 !== n && (a[t] = n));
                return a
            }, b.extend({
                expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
                isReady: !0,
                error: function(e) {
                    throw new Error(e)
                },
                noop: function() {},
                isPlainObject: function(e) {
                    var t, i;
                    return !(!e || "[object Object]" !== u.call(e)) && (!(t = o(e)) || "function" == typeof(i = d.call(t, "constructor") && t.constructor) && p.call(i) === h)
                },
                isEmptyObject: function(e) {
                    var t;
                    for (t in e) return !1;
                    return !0
                },
                globalEval: function(e) {
                    y(e)
                },
                each: function(e, t) {
                    var i, n = 0;
                    if (w(e))
                        for (i = e.length; n < i && !1 !== t.call(e[n], n, e[n]); n++);
                    else
                        for (n in e)
                            if (!1 === t.call(e[n], n, e[n])) break;
                    return e
                },
                trim: function(e) {
                    return null == e ? "" : (e + "").replace(x, "")
                },
                makeArray: function(e, t) {
                    var i = t || [];
                    return null != e && (w(Object(e)) ? b.merge(i, "string" == typeof e ? [e] : e) : s.call(i, e)), i
                },
                inArray: function(e, t, i) {
                    return null == t ? -1 : l.call(t, e, i)
                },
                merge: function(e, t) {
                    for (var i = +t.length, n = 0, o = e.length; n < i; n++) e[o++] = t[n];
                    return e.length = o, e
                },
                grep: function(e, t, i) {
                    for (var n = [], o = 0, r = e.length, a = !i; o < r; o++) !t(e[o], o) !== a && n.push(e[o]);
                    return n
                },
                map: function(e, t, i) {
                    var n, o, r = 0,
                        s = [];
                    if (w(e))
                        for (n = e.length; r < n; r++) null != (o = t(e[r], r, i)) && s.push(o);
                    else
                        for (r in e) null != (o = t(e[r], r, i)) && s.push(o);
                    return a.apply([], s)
                },
                guid: 1,
                support: f
            }), "function" == typeof Symbol && (b.fn[Symbol.iterator] = i[Symbol.iterator]), b.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
                c["[object " + t + "]"] = t.toLowerCase()
            });
            var T = function(e) {
                var t, i, n, o, r, a, s, l, c, u, d, p, h, f, m, v, g, y, C, b = "sizzle" + 1 * new Date,
                    x = e.document,
                    w = 0,
                    T = 0,
                    E = ae(),
                    P = ae(),
                    N = ae(),
                    A = function(e, t) {
                        return e === t && (d = !0), 0
                    },
                    S = {}.hasOwnProperty,
                    M = [],
                    B = M.pop,
                    H = M.push,
                    I = M.push,
                    k = M.slice,
                    _ = function(e, t) {
                        for (var i = 0, n = e.length; i < n; i++)
                            if (e[i] === t) return i;
                        return -1
                    },
                    D = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                    O = "[\\x20\\t\\r\\n\\f]",
                    L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
                    U = "\\[" + O + "*(" + L + ")(?:" + O + "*([*^$|!~]?=)" + O + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + O + "*\\]",
                    F = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + U + ")*)|.*)\\)|)",
                    j = new RegExp(O + "+", "g"),
                    R = new RegExp("^" + O + "+|((?:^|[^\\\\])(?:\\\\.)*)" + O + "+$", "g"),
                    q = new RegExp("^" + O + "*," + O + "*"),
                    W = new RegExp("^" + O + "*([>+~]|" + O + ")" + O + "*"),
                    G = new RegExp("=" + O + "*([^\\]'\"]*?)" + O + "*\\]", "g"),
                    V = new RegExp(F),
                    z = new RegExp("^" + L + "$"),
                    X = {
                        ID: new RegExp("^#(" + L + ")"),
                        CLASS: new RegExp("^\\.(" + L + ")"),
                        TAG: new RegExp("^(" + L + "|[*])"),
                        ATTR: new RegExp("^" + U),
                        PSEUDO: new RegExp("^" + F),
                        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + O + "*(even|odd|(([+-]|)(\\d*)n|)" + O + "*(?:([+-]|)" + O + "*(\\d+)|))" + O + "*\\)|)", "i"),
                        bool: new RegExp("^(?:" + D + ")$", "i"),
                        needsContext: new RegExp("^" + O + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + O + "*((?:-\\d)?\\d*)" + O + "*\\)|)(?=[^-]|$)", "i")
                    },
                    $ = /^(?:input|select|textarea|button)$/i,
                    Y = /^h\d$/i,
                    J = /^[^{]+\{\s*\[native \w/,
                    K = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                    Q = /[+~]/,
                    Z = new RegExp("\\\\([\\da-f]{1,6}" + O + "?|(" + O + ")|.)", "ig"),
                    ee = function(e, t, i) {
                        var n = "0x" + t - 65536;
                        return n != n || i ? t : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
                    },
                    te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
                    ie = function(e, t) {
                        return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
                    },
                    ne = function() {
                        p()
                    },
                    oe = ye(function(e) {
                        return !0 === e.disabled && ("form" in e || "label" in e)
                    }, {
                        dir: "parentNode",
                        next: "legend"
                    });
                try {
                    I.apply(M = k.call(x.childNodes), x.childNodes), M[x.childNodes.length].nodeType
                } catch (e) {
                    I = {
                        apply: M.length ? function(e, t) {
                            H.apply(e, k.call(t))
                        } : function(e, t) {
                            for (var i = e.length, n = 0; e[i++] = t[n++];);
                            e.length = i - 1
                        }
                    }
                }

                function re(e, t, n, o) {
                    var r, s, c, u, d, f, g, y = t && t.ownerDocument,
                        w = t ? t.nodeType : 9;
                    if (n = n || [], "string" != typeof e || !e || 1 !== w && 9 !== w && 11 !== w) return n;
                    if (!o && ((t ? t.ownerDocument || t : x) !== h && p(t), t = t || h, m)) {
                        if (11 !== w && (d = K.exec(e)))
                            if (r = d[1]) {
                                if (9 === w) {
                                    if (!(c = t.getElementById(r))) return n;
                                    if (c.id === r) return n.push(c), n
                                } else if (y && (c = y.getElementById(r)) && C(t, c) && c.id === r) return n.push(c), n
                            } else {
                                if (d[2]) return I.apply(n, t.getElementsByTagName(e)), n;
                                if ((r = d[3]) && i.getElementsByClassName && t.getElementsByClassName) return I.apply(n, t.getElementsByClassName(r)), n
                            } if (i.qsa && !N[e + " "] && (!v || !v.test(e))) {
                            if (1 !== w) y = t, g = e;
                            else if ("object" !== t.nodeName.toLowerCase()) {
                                for ((u = t.getAttribute("id")) ? u = u.replace(te, ie) : t.setAttribute("id", u = b), s = (f = a(e)).length; s--;) f[s] = "#" + u + " " + ge(f[s]);
                                g = f.join(","), y = Q.test(e) && me(t.parentNode) || t
                            }
                            if (g) try {
                                return I.apply(n, y.querySelectorAll(g)), n
                            } catch (e) {} finally {
                                u === b && t.removeAttribute("id")
                            }
                        }
                    }
                    return l(e.replace(R, "$1"), t, n, o)
                }

                function ae() {
                    var e = [];
                    return function t(i, o) {
                        return e.push(i + " ") > n.cacheLength && delete t[e.shift()], t[i + " "] = o
                    }
                }

                function se(e) {
                    return e[b] = !0, e
                }

                function le(e) {
                    var t = h.createElement("fieldset");
                    try {
                        return !!e(t)
                    } catch (e) {
                        return !1
                    } finally {
                        t.parentNode && t.parentNode.removeChild(t), t = null
                    }
                }

                function ce(e, t) {
                    for (var i = e.split("|"), o = i.length; o--;) n.attrHandle[i[o]] = t
                }

                function ue(e, t) {
                    var i = t && e,
                        n = i && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
                    if (n) return n;
                    if (i)
                        for (; i = i.nextSibling;)
                            if (i === t) return -1;
                    return e ? 1 : -1
                }

                function de(e) {
                    return function(t) {
                        return "input" === t.nodeName.toLowerCase() && t.type === e
                    }
                }

                function pe(e) {
                    return function(t) {
                        var i = t.nodeName.toLowerCase();
                        return ("input" === i || "button" === i) && t.type === e
                    }
                }

                function he(e) {
                    return function(t) {
                        return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && oe(t) === e : t.disabled === e : "label" in t && t.disabled === e
                    }
                }

                function fe(e) {
                    return se(function(t) {
                        return t = +t, se(function(i, n) {
                            for (var o, r = e([], i.length, t), a = r.length; a--;) i[o = r[a]] && (i[o] = !(n[o] = i[o]))
                        })
                    })
                }

                function me(e) {
                    return e && void 0 !== e.getElementsByTagName && e
                }
                i = re.support = {}, r = re.isXML = function(e) {
                    var t = e && (e.ownerDocument || e).documentElement;
                    return !!t && "HTML" !== t.nodeName
                }, p = re.setDocument = function(e) {
                    var t, o, a = e ? e.ownerDocument || e : x;
                    return a !== h && 9 === a.nodeType && a.documentElement ? (f = (h = a).documentElement, m = !r(h), x !== h && (o = h.defaultView) && o.top !== o && (o.addEventListener ? o.addEventListener("unload", ne, !1) : o.attachEvent && o.attachEvent("onunload", ne)), i.attributes = le(function(e) {
                        return e.className = "i", !e.getAttribute("className")
                    }), i.getElementsByTagName = le(function(e) {
                        return e.appendChild(h.createComment("")), !e.getElementsByTagName("*").length
                    }), i.getElementsByClassName = J.test(h.getElementsByClassName), i.getById = le(function(e) {
                        return f.appendChild(e).id = b, !h.getElementsByName || !h.getElementsByName(b).length
                    }), i.getById ? (n.filter.ID = function(e) {
                        var t = e.replace(Z, ee);
                        return function(e) {
                            return e.getAttribute("id") === t
                        }
                    }, n.find.ID = function(e, t) {
                        if (void 0 !== t.getElementById && m) {
                            var i = t.getElementById(e);
                            return i ? [i] : []
                        }
                    }) : (n.filter.ID = function(e) {
                        var t = e.replace(Z, ee);
                        return function(e) {
                            var i = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                            return i && i.value === t
                        }
                    }, n.find.ID = function(e, t) {
                        if (void 0 !== t.getElementById && m) {
                            var i, n, o, r = t.getElementById(e);
                            if (r) {
                                if ((i = r.getAttributeNode("id")) && i.value === e) return [r];
                                for (o = t.getElementsByName(e), n = 0; r = o[n++];)
                                    if ((i = r.getAttributeNode("id")) && i.value === e) return [r]
                            }
                            return []
                        }
                    }), n.find.TAG = i.getElementsByTagName ? function(e, t) {
                        return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : i.qsa ? t.querySelectorAll(e) : void 0
                    } : function(e, t) {
                        var i, n = [],
                            o = 0,
                            r = t.getElementsByTagName(e);
                        if ("*" === e) {
                            for (; i = r[o++];) 1 === i.nodeType && n.push(i);
                            return n
                        }
                        return r
                    }, n.find.CLASS = i.getElementsByClassName && function(e, t) {
                        if (void 0 !== t.getElementsByClassName && m) return t.getElementsByClassName(e)
                    }, g = [], v = [], (i.qsa = J.test(h.querySelectorAll)) && (le(function(e) {
                        f.appendChild(e).innerHTML = "<a id='" + b + "'></a><select id='" + b + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + O + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + O + "*(?:value|" + D + ")"), e.querySelectorAll("[id~=" + b + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + b + "+*").length || v.push(".#.+[+~]")
                    }), le(function(e) {
                        e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                        var t = h.createElement("input");
                        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + O + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), f.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
                    })), (i.matchesSelector = J.test(y = f.matches || f.webkitMatchesSelector || f.mozMatchesSelector || f.oMatchesSelector || f.msMatchesSelector)) && le(function(e) {
                        i.disconnectedMatch = y.call(e, "*"), y.call(e, "[s!='']:x"), g.push("!=", F)
                    }), v = v.length && new RegExp(v.join("|")), g = g.length && new RegExp(g.join("|")), t = J.test(f.compareDocumentPosition), C = t || J.test(f.contains) ? function(e, t) {
                        var i = 9 === e.nodeType ? e.documentElement : e,
                            n = t && t.parentNode;
                        return e === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(n)))
                    } : function(e, t) {
                        if (t)
                            for (; t = t.parentNode;)
                                if (t === e) return !0;
                        return !1
                    }, A = t ? function(e, t) {
                        if (e === t) return d = !0, 0;
                        var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                        return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !i.sortDetached && t.compareDocumentPosition(e) === n ? e === h || e.ownerDocument === x && C(x, e) ? -1 : t === h || t.ownerDocument === x && C(x, t) ? 1 : u ? _(u, e) - _(u, t) : 0 : 4 & n ? -1 : 1)
                    } : function(e, t) {
                        if (e === t) return d = !0, 0;
                        var i, n = 0,
                            o = e.parentNode,
                            r = t.parentNode,
                            a = [e],
                            s = [t];
                        if (!o || !r) return e === h ? -1 : t === h ? 1 : o ? -1 : r ? 1 : u ? _(u, e) - _(u, t) : 0;
                        if (o === r) return ue(e, t);
                        for (i = e; i = i.parentNode;) a.unshift(i);
                        for (i = t; i = i.parentNode;) s.unshift(i);
                        for (; a[n] === s[n];) n++;
                        return n ? ue(a[n], s[n]) : a[n] === x ? -1 : s[n] === x ? 1 : 0
                    }, h) : h
                }, re.matches = function(e, t) {
                    return re(e, null, null, t)
                }, re.matchesSelector = function(e, t) {
                    if ((e.ownerDocument || e) !== h && p(e), t = t.replace(G, "='$1']"), i.matchesSelector && m && !N[t + " "] && (!g || !g.test(t)) && (!v || !v.test(t))) try {
                        var n = y.call(e, t);
                        if (n || i.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                    } catch (e) {}
                    return re(t, h, null, [e]).length > 0
                }, re.contains = function(e, t) {
                    return (e.ownerDocument || e) !== h && p(e), C(e, t)
                }, re.attr = function(e, t) {
                    (e.ownerDocument || e) !== h && p(e);
                    var o = n.attrHandle[t.toLowerCase()],
                        r = o && S.call(n.attrHandle, t.toLowerCase()) ? o(e, t, !m) : void 0;
                    return void 0 !== r ? r : i.attributes || !m ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
                }, re.escape = function(e) {
                    return (e + "").replace(te, ie)
                }, re.error = function(e) {
                    throw new Error("Syntax error, unrecognized expression: " + e)
                }, re.uniqueSort = function(e) {
                    var t, n = [],
                        o = 0,
                        r = 0;
                    if (d = !i.detectDuplicates, u = !i.sortStable && e.slice(0), e.sort(A), d) {
                        for (; t = e[r++];) t === e[r] && (o = n.push(r));
                        for (; o--;) e.splice(n[o], 1)
                    }
                    return u = null, e
                }, o = re.getText = function(e) {
                    var t, i = "",
                        n = 0,
                        r = e.nodeType;
                    if (r) {
                        if (1 === r || 9 === r || 11 === r) {
                            if ("string" == typeof e.textContent) return e.textContent;
                            for (e = e.firstChild; e; e = e.nextSibling) i += o(e)
                        } else if (3 === r || 4 === r) return e.nodeValue
                    } else
                        for (; t = e[n++];) i += o(t);
                    return i
                }, (n = re.selectors = {
                    cacheLength: 50,
                    createPseudo: se,
                    match: X,
                    attrHandle: {},
                    find: {},
                    relative: {
                        ">": {
                            dir: "parentNode",
                            first: !0
                        },
                        " ": {
                            dir: "parentNode"
                        },
                        "+": {
                            dir: "previousSibling",
                            first: !0
                        },
                        "~": {
                            dir: "previousSibling"
                        }
                    },
                    preFilter: {
                        ATTR: function(e) {
                            return e[1] = e[1].replace(Z, ee), e[3] = (e[3] || e[4] || e[5] || "").replace(Z, ee), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                        },
                        CHILD: function(e) {
                            return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || re.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && re.error(e[0]), e
                        },
                        PSEUDO: function(e) {
                            var t, i = !e[6] && e[2];
                            return X.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : i && V.test(i) && (t = a(i, !0)) && (t = i.indexOf(")", i.length - t) - i.length) && (e[0] = e[0].slice(0, t), e[2] = i.slice(0, t)), e.slice(0, 3))
                        }
                    },
                    filter: {
                        TAG: function(e) {
                            var t = e.replace(Z, ee).toLowerCase();
                            return "*" === e ? function() {
                                return !0
                            } : function(e) {
                                return e.nodeName && e.nodeName.toLowerCase() === t
                            }
                        },
                        CLASS: function(e) {
                            var t = E[e + " "];
                            return t || (t = new RegExp("(^|" + O + ")" + e + "(" + O + "|$)")) && E(e, function(e) {
                                return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                            })
                        },
                        ATTR: function(e, t, i) {
                            return function(n) {
                                var o = re.attr(n, e);
                                return null == o ? "!=" === t : !t || (o += "", "=" === t ? o === i : "!=" === t ? o !== i : "^=" === t ? i && 0 === o.indexOf(i) : "*=" === t ? i && o.indexOf(i) > -1 : "$=" === t ? i && o.slice(-i.length) === i : "~=" === t ? (" " + o.replace(j, " ") + " ").indexOf(i) > -1 : "|=" === t && (o === i || o.slice(0, i.length + 1) === i + "-"))
                            }
                        },
                        CHILD: function(e, t, i, n, o) {
                            var r = "nth" !== e.slice(0, 3),
                                a = "last" !== e.slice(-4),
                                s = "of-type" === t;
                            return 1 === n && 0 === o ? function(e) {
                                return !!e.parentNode
                            } : function(t, i, l) {
                                var c, u, d, p, h, f, m = r !== a ? "nextSibling" : "previousSibling",
                                    v = t.parentNode,
                                    g = s && t.nodeName.toLowerCase(),
                                    y = !l && !s,
                                    C = !1;
                                if (v) {
                                    if (r) {
                                        for (; m;) {
                                            for (p = t; p = p[m];)
                                                if (s ? p.nodeName.toLowerCase() === g : 1 === p.nodeType) return !1;
                                            f = m = "only" === e && !f && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (f = [a ? v.firstChild : v.lastChild], a && y) {
                                        for (C = (h = (c = (u = (d = (p = v)[b] || (p[b] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] || [])[0] === w && c[1]) && c[2], p = h && v.childNodes[h]; p = ++h && p && p[m] || (C = h = 0) || f.pop();)
                                            if (1 === p.nodeType && ++C && p === t) {
                                                u[e] = [w, h, C];
                                                break
                                            }
                                    } else if (y && (C = h = (c = (u = (d = (p = t)[b] || (p[b] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] || [])[0] === w && c[1]), !1 === C)
                                        for (;
                                            (p = ++h && p && p[m] || (C = h = 0) || f.pop()) && ((s ? p.nodeName.toLowerCase() !== g : 1 !== p.nodeType) || !++C || (y && ((u = (d = p[b] || (p[b] = {}))[p.uniqueID] || (d[p.uniqueID] = {}))[e] = [w, C]), p !== t)););
                                    return (C -= o) === n || C % n == 0 && C / n >= 0
                                }
                            }
                        },
                        PSEUDO: function(e, t) {
                            var i, o = n.pseudos[e] || n.setFilters[e.toLowerCase()] || re.error("unsupported pseudo: " + e);
                            return o[b] ? o(t) : o.length > 1 ? (i = [e, e, "", t], n.setFilters.hasOwnProperty(e.toLowerCase()) ? se(function(e, i) {
                                for (var n, r = o(e, t), a = r.length; a--;) e[n = _(e, r[a])] = !(i[n] = r[a])
                            }) : function(e) {
                                return o(e, 0, i)
                            }) : o
                        }
                    },
                    pseudos: {
                        not: se(function(e) {
                            var t = [],
                                i = [],
                                n = s(e.replace(R, "$1"));
                            return n[b] ? se(function(e, t, i, o) {
                                for (var r, a = n(e, null, o, []), s = e.length; s--;)(r = a[s]) && (e[s] = !(t[s] = r))
                            }) : function(e, o, r) {
                                return t[0] = e, n(t, null, r, i), t[0] = null, !i.pop()
                            }
                        }),
                        has: se(function(e) {
                            return function(t) {
                                return re(e, t).length > 0
                            }
                        }),
                        contains: se(function(e) {
                            return e = e.replace(Z, ee),
                                function(t) {
                                    return (t.textContent || t.innerText || o(t)).indexOf(e) > -1
                                }
                        }),
                        lang: se(function(e) {
                            return z.test(e || "") || re.error("unsupported lang: " + e), e = e.replace(Z, ee).toLowerCase(),
                                function(t) {
                                    var i;
                                    do {
                                        if (i = m ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (i = i.toLowerCase()) === e || 0 === i.indexOf(e + "-")
                                    } while ((t = t.parentNode) && 1 === t.nodeType);
                                    return !1
                                }
                        }),
                        target: function(t) {
                            var i = e.location && e.location.hash;
                            return i && i.slice(1) === t.id
                        },
                        root: function(e) {
                            return e === f
                        },
                        focus: function(e) {
                            return e === h.activeElement && (!h.hasFocus || h.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                        },
                        enabled: he(!1),
                        disabled: he(!0),
                        checked: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && !!e.checked || "option" === t && !!e.selected
                        },
                        selected: function(e) {
                            return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                        },
                        empty: function(e) {
                            for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeType < 6) return !1;
                            return !0
                        },
                        parent: function(e) {
                            return !n.pseudos.empty(e)
                        },
                        header: function(e) {
                            return Y.test(e.nodeName)
                        },
                        input: function(e) {
                            return $.test(e.nodeName)
                        },
                        button: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && "button" === e.type || "button" === t
                        },
                        text: function(e) {
                            var t;
                            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                        },
                        first: fe(function() {
                            return [0]
                        }),
                        last: fe(function(e, t) {
                            return [t - 1]
                        }),
                        eq: fe(function(e, t, i) {
                            return [i < 0 ? i + t : i]
                        }),
                        even: fe(function(e, t) {
                            for (var i = 0; i < t; i += 2) e.push(i);
                            return e
                        }),
                        odd: fe(function(e, t) {
                            for (var i = 1; i < t; i += 2) e.push(i);
                            return e
                        }),
                        lt: fe(function(e, t, i) {
                            for (var n = i < 0 ? i + t : i; --n >= 0;) e.push(n);
                            return e
                        }),
                        gt: fe(function(e, t, i) {
                            for (var n = i < 0 ? i + t : i; ++n < t;) e.push(n);
                            return e
                        })
                    }
                }).pseudos.nth = n.pseudos.eq;
                for (t in {
                        radio: !0,
                        checkbox: !0,
                        file: !0,
                        password: !0,
                        image: !0
                    }) n.pseudos[t] = de(t);
                for (t in {
                        submit: !0,
                        reset: !0
                    }) n.pseudos[t] = pe(t);

                function ve() {}

                function ge(e) {
                    for (var t = 0, i = e.length, n = ""; t < i; t++) n += e[t].value;
                    return n
                }

                function ye(e, t, i) {
                    var n = t.dir,
                        o = t.next,
                        r = o || n,
                        a = i && "parentNode" === r,
                        s = T++;
                    return t.first ? function(t, i, o) {
                        for (; t = t[n];)
                            if (1 === t.nodeType || a) return e(t, i, o);
                        return !1
                    } : function(t, i, l) {
                        var c, u, d, p = [w, s];
                        if (l) {
                            for (; t = t[n];)
                                if ((1 === t.nodeType || a) && e(t, i, l)) return !0
                        } else
                            for (; t = t[n];)
                                if (1 === t.nodeType || a)
                                    if (u = (d = t[b] || (t[b] = {}))[t.uniqueID] || (d[t.uniqueID] = {}), o && o === t.nodeName.toLowerCase()) t = t[n] || t;
                                    else {
                                        if ((c = u[r]) && c[0] === w && c[1] === s) return p[2] = c[2];
                                        if (u[r] = p, p[2] = e(t, i, l)) return !0
                                    } return !1
                    }
                }

                function Ce(e) {
                    return e.length > 1 ? function(t, i, n) {
                        for (var o = e.length; o--;)
                            if (!e[o](t, i, n)) return !1;
                        return !0
                    } : e[0]
                }

                function be(e, t, i, n, o) {
                    for (var r, a = [], s = 0, l = e.length, c = null != t; s < l; s++)(r = e[s]) && (i && !i(r, n, o) || (a.push(r), c && t.push(s)));
                    return a
                }

                function xe(e, t, i, n, o, r) {
                    return n && !n[b] && (n = xe(n)), o && !o[b] && (o = xe(o, r)), se(function(r, a, s, l) {
                        var c, u, d, p = [],
                            h = [],
                            f = a.length,
                            m = r || function(e, t, i) {
                                for (var n = 0, o = t.length; n < o; n++) re(e, t[n], i);
                                return i
                            }(t || "*", s.nodeType ? [s] : s, []),
                            v = !e || !r && t ? m : be(m, p, e, s, l),
                            g = i ? o || (r ? e : f || n) ? [] : a : v;
                        if (i && i(v, g, s, l), n)
                            for (c = be(g, h), n(c, [], s, l), u = c.length; u--;)(d = c[u]) && (g[h[u]] = !(v[h[u]] = d));
                        if (r) {
                            if (o || e) {
                                if (o) {
                                    for (c = [], u = g.length; u--;)(d = g[u]) && c.push(v[u] = d);
                                    o(null, g = [], c, l)
                                }
                                for (u = g.length; u--;)(d = g[u]) && (c = o ? _(r, d) : p[u]) > -1 && (r[c] = !(a[c] = d))
                            }
                        } else g = be(g === a ? g.splice(f, g.length) : g), o ? o(null, a, g, l) : I.apply(a, g)
                    })
                }

                function we(e) {
                    for (var t, i, o, r = e.length, a = n.relative[e[0].type], s = a || n.relative[" "], l = a ? 1 : 0, u = ye(function(e) {
                            return e === t
                        }, s, !0), d = ye(function(e) {
                            return _(t, e) > -1
                        }, s, !0), p = [function(e, i, n) {
                            var o = !a && (n || i !== c) || ((t = i).nodeType ? u(e, i, n) : d(e, i, n));
                            return t = null, o
                        }]; l < r; l++)
                        if (i = n.relative[e[l].type]) p = [ye(Ce(p), i)];
                        else {
                            if ((i = n.filter[e[l].type].apply(null, e[l].matches))[b]) {
                                for (o = ++l; o < r && !n.relative[e[o].type]; o++);
                                return xe(l > 1 && Ce(p), l > 1 && ge(e.slice(0, l - 1).concat({
                                    value: " " === e[l - 2].type ? "*" : ""
                                })).replace(R, "$1"), i, l < o && we(e.slice(l, o)), o < r && we(e = e.slice(o)), o < r && ge(e))
                            }
                            p.push(i)
                        } return Ce(p)
                }
                return ve.prototype = n.filters = n.pseudos, n.setFilters = new ve, a = re.tokenize = function(e, t) {
                    var i, o, r, a, s, l, c, u = P[e + " "];
                    if (u) return t ? 0 : u.slice(0);
                    for (s = e, l = [], c = n.preFilter; s;) {
                        i && !(o = q.exec(s)) || (o && (s = s.slice(o[0].length) || s), l.push(r = [])), i = !1, (o = W.exec(s)) && (i = o.shift(), r.push({
                            value: i,
                            type: o[0].replace(R, " ")
                        }), s = s.slice(i.length));
                        for (a in n.filter) !(o = X[a].exec(s)) || c[a] && !(o = c[a](o)) || (i = o.shift(), r.push({
                            value: i,
                            type: a,
                            matches: o
                        }), s = s.slice(i.length));
                        if (!i) break
                    }
                    return t ? s.length : s ? re.error(e) : P(e, l).slice(0)
                }, s = re.compile = function(e, t) {
                    var i, o, r, s, l, u, d = [],
                        f = [],
                        v = N[e + " "];
                    if (!v) {
                        for (t || (t = a(e)), i = t.length; i--;)(v = we(t[i]))[b] ? d.push(v) : f.push(v);
                        (v = N(e, (o = f, s = (r = d).length > 0, l = o.length > 0, u = function(e, t, i, a, u) {
                            var d, f, v, g = 0,
                                y = "0",
                                C = e && [],
                                b = [],
                                x = c,
                                T = e || l && n.find.TAG("*", u),
                                E = w += null == x ? 1 : Math.random() || .1,
                                P = T.length;
                            for (u && (c = t === h || t || u); y !== P && null != (d = T[y]); y++) {
                                if (l && d) {
                                    for (f = 0, t || d.ownerDocument === h || (p(d), i = !m); v = o[f++];)
                                        if (v(d, t || h, i)) {
                                            a.push(d);
                                            break
                                        } u && (w = E)
                                }
                                s && ((d = !v && d) && g--, e && C.push(d))
                            }
                            if (g += y, s && y !== g) {
                                for (f = 0; v = r[f++];) v(C, b, t, i);
                                if (e) {
                                    if (g > 0)
                                        for (; y--;) C[y] || b[y] || (b[y] = B.call(a));
                                    b = be(b)
                                }
                                I.apply(a, b), u && !e && b.length > 0 && g + r.length > 1 && re.uniqueSort(a)
                            }
                            return u && (w = E, c = x), C
                        }, s ? se(u) : u))).selector = e
                    }
                    return v
                }, l = re.select = function(e, t, i, o) {
                    var r, l, c, u, d, p = "function" == typeof e && e,
                        h = !o && a(e = p.selector || e);
                    if (i = i || [], 1 === h.length) {
                        if ((l = h[0] = h[0].slice(0)).length > 2 && "ID" === (c = l[0]).type && 9 === t.nodeType && m && n.relative[l[1].type]) {
                            if (!(t = (n.find.ID(c.matches[0].replace(Z, ee), t) || [])[0])) return i;
                            p && (t = t.parentNode), e = e.slice(l.shift().value.length)
                        }
                        for (r = X.needsContext.test(e) ? 0 : l.length; r-- && (c = l[r], !n.relative[u = c.type]);)
                            if ((d = n.find[u]) && (o = d(c.matches[0].replace(Z, ee), Q.test(l[0].type) && me(t.parentNode) || t))) {
                                if (l.splice(r, 1), !(e = o.length && ge(l))) return I.apply(i, o), i;
                                break
                            }
                    }
                    return (p || s(e, h))(o, t, !m, i, !t || Q.test(e) && me(t.parentNode) || t), i
                }, i.sortStable = b.split("").sort(A).join("") === b, i.detectDuplicates = !!d, p(), i.sortDetached = le(function(e) {
                    return 1 & e.compareDocumentPosition(h.createElement("fieldset"))
                }), le(function(e) {
                    return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
                }) || ce("type|href|height|width", function(e, t, i) {
                    if (!i) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
                }), i.attributes && le(function(e) {
                    return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
                }) || ce("value", function(e, t, i) {
                    if (!i && "input" === e.nodeName.toLowerCase()) return e.defaultValue
                }), le(function(e) {
                    return null == e.getAttribute("disabled")
                }) || ce(D, function(e, t, i) {
                    var n;
                    if (!i) return !0 === e[t] ? t.toLowerCase() : (n = e.getAttributeNode(t)) && n.specified ? n.value : null
                }), re
            }(e);
            b.find = T, b.expr = T.selectors, b.expr[":"] = b.expr.pseudos, b.uniqueSort = b.unique = T.uniqueSort, b.text = T.getText, b.isXMLDoc = T.isXML, b.contains = T.contains, b.escapeSelector = T.escape;
            var E = function(e, t, i) {
                    for (var n = [], o = void 0 !== i;
                        (e = e[t]) && 9 !== e.nodeType;)
                        if (1 === e.nodeType) {
                            if (o && b(e).is(i)) break;
                            n.push(e)
                        } return n
                },
                P = function(e, t) {
                    for (var i = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && i.push(e);
                    return i
                },
                N = b.expr.match.needsContext;

            function A(e, t) {
                return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
            }
            var S = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

            function M(e, t, i) {
                return m(t) ? b.grep(e, function(e, n) {
                    return !!t.call(e, n, e) !== i
                }) : t.nodeType ? b.grep(e, function(e) {
                    return e === t !== i
                }) : "string" != typeof t ? b.grep(e, function(e) {
                    return l.call(t, e) > -1 !== i
                }) : b.filter(t, e, i)
            }
            b.filter = function(e, t, i) {
                var n = t[0];
                return i && (e = ":not(" + e + ")"), 1 === t.length && 1 === n.nodeType ? b.find.matchesSelector(n, e) ? [n] : [] : b.find.matches(e, b.grep(t, function(e) {
                    return 1 === e.nodeType
                }))
            }, b.fn.extend({
                find: function(e) {
                    var t, i, n = this.length,
                        o = this;
                    if ("string" != typeof e) return this.pushStack(b(e).filter(function() {
                        for (t = 0; t < n; t++)
                            if (b.contains(o[t], this)) return !0
                    }));
                    for (i = this.pushStack([]), t = 0; t < n; t++) b.find(e, o[t], i);
                    return n > 1 ? b.uniqueSort(i) : i
                },
                filter: function(e) {
                    return this.pushStack(M(this, e || [], !1))
                },
                not: function(e) {
                    return this.pushStack(M(this, e || [], !0))
                },
                is: function(e) {
                    return !!M(this, "string" == typeof e && N.test(e) ? b(e) : e || [], !1).length
                }
            });
            var B, H = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
            (b.fn.init = function(e, t, i) {
                var o, r;
                if (!e) return this;
                if (i = i || B, "string" == typeof e) {
                    if (!(o = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : H.exec(e)) || !o[1] && t) return !t || t.jquery ? (t || i).find(e) : this.constructor(t).find(e);
                    if (o[1]) {
                        if (t = t instanceof b ? t[0] : t, b.merge(this, b.parseHTML(o[1], t && t.nodeType ? t.ownerDocument || t : n, !0)), S.test(o[1]) && b.isPlainObject(t))
                            for (o in t) m(this[o]) ? this[o](t[o]) : this.attr(o, t[o]);
                        return this
                    }
                    return (r = n.getElementById(o[2])) && (this[0] = r, this.length = 1), this
                }
                return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== i.ready ? i.ready(e) : e(b) : b.makeArray(e, this)
            }).prototype = b.fn, B = b(n);
            var I = /^(?:parents|prev(?:Until|All))/,
                k = {
                    children: !0,
                    contents: !0,
                    next: !0,
                    prev: !0
                };

            function _(e, t) {
                for (;
                    (e = e[t]) && 1 !== e.nodeType;);
                return e
            }
            b.fn.extend({
                has: function(e) {
                    var t = b(e, this),
                        i = t.length;
                    return this.filter(function() {
                        for (var e = 0; e < i; e++)
                            if (b.contains(this, t[e])) return !0
                    })
                },
                closest: function(e, t) {
                    var i, n = 0,
                        o = this.length,
                        r = [],
                        a = "string" != typeof e && b(e);
                    if (!N.test(e))
                        for (; n < o; n++)
                            for (i = this[n]; i && i !== t; i = i.parentNode)
                                if (i.nodeType < 11 && (a ? a.index(i) > -1 : 1 === i.nodeType && b.find.matchesSelector(i, e))) {
                                    r.push(i);
                                    break
                                } return this.pushStack(r.length > 1 ? b.uniqueSort(r) : r)
                },
                index: function(e) {
                    return e ? "string" == typeof e ? l.call(b(e), this[0]) : l.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
                },
                add: function(e, t) {
                    return this.pushStack(b.uniqueSort(b.merge(this.get(), b(e, t))))
                },
                addBack: function(e) {
                    return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
                }
            }), b.each({
                parent: function(e) {
                    var t = e.parentNode;
                    return t && 11 !== t.nodeType ? t : null
                },
                parents: function(e) {
                    return E(e, "parentNode")
                },
                parentsUntil: function(e, t, i) {
                    return E(e, "parentNode", i)
                },
                next: function(e) {
                    return _(e, "nextSibling")
                },
                prev: function(e) {
                    return _(e, "previousSibling")
                },
                nextAll: function(e) {
                    return E(e, "nextSibling")
                },
                prevAll: function(e) {
                    return E(e, "previousSibling")
                },
                nextUntil: function(e, t, i) {
                    return E(e, "nextSibling", i)
                },
                prevUntil: function(e, t, i) {
                    return E(e, "previousSibling", i)
                },
                siblings: function(e) {
                    return P((e.parentNode || {}).firstChild, e)
                },
                children: function(e) {
                    return P(e.firstChild)
                },
                contents: function(e) {
                    return A(e, "iframe") ? e.contentDocument : (A(e, "template") && (e = e.content || e), b.merge([], e.childNodes))
                }
            }, function(e, t) {
                b.fn[e] = function(i, n) {
                    var o = b.map(this, t, i);
                    return "Until" !== e.slice(-5) && (n = i), n && "string" == typeof n && (o = b.filter(n, o)), this.length > 1 && (k[e] || b.uniqueSort(o), I.test(e) && o.reverse()), this.pushStack(o)
                }
            });
            var D = /[^\x20\t\r\n\f]+/g;

            function O(e) {
                return e
            }

            function L(e) {
                throw e
            }

            function U(e, t, i, n) {
                var o;
                try {
                    e && m(o = e.promise) ? o.call(e).done(t).fail(i) : e && m(o = e.then) ? o.call(e, t, i) : t.apply(void 0, [e].slice(n))
                } catch (e) {
                    i.apply(void 0, [e])
                }
            }
            b.Callbacks = function(e) {
                var t, i;
                e = "string" == typeof e ? (t = e, i = {}, b.each(t.match(D) || [], function(e, t) {
                    i[t] = !0
                }), i) : b.extend({}, e);
                var n, o, r, a, s = [],
                    l = [],
                    c = -1,
                    u = function() {
                        for (a = a || e.once, r = n = !0; l.length; c = -1)
                            for (o = l.shift(); ++c < s.length;) !1 === s[c].apply(o[0], o[1]) && e.stopOnFalse && (c = s.length, o = !1);
                        e.memory || (o = !1), n = !1, a && (s = o ? [] : "")
                    },
                    d = {
                        add: function() {
                            return s && (o && !n && (c = s.length - 1, l.push(o)), function t(i) {
                                b.each(i, function(i, n) {
                                    m(n) ? e.unique && d.has(n) || s.push(n) : n && n.length && "string" !== C(n) && t(n)
                                })
                            }(arguments), o && !n && u()), this
                        },
                        remove: function() {
                            return b.each(arguments, function(e, t) {
                                for (var i;
                                    (i = b.inArray(t, s, i)) > -1;) s.splice(i, 1), i <= c && c--
                            }), this
                        },
                        has: function(e) {
                            return e ? b.inArray(e, s) > -1 : s.length > 0
                        },
                        empty: function() {
                            return s && (s = []), this
                        },
                        disable: function() {
                            return a = l = [], s = o = "", this
                        },
                        disabled: function() {
                            return !s
                        },
                        lock: function() {
                            return a = l = [], o || n || (s = o = ""), this
                        },
                        locked: function() {
                            return !!a
                        },
                        fireWith: function(e, t) {
                            return a || (t = [e, (t = t || []).slice ? t.slice() : t], l.push(t), n || u()), this
                        },
                        fire: function() {
                            return d.fireWith(this, arguments), this
                        },
                        fired: function() {
                            return !!r
                        }
                    };
                return d
            }, b.extend({
                Deferred: function(t) {
                    var i = [
                            ["notify", "progress", b.Callbacks("memory"), b.Callbacks("memory"), 2],
                            ["resolve", "done", b.Callbacks("once memory"), b.Callbacks("once memory"), 0, "resolved"],
                            ["reject", "fail", b.Callbacks("once memory"), b.Callbacks("once memory"), 1, "rejected"]
                        ],
                        n = "pending",
                        o = {
                            state: function() {
                                return n
                            },
                            always: function() {
                                return r.done(arguments).fail(arguments), this
                            },
                            catch: function(e) {
                                return o.then(null, e)
                            },
                            pipe: function() {
                                var e = arguments;
                                return b.Deferred(function(t) {
                                    b.each(i, function(i, n) {
                                        var o = m(e[n[4]]) && e[n[4]];
                                        r[n[1]](function() {
                                            var e = o && o.apply(this, arguments);
                                            e && m(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[n[0] + "With"](this, o ? [e] : arguments)
                                        })
                                    }), e = null
                                }).promise()
                            },
                            then: function(t, n, o) {
                                var r = 0;

                                function a(t, i, n, o) {
                                    return function() {
                                        var s = this,
                                            l = arguments,
                                            c = function() {
                                                var e, c;
                                                if (!(t < r)) {
                                                    if ((e = n.apply(s, l)) === i.promise()) throw new TypeError("Thenable self-resolution");
                                                    c = e && ("object" == typeof e || "function" == typeof e) && e.then, m(c) ? o ? c.call(e, a(r, i, O, o), a(r, i, L, o)) : (r++, c.call(e, a(r, i, O, o), a(r, i, L, o), a(r, i, O, i.notifyWith))) : (n !== O && (s = void 0, l = [e]), (o || i.resolveWith)(s, l))
                                                }
                                            },
                                            u = o ? c : function() {
                                                try {
                                                    c()
                                                } catch (e) {
                                                    b.Deferred.exceptionHook && b.Deferred.exceptionHook(e, u.stackTrace), t + 1 >= r && (n !== L && (s = void 0, l = [e]), i.rejectWith(s, l))
                                                }
                                            };
                                        t ? u() : (b.Deferred.getStackHook && (u.stackTrace = b.Deferred.getStackHook()), e.setTimeout(u))
                                    }
                                }
                                return b.Deferred(function(e) {
                                    i[0][3].add(a(0, e, m(o) ? o : O, e.notifyWith)), i[1][3].add(a(0, e, m(t) ? t : O)), i[2][3].add(a(0, e, m(n) ? n : L))
                                }).promise()
                            },
                            promise: function(e) {
                                return null != e ? b.extend(e, o) : o
                            }
                        },
                        r = {};
                    return b.each(i, function(e, t) {
                        var a = t[2],
                            s = t[5];
                        o[t[1]] = a.add, s && a.add(function() {
                            n = s
                        }, i[3 - e][2].disable, i[3 - e][3].disable, i[0][2].lock, i[0][3].lock), a.add(t[3].fire), r[t[0]] = function() {
                            return r[t[0] + "With"](this === r ? void 0 : this, arguments), this
                        }, r[t[0] + "With"] = a.fireWith
                    }), o.promise(r), t && t.call(r, r), r
                },
                when: function(e) {
                    var t = arguments.length,
                        i = t,
                        n = Array(i),
                        o = r.call(arguments),
                        a = b.Deferred(),
                        s = function(e) {
                            return function(i) {
                                n[e] = this, o[e] = arguments.length > 1 ? r.call(arguments) : i, --t || a.resolveWith(n, o)
                            }
                        };
                    if (t <= 1 && (U(e, a.done(s(i)).resolve, a.reject, !t), "pending" === a.state() || m(o[i] && o[i].then))) return a.then();
                    for (; i--;) U(o[i], s(i), a.reject);
                    return a.promise()
                }
            });
            var F = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
            b.Deferred.exceptionHook = function(t, i) {
                e.console && e.console.warn && t && F.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, i)
            }, b.readyException = function(t) {
                e.setTimeout(function() {
                    throw t
                })
            };
            var j = b.Deferred();

            function R() {
                n.removeEventListener("DOMContentLoaded", R), e.removeEventListener("load", R), b.ready()
            }
            b.fn.ready = function(e) {
                return j.then(e).catch(function(e) {
                    b.readyException(e)
                }), this
            }, b.extend({
                isReady: !1,
                readyWait: 1,
                ready: function(e) {
                    (!0 === e ? --b.readyWait : b.isReady) || (b.isReady = !0, !0 !== e && --b.readyWait > 0 || j.resolveWith(n, [b]))
                }
            }), b.ready.then = j.then, "complete" === n.readyState || "loading" !== n.readyState && !n.documentElement.doScroll ? e.setTimeout(b.ready) : (n.addEventListener("DOMContentLoaded", R), e.addEventListener("load", R));
            var q = function(e, t, i, n, o, r, a) {
                    var s = 0,
                        l = e.length,
                        c = null == i;
                    if ("object" === C(i)) {
                        o = !0;
                        for (s in i) q(e, t, s, i[s], !0, r, a)
                    } else if (void 0 !== n && (o = !0, m(n) || (a = !0), c && (a ? (t.call(e, n), t = null) : (c = t, t = function(e, t, i) {
                            return c.call(b(e), i)
                        })), t))
                        for (; s < l; s++) t(e[s], i, a ? n : n.call(e[s], s, t(e[s], i)));
                    return o ? e : c ? t.call(e) : l ? t(e[0], i) : r
                },
                W = /^-ms-/,
                G = /-([a-z])/g;

            function V(e, t) {
                return t.toUpperCase()
            }

            function z(e) {
                return e.replace(W, "ms-").replace(G, V)
            }
            var X = function(e) {
                return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
            };

            function $() {
                this.expando = b.expando + $.uid++
            }
            $.uid = 1, $.prototype = {
                cache: function(e) {
                    var t = e[this.expando];
                    return t || (t = {}, X(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                        value: t,
                        configurable: !0
                    }))), t
                },
                set: function(e, t, i) {
                    var n, o = this.cache(e);
                    if ("string" == typeof t) o[z(t)] = i;
                    else
                        for (n in t) o[z(n)] = t[n];
                    return o
                },
                get: function(e, t) {
                    return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][z(t)]
                },
                access: function(e, t, i) {
                    return void 0 === t || t && "string" == typeof t && void 0 === i ? this.get(e, t) : (this.set(e, t, i), void 0 !== i ? i : t)
                },
                remove: function(e, t) {
                    var i, n = e[this.expando];
                    if (void 0 !== n) {
                        if (void 0 !== t) {
                            i = (t = Array.isArray(t) ? t.map(z) : (t = z(t)) in n ? [t] : t.match(D) || []).length;
                            for (; i--;) delete n[t[i]]
                        }(void 0 === t || b.isEmptyObject(n)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
                    }
                },
                hasData: function(e) {
                    var t = e[this.expando];
                    return void 0 !== t && !b.isEmptyObject(t)
                }
            };
            var Y = new $,
                J = new $,
                K = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
                Q = /[A-Z]/g;

            function Z(e, t, i) {
                var n, o;
                if (void 0 === i && 1 === e.nodeType)
                    if (n = "data-" + t.replace(Q, "-$&").toLowerCase(), "string" == typeof(i = e.getAttribute(n))) {
                        try {
                            i = "true" === (o = i) || "false" !== o && ("null" === o ? null : o === +o + "" ? +o : K.test(o) ? JSON.parse(o) : o)
                        } catch (e) {}
                        J.set(e, t, i)
                    } else i = void 0;
                return i
            }
            b.extend({
                hasData: function(e) {
                    return J.hasData(e) || Y.hasData(e)
                },
                data: function(e, t, i) {
                    return J.access(e, t, i)
                },
                removeData: function(e, t) {
                    J.remove(e, t)
                },
                _data: function(e, t, i) {
                    return Y.access(e, t, i)
                },
                _removeData: function(e, t) {
                    Y.remove(e, t)
                }
            }), b.fn.extend({
                data: function(e, t) {
                    var i, n, o, r = this[0],
                        a = r && r.attributes;
                    if (void 0 === e) {
                        if (this.length && (o = J.get(r), 1 === r.nodeType && !Y.get(r, "hasDataAttrs"))) {
                            for (i = a.length; i--;) a[i] && 0 === (n = a[i].name).indexOf("data-") && (n = z(n.slice(5)), Z(r, n, o[n]));
                            Y.set(r, "hasDataAttrs", !0)
                        }
                        return o
                    }
                    return "object" == typeof e ? this.each(function() {
                        J.set(this, e)
                    }) : q(this, function(t) {
                        var i;
                        if (r && void 0 === t) return void 0 !== (i = J.get(r, e)) ? i : void 0 !== (i = Z(r, e)) ? i : void 0;
                        this.each(function() {
                            J.set(this, e, t)
                        })
                    }, null, t, arguments.length > 1, null, !0)
                },
                removeData: function(e) {
                    return this.each(function() {
                        J.remove(this, e)
                    })
                }
            }), b.extend({
                queue: function(e, t, i) {
                    var n;
                    if (e) return t = (t || "fx") + "queue", n = Y.get(e, t), i && (!n || Array.isArray(i) ? n = Y.access(e, t, b.makeArray(i)) : n.push(i)), n || []
                },
                dequeue: function(e, t) {
                    t = t || "fx";
                    var i = b.queue(e, t),
                        n = i.length,
                        o = i.shift(),
                        r = b._queueHooks(e, t);
                    "inprogress" === o && (o = i.shift(), n--), o && ("fx" === t && i.unshift("inprogress"), delete r.stop, o.call(e, function() {
                        b.dequeue(e, t)
                    }, r)), !n && r && r.empty.fire()
                },
                _queueHooks: function(e, t) {
                    var i = t + "queueHooks";
                    return Y.get(e, i) || Y.access(e, i, {
                        empty: b.Callbacks("once memory").add(function() {
                            Y.remove(e, [t + "queue", i])
                        })
                    })
                }
            }), b.fn.extend({
                queue: function(e, t) {
                    var i = 2;
                    return "string" != typeof e && (t = e, e = "fx", i--), arguments.length < i ? b.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                        var i = b.queue(this, e, t);
                        b._queueHooks(this, e), "fx" === e && "inprogress" !== i[0] && b.dequeue(this, e)
                    })
                },
                dequeue: function(e) {
                    return this.each(function() {
                        b.dequeue(this, e)
                    })
                },
                clearQueue: function(e) {
                    return this.queue(e || "fx", [])
                },
                promise: function(e, t) {
                    var i, n = 1,
                        o = b.Deferred(),
                        r = this,
                        a = this.length,
                        s = function() {
                            --n || o.resolveWith(r, [r])
                        };
                    for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;)(i = Y.get(r[a], e + "queueHooks")) && i.empty && (n++, i.empty.add(s));
                    return s(), o.promise(t)
                }
            });
            var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
                te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
                ie = ["Top", "Right", "Bottom", "Left"],
                ne = function(e, t) {
                    return "none" === (e = t || e).style.display || "" === e.style.display && b.contains(e.ownerDocument, e) && "none" === b.css(e, "display")
                },
                oe = function(e, t, i, n) {
                    var o, r, a = {};
                    for (r in t) a[r] = e.style[r], e.style[r] = t[r];
                    o = i.apply(e, n || []);
                    for (r in t) e.style[r] = a[r];
                    return o
                };

            function re(e, t, i, n) {
                var o, r, a = 20,
                    s = n ? function() {
                        return n.cur()
                    } : function() {
                        return b.css(e, t, "")
                    },
                    l = s(),
                    c = i && i[3] || (b.cssNumber[t] ? "" : "px"),
                    u = (b.cssNumber[t] || "px" !== c && +l) && te.exec(b.css(e, t));
                if (u && u[3] !== c) {
                    for (l /= 2, c = c || u[3], u = +l || 1; a--;) b.style(e, t, u + c), (1 - r) * (1 - (r = s() / l || .5)) <= 0 && (a = 0), u /= r;
                    u *= 2, b.style(e, t, u + c), i = i || []
                }
                return i && (u = +u || +l || 0, o = i[1] ? u + (i[1] + 1) * i[2] : +i[2], n && (n.unit = c, n.start = u, n.end = o)), o
            }
            var ae = {};

            function se(e, t) {
                for (var i, n, o, r, a, s, l, c = [], u = 0, d = e.length; u < d; u++)(n = e[u]).style && (i = n.style.display, t ? ("none" === i && (c[u] = Y.get(n, "display") || null, c[u] || (n.style.display = "")), "" === n.style.display && ne(n) && (c[u] = (r = void 0, a = void 0, void 0, l = void 0, a = (o = n).ownerDocument, s = o.nodeName, (l = ae[s]) || (r = a.body.appendChild(a.createElement(s)), l = b.css(r, "display"), r.parentNode.removeChild(r), "none" === l && (l = "block"), ae[s] = l, l)))) : "none" !== i && (c[u] = "none", Y.set(n, "display", i)));
                for (u = 0; u < d; u++) null != c[u] && (e[u].style.display = c[u]);
                return e
            }
            b.fn.extend({
                show: function() {
                    return se(this, !0)
                },
                hide: function() {
                    return se(this)
                },
                toggle: function(e) {
                    return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                        ne(this) ? b(this).show() : b(this).hide()
                    })
                }
            });
            var le = /^(?:checkbox|radio)$/i,
                ce = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
                ue = /^$|^module$|\/(?:java|ecma)script/i,
                de = {
                    option: [1, "<select multiple='multiple'>", "</select>"],
                    thead: [1, "<table>", "</table>"],
                    col: [2, "<table><colgroup>", "</colgroup></table>"],
                    tr: [2, "<table><tbody>", "</tbody></table>"],
                    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                    _default: [0, "", ""]
                };

            function pe(e, t) {
                var i;
                return i = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? b.merge([e], i) : i
            }

            function he(e, t) {
                for (var i = 0, n = e.length; i < n; i++) Y.set(e[i], "globalEval", !t || Y.get(t[i], "globalEval"))
            }
            de.optgroup = de.option, de.tbody = de.tfoot = de.colgroup = de.caption = de.thead, de.th = de.td;
            var fe, me, ve = /<|&#?\w+;/;

            function ge(e, t, i, n, o) {
                for (var r, a, s, l, c, u, d = t.createDocumentFragment(), p = [], h = 0, f = e.length; h < f; h++)
                    if ((r = e[h]) || 0 === r)
                        if ("object" === C(r)) b.merge(p, r.nodeType ? [r] : r);
                        else if (ve.test(r)) {
                    for (a = a || d.appendChild(t.createElement("div")), s = (ce.exec(r) || ["", ""])[1].toLowerCase(), l = de[s] || de._default, a.innerHTML = l[1] + b.htmlPrefilter(r) + l[2], u = l[0]; u--;) a = a.lastChild;
                    b.merge(p, a.childNodes), (a = d.firstChild).textContent = ""
                } else p.push(t.createTextNode(r));
                for (d.textContent = "", h = 0; r = p[h++];)
                    if (n && b.inArray(r, n) > -1) o && o.push(r);
                    else if (c = b.contains(r.ownerDocument, r), a = pe(d.appendChild(r), "script"), c && he(a), i)
                    for (u = 0; r = a[u++];) ue.test(r.type || "") && i.push(r);
                return d
            }
            fe = n.createDocumentFragment().appendChild(n.createElement("div")), (me = n.createElement("input")).setAttribute("type", "radio"), me.setAttribute("checked", "checked"), me.setAttribute("name", "t"), fe.appendChild(me), f.checkClone = fe.cloneNode(!0).cloneNode(!0).lastChild.checked, fe.innerHTML = "<textarea>x</textarea>", f.noCloneChecked = !!fe.cloneNode(!0).lastChild.defaultValue;
            var ye = n.documentElement,
                Ce = /^key/,
                be = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
                xe = /^([^.]*)(?:\.(.+)|)/;

            function we() {
                return !0
            }

            function Te() {
                return !1
            }

            function Ee() {
                try {
                    return n.activeElement
                } catch (e) {}
            }

            function Pe(e, t, i, n, o, r) {
                var a, s;
                if ("object" == typeof t) {
                    "string" != typeof i && (n = n || i, i = void 0);
                    for (s in t) Pe(e, s, i, n, t[s], r);
                    return e
                }
                if (null == n && null == o ? (o = i, n = i = void 0) : null == o && ("string" == typeof i ? (o = n, n = void 0) : (o = n, n = i, i = void 0)), !1 === o) o = Te;
                else if (!o) return e;
                return 1 === r && (a = o, (o = function(e) {
                    return b().off(e), a.apply(this, arguments)
                }).guid = a.guid || (a.guid = b.guid++)), e.each(function() {
                    b.event.add(this, t, o, n, i)
                })
            }
            b.event = {
                global: {},
                add: function(e, t, i, n, o) {
                    var r, a, s, l, c, u, d, p, h, f, m, v = Y.get(e);
                    if (v)
                        for (i.handler && (i = (r = i).handler, o = r.selector), o && b.find.matchesSelector(ye, o), i.guid || (i.guid = b.guid++), (l = v.events) || (l = v.events = {}), (a = v.handle) || (a = v.handle = function(t) {
                                return void 0 !== b && b.event.triggered !== t.type ? b.event.dispatch.apply(e, arguments) : void 0
                            }), c = (t = (t || "").match(D) || [""]).length; c--;) h = m = (s = xe.exec(t[c]) || [])[1], f = (s[2] || "").split(".").sort(), h && (d = b.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, d = b.event.special[h] || {}, u = b.extend({
                            type: h,
                            origType: m,
                            data: n,
                            handler: i,
                            guid: i.guid,
                            selector: o,
                            needsContext: o && b.expr.match.needsContext.test(o),
                            namespace: f.join(".")
                        }, r), (p = l[h]) || ((p = l[h] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(e, n, f, a) || e.addEventListener && e.addEventListener(h, a)), d.add && (d.add.call(e, u), u.handler.guid || (u.handler.guid = i.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), b.event.global[h] = !0)
                },
                remove: function(e, t, i, n, o) {
                    var r, a, s, l, c, u, d, p, h, f, m, v = Y.hasData(e) && Y.get(e);
                    if (v && (l = v.events)) {
                        for (c = (t = (t || "").match(D) || [""]).length; c--;)
                            if (h = m = (s = xe.exec(t[c]) || [])[1], f = (s[2] || "").split(".").sort(), h) {
                                for (d = b.event.special[h] || {}, p = l[h = (n ? d.delegateType : d.bindType) || h] || [], s = s[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = r = p.length; r--;) u = p[r], !o && m !== u.origType || i && i.guid !== u.guid || s && !s.test(u.namespace) || n && n !== u.selector && ("**" !== n || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
                                a && !p.length && (d.teardown && !1 !== d.teardown.call(e, f, v.handle) || b.removeEvent(e, h, v.handle), delete l[h])
                            } else
                                for (h in l) b.event.remove(e, h + t[c], i, n, !0);
                        b.isEmptyObject(l) && Y.remove(e, "handle events")
                    }
                },
                dispatch: function(e) {
                    var t, i, n, o, r, a, s = b.event.fix(e),
                        l = new Array(arguments.length),
                        c = (Y.get(this, "events") || {})[s.type] || [],
                        u = b.event.special[s.type] || {};
                    for (l[0] = s, t = 1; t < arguments.length; t++) l[t] = arguments[t];
                    if (s.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, s)) {
                        for (a = b.event.handlers.call(this, s, c), t = 0;
                            (o = a[t++]) && !s.isPropagationStopped();)
                            for (s.currentTarget = o.elem, i = 0;
                                (r = o.handlers[i++]) && !s.isImmediatePropagationStopped();) s.rnamespace && !s.rnamespace.test(r.namespace) || (s.handleObj = r, s.data = r.data, void 0 !== (n = ((b.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (s.result = n) && (s.preventDefault(), s.stopPropagation()));
                        return u.postDispatch && u.postDispatch.call(this, s), s.result
                    }
                },
                handlers: function(e, t) {
                    var i, n, o, r, a, s = [],
                        l = t.delegateCount,
                        c = e.target;
                    if (l && c.nodeType && !("click" === e.type && e.button >= 1))
                        for (; c !== this; c = c.parentNode || this)
                            if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                                for (r = [], a = {}, i = 0; i < l; i++) void 0 === a[o = (n = t[i]).selector + " "] && (a[o] = n.needsContext ? b(o, this).index(c) > -1 : b.find(o, this, null, [c]).length), a[o] && r.push(n);
                                r.length && s.push({
                                    elem: c,
                                    handlers: r
                                })
                            } return c = this, l < t.length && s.push({
                        elem: c,
                        handlers: t.slice(l)
                    }), s
                },
                addProp: function(e, t) {
                    Object.defineProperty(b.Event.prototype, e, {
                        enumerable: !0,
                        configurable: !0,
                        get: m(t) ? function() {
                            if (this.originalEvent) return t(this.originalEvent)
                        } : function() {
                            if (this.originalEvent) return this.originalEvent[e]
                        },
                        set: function(t) {
                            Object.defineProperty(this, e, {
                                enumerable: !0,
                                configurable: !0,
                                writable: !0,
                                value: t
                            })
                        }
                    })
                },
                fix: function(e) {
                    return e[b.expando] ? e : new b.Event(e)
                },
                special: {
                    load: {
                        noBubble: !0
                    },
                    focus: {
                        trigger: function() {
                            if (this !== Ee() && this.focus) return this.focus(), !1
                        },
                        delegateType: "focusin"
                    },
                    blur: {
                        trigger: function() {
                            if (this === Ee() && this.blur) return this.blur(), !1
                        },
                        delegateType: "focusout"
                    },
                    click: {
                        trigger: function() {
                            if ("checkbox" === this.type && this.click && A(this, "input")) return this.click(), !1
                        },
                        _default: function(e) {
                            return A(e.target, "a")
                        }
                    },
                    beforeunload: {
                        postDispatch: function(e) {
                            void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                        }
                    }
                }
            }, b.removeEvent = function(e, t, i) {
                e.removeEventListener && e.removeEventListener(t, i)
            }, b.Event = function(e, t) {
                if (!(this instanceof b.Event)) return new b.Event(e, t);
                e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? we : Te, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && b.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[b.expando] = !0
            }, b.Event.prototype = {
                constructor: b.Event,
                isDefaultPrevented: Te,
                isPropagationStopped: Te,
                isImmediatePropagationStopped: Te,
                isSimulated: !1,
                preventDefault: function() {
                    var e = this.originalEvent;
                    this.isDefaultPrevented = we, e && !this.isSimulated && e.preventDefault()
                },
                stopPropagation: function() {
                    var e = this.originalEvent;
                    this.isPropagationStopped = we, e && !this.isSimulated && e.stopPropagation()
                },
                stopImmediatePropagation: function() {
                    var e = this.originalEvent;
                    this.isImmediatePropagationStopped = we, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
                }
            }, b.each({
                altKey: !0,
                bubbles: !0,
                cancelable: !0,
                changedTouches: !0,
                ctrlKey: !0,
                detail: !0,
                eventPhase: !0,
                metaKey: !0,
                pageX: !0,
                pageY: !0,
                shiftKey: !0,
                view: !0,
                char: !0,
                charCode: !0,
                key: !0,
                keyCode: !0,
                button: !0,
                buttons: !0,
                clientX: !0,
                clientY: !0,
                offsetX: !0,
                offsetY: !0,
                pointerId: !0,
                pointerType: !0,
                screenX: !0,
                screenY: !0,
                targetTouches: !0,
                toElement: !0,
                touches: !0,
                which: function(e) {
                    var t = e.button;
                    return null == e.which && Ce.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && be.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
                }
            }, b.event.addProp), b.each({
                mouseenter: "mouseover",
                mouseleave: "mouseout",
                pointerenter: "pointerover",
                pointerleave: "pointerout"
            }, function(e, t) {
                b.event.special[e] = {
                    delegateType: t,
                    bindType: t,
                    handle: function(e) {
                        var i, n = e.relatedTarget,
                            o = e.handleObj;
                        return n && (n === this || b.contains(this, n)) || (e.type = o.origType, i = o.handler.apply(this, arguments), e.type = t), i
                    }
                }
            }), b.fn.extend({
                on: function(e, t, i, n) {
                    return Pe(this, e, t, i, n)
                },
                one: function(e, t, i, n) {
                    return Pe(this, e, t, i, n, 1)
                },
                off: function(e, t, i) {
                    var n, o;
                    if (e && e.preventDefault && e.handleObj) return n = e.handleObj, b(e.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
                    if ("object" == typeof e) {
                        for (o in e) this.off(o, t, e[o]);
                        return this
                    }
                    return !1 !== t && "function" != typeof t || (i = t, t = void 0), !1 === i && (i = Te), this.each(function() {
                        b.event.remove(this, e, i, t)
                    })
                }
            });
            var Ne = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
                Ae = /<script|<style|<link/i,
                Se = /checked\s*(?:[^=]|=\s*.checked.)/i,
                Me = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

            function Be(e, t) {
                return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && b(e).children("tbody")[0] || e
            }

            function He(e) {
                return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
            }

            function Ie(e) {
                return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
            }

            function ke(e, t) {
                var i, n, o, r, a, s, l, c;
                if (1 === t.nodeType) {
                    if (Y.hasData(e) && (r = Y.access(e), a = Y.set(t, r), c = r.events)) {
                        delete a.handle, a.events = {};
                        for (o in c)
                            for (i = 0, n = c[o].length; i < n; i++) b.event.add(t, o, c[o][i])
                    }
                    J.hasData(e) && (s = J.access(e), l = b.extend({}, s), J.set(t, l))
                }
            }

            function _e(e, t, i, n) {
                t = a.apply([], t);
                var o, r, s, l, c, u, d = 0,
                    p = e.length,
                    h = p - 1,
                    v = t[0],
                    g = m(v);
                if (g || p > 1 && "string" == typeof v && !f.checkClone && Se.test(v)) return e.each(function(o) {
                    var r = e.eq(o);
                    g && (t[0] = v.call(this, o, r.html())), _e(r, t, i, n)
                });
                if (p && (r = (o = ge(t, e[0].ownerDocument, !1, e, n)).firstChild, 1 === o.childNodes.length && (o = r), r || n)) {
                    for (l = (s = b.map(pe(o, "script"), He)).length; d < p; d++) c = o, d !== h && (c = b.clone(c, !0, !0), l && b.merge(s, pe(c, "script"))), i.call(e[d], c, d);
                    if (l)
                        for (u = s[s.length - 1].ownerDocument, b.map(s, Ie), d = 0; d < l; d++) c = s[d], ue.test(c.type || "") && !Y.access(c, "globalEval") && b.contains(u, c) && (c.src && "module" !== (c.type || "").toLowerCase() ? b._evalUrl && b._evalUrl(c.src) : y(c.textContent.replace(Me, ""), u, c))
                }
                return e
            }

            function De(e, t, i) {
                for (var n, o = t ? b.filter(t, e) : e, r = 0; null != (n = o[r]); r++) i || 1 !== n.nodeType || b.cleanData(pe(n)), n.parentNode && (i && b.contains(n.ownerDocument, n) && he(pe(n, "script")), n.parentNode.removeChild(n));
                return e
            }
            b.extend({
                htmlPrefilter: function(e) {
                    return e.replace(Ne, "<$1></$2>")
                },
                clone: function(e, t, i) {
                    var n, o, r, a, s, l, c, u = e.cloneNode(!0),
                        d = b.contains(e.ownerDocument, e);
                    if (!(f.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || b.isXMLDoc(e)))
                        for (a = pe(u), n = 0, o = (r = pe(e)).length; n < o; n++) s = r[n], l = a[n], void 0, "input" === (c = l.nodeName.toLowerCase()) && le.test(s.type) ? l.checked = s.checked : "input" !== c && "textarea" !== c || (l.defaultValue = s.defaultValue);
                    if (t)
                        if (i)
                            for (r = r || pe(e), a = a || pe(u), n = 0, o = r.length; n < o; n++) ke(r[n], a[n]);
                        else ke(e, u);
                    return (a = pe(u, "script")).length > 0 && he(a, !d && pe(e, "script")), u
                },
                cleanData: function(e) {
                    for (var t, i, n, o = b.event.special, r = 0; void 0 !== (i = e[r]); r++)
                        if (X(i)) {
                            if (t = i[Y.expando]) {
                                if (t.events)
                                    for (n in t.events) o[n] ? b.event.remove(i, n) : b.removeEvent(i, n, t.handle);
                                i[Y.expando] = void 0
                            }
                            i[J.expando] && (i[J.expando] = void 0)
                        }
                }
            }), b.fn.extend({
                detach: function(e) {
                    return De(this, e, !0)
                },
                remove: function(e) {
                    return De(this, e)
                },
                text: function(e) {
                    return q(this, function(e) {
                        return void 0 === e ? b.text(this) : this.empty().each(function() {
                            1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                        })
                    }, null, e, arguments.length)
                },
                append: function() {
                    return _e(this, arguments, function(e) {
                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Be(this, e).appendChild(e)
                    })
                },
                prepend: function() {
                    return _e(this, arguments, function(e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            var t = Be(this, e);
                            t.insertBefore(e, t.firstChild)
                        }
                    })
                },
                before: function() {
                    return _e(this, arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this)
                    })
                },
                after: function() {
                    return _e(this, arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                    })
                },
                empty: function() {
                    for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (b.cleanData(pe(e, !1)), e.textContent = "");
                    return this
                },
                clone: function(e, t) {
                    return e = null != e && e, t = null == t ? e : t, this.map(function() {
                        return b.clone(this, e, t)
                    })
                },
                html: function(e) {
                    return q(this, function(e) {
                        var t = this[0] || {},
                            i = 0,
                            n = this.length;
                        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                        if ("string" == typeof e && !Ae.test(e) && !de[(ce.exec(e) || ["", ""])[1].toLowerCase()]) {
                            e = b.htmlPrefilter(e);
                            try {
                                for (; i < n; i++) 1 === (t = this[i] || {}).nodeType && (b.cleanData(pe(t, !1)), t.innerHTML = e);
                                t = 0
                            } catch (e) {}
                        }
                        t && this.empty().append(e)
                    }, null, e, arguments.length)
                },
                replaceWith: function() {
                    var e = [];
                    return _e(this, arguments, function(t) {
                        var i = this.parentNode;
                        b.inArray(this, e) < 0 && (b.cleanData(pe(this)), i && i.replaceChild(t, this))
                    }, e)
                }
            }), b.each({
                appendTo: "append",
                prependTo: "prepend",
                insertBefore: "before",
                insertAfter: "after",
                replaceAll: "replaceWith"
            }, function(e, t) {
                b.fn[e] = function(e) {
                    for (var i, n = [], o = b(e), r = o.length - 1, a = 0; a <= r; a++) i = a === r ? this : this.clone(!0), b(o[a])[t](i), s.apply(n, i.get());
                    return this.pushStack(n)
                }
            });
            var Oe = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
                Le = function(t) {
                    var i = t.ownerDocument.defaultView;
                    return i && i.opener || (i = e), i.getComputedStyle(t)
                },
                Ue = new RegExp(ie.join("|"), "i");

            function Fe(e, t, i) {
                var n, o, r, a, s = e.style;
                return (i = i || Le(e)) && ("" !== (a = i.getPropertyValue(t) || i[t]) || b.contains(e.ownerDocument, e) || (a = b.style(e, t)), !f.pixelBoxStyles() && Oe.test(a) && Ue.test(t) && (n = s.width, o = s.minWidth, r = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = i.width, s.width = n, s.minWidth = o, s.maxWidth = r)), void 0 !== a ? a + "" : a
            }

            function je(e, t) {
                return {
                    get: function() {
                        if (!e()) return (this.get = t).apply(this, arguments);
                        delete this.get
                    }
                }
            }! function() {
                function t() {
                    if (u) {
                        c.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", ye.appendChild(c).appendChild(u);
                        var t = e.getComputedStyle(u);
                        o = "1%" !== t.top, l = 12 === i(t.marginLeft), u.style.right = "60%", s = 36 === i(t.right), r = 36 === i(t.width), u.style.position = "absolute", a = 36 === u.offsetWidth || "absolute", ye.removeChild(c), u = null
                    }
                }

                function i(e) {
                    return Math.round(parseFloat(e))
                }
                var o, r, a, s, l, c = n.createElement("div"),
                    u = n.createElement("div");
                u.style && (u.style.backgroundClip = "content-box", u.cloneNode(!0).style.backgroundClip = "", f.clearCloneStyle = "content-box" === u.style.backgroundClip, b.extend(f, {
                    boxSizingReliable: function() {
                        return t(), r
                    },
                    pixelBoxStyles: function() {
                        return t(), s
                    },
                    pixelPosition: function() {
                        return t(), o
                    },
                    reliableMarginLeft: function() {
                        return t(), l
                    },
                    scrollboxSize: function() {
                        return t(), a
                    }
                }))
            }();
            var Re = /^(none|table(?!-c[ea]).+)/,
                qe = /^--/,
                We = {
                    position: "absolute",
                    visibility: "hidden",
                    display: "block"
                },
                Ge = {
                    letterSpacing: "0",
                    fontWeight: "400"
                },
                Ve = ["Webkit", "Moz", "ms"],
                ze = n.createElement("div").style;

            function Xe(e) {
                var t = b.cssProps[e];
                return t || (t = b.cssProps[e] = function(e) {
                    if (e in ze) return e;
                    for (var t = e[0].toUpperCase() + e.slice(1), i = Ve.length; i--;)
                        if ((e = Ve[i] + t) in ze) return e
                }(e) || e), t
            }

            function $e(e, t, i) {
                var n = te.exec(t);
                return n ? Math.max(0, n[2] - (i || 0)) + (n[3] || "px") : t
            }

            function Ye(e, t, i, n, o, r) {
                var a = "width" === t ? 1 : 0,
                    s = 0,
                    l = 0;
                if (i === (n ? "border" : "content")) return 0;
                for (; a < 4; a += 2) "margin" === i && (l += b.css(e, i + ie[a], !0, o)), n ? ("content" === i && (l -= b.css(e, "padding" + ie[a], !0, o)), "margin" !== i && (l -= b.css(e, "border" + ie[a] + "Width", !0, o))) : (l += b.css(e, "padding" + ie[a], !0, o), "padding" !== i ? l += b.css(e, "border" + ie[a] + "Width", !0, o) : s += b.css(e, "border" + ie[a] + "Width", !0, o));
                return !n && r >= 0 && (l += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - r - l - s - .5))), l
            }

            function Je(e, t, i) {
                var n = Le(e),
                    o = Fe(e, t, n),
                    r = "border-box" === b.css(e, "boxSizing", !1, n),
                    a = r;
                if (Oe.test(o)) {
                    if (!i) return o;
                    o = "auto"
                }
                return a = a && (f.boxSizingReliable() || o === e.style[t]), ("auto" === o || !parseFloat(o) && "inline" === b.css(e, "display", !1, n)) && (o = e["offset" + t[0].toUpperCase() + t.slice(1)], a = !0), (o = parseFloat(o) || 0) + Ye(e, t, i || (r ? "border" : "content"), a, n, o) + "px"
            }

            function Ke(e, t, i, n, o) {
                return new Ke.prototype.init(e, t, i, n, o)
            }
            b.extend({
                cssHooks: {
                    opacity: {
                        get: function(e, t) {
                            if (t) {
                                var i = Fe(e, "opacity");
                                return "" === i ? "1" : i
                            }
                        }
                    }
                },
                cssNumber: {
                    animationIterationCount: !0,
                    columnCount: !0,
                    fillOpacity: !0,
                    flexGrow: !0,
                    flexShrink: !0,
                    fontWeight: !0,
                    lineHeight: !0,
                    opacity: !0,
                    order: !0,
                    orphans: !0,
                    widows: !0,
                    zIndex: !0,
                    zoom: !0
                },
                cssProps: {},
                style: function(e, t, i, n) {
                    if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                        var o, r, a, s = z(t),
                            l = qe.test(t),
                            c = e.style;
                        if (l || (t = Xe(s)), a = b.cssHooks[t] || b.cssHooks[s], void 0 === i) return a && "get" in a && void 0 !== (o = a.get(e, !1, n)) ? o : c[t];
                        "string" === (r = typeof i) && (o = te.exec(i)) && o[1] && (i = re(e, t, o), r = "number"), null != i && i == i && ("number" === r && (i += o && o[3] || (b.cssNumber[s] ? "" : "px")), f.clearCloneStyle || "" !== i || 0 !== t.indexOf("background") || (c[t] = "inherit"), a && "set" in a && void 0 === (i = a.set(e, i, n)) || (l ? c.setProperty(t, i) : c[t] = i))
                    }
                },
                css: function(e, t, i, n) {
                    var o, r, a, s = z(t);
                    return qe.test(t) || (t = Xe(s)), (a = b.cssHooks[t] || b.cssHooks[s]) && "get" in a && (o = a.get(e, !0, i)), void 0 === o && (o = Fe(e, t, n)), "normal" === o && t in Ge && (o = Ge[t]), "" === i || i ? (r = parseFloat(o), !0 === i || isFinite(r) ? r || 0 : o) : o
                }
            }), b.each(["height", "width"], function(e, t) {
                b.cssHooks[t] = {
                    get: function(e, i, n) {
                        if (i) return !Re.test(b.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Je(e, t, n) : oe(e, We, function() {
                            return Je(e, t, n)
                        })
                    },
                    set: function(e, i, n) {
                        var o, r = Le(e),
                            a = "border-box" === b.css(e, "boxSizing", !1, r),
                            s = n && Ye(e, t, n, a, r);
                        return a && f.scrollboxSize() === r.position && (s -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(r[t]) - Ye(e, t, "border", !1, r) - .5)), s && (o = te.exec(i)) && "px" !== (o[3] || "px") && (e.style[t] = i, i = b.css(e, t)), $e(0, i, s)
                    }
                }
            }), b.cssHooks.marginLeft = je(f.reliableMarginLeft, function(e, t) {
                if (t) return (parseFloat(Fe(e, "marginLeft")) || e.getBoundingClientRect().left - oe(e, {
                    marginLeft: 0
                }, function() {
                    return e.getBoundingClientRect().left
                })) + "px"
            }), b.each({
                margin: "",
                padding: "",
                border: "Width"
            }, function(e, t) {
                b.cssHooks[e + t] = {
                    expand: function(i) {
                        for (var n = 0, o = {}, r = "string" == typeof i ? i.split(" ") : [i]; n < 4; n++) o[e + ie[n] + t] = r[n] || r[n - 2] || r[0];
                        return o
                    }
                }, "margin" !== e && (b.cssHooks[e + t].set = $e)
            }), b.fn.extend({
                css: function(e, t) {
                    return q(this, function(e, t, i) {
                        var n, o, r = {},
                            a = 0;
                        if (Array.isArray(t)) {
                            for (n = Le(e), o = t.length; a < o; a++) r[t[a]] = b.css(e, t[a], !1, n);
                            return r
                        }
                        return void 0 !== i ? b.style(e, t, i) : b.css(e, t)
                    }, e, t, arguments.length > 1)
                }
            }), b.Tween = Ke, Ke.prototype = {
                constructor: Ke,
                init: function(e, t, i, n, o, r) {
                    this.elem = e, this.prop = i, this.easing = o || b.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = n, this.unit = r || (b.cssNumber[i] ? "" : "px")
                },
                cur: function() {
                    var e = Ke.propHooks[this.prop];
                    return e && e.get ? e.get(this) : Ke.propHooks._default.get(this)
                },
                run: function(e) {
                    var t, i = Ke.propHooks[this.prop];
                    return this.options.duration ? this.pos = t = b.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : Ke.propHooks._default.set(this), this
                }
            }, Ke.prototype.init.prototype = Ke.prototype, Ke.propHooks = {
                _default: {
                    get: function(e) {
                        var t;
                        return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = b.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
                    },
                    set: function(e) {
                        b.fx.step[e.prop] ? b.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[b.cssProps[e.prop]] && !b.cssHooks[e.prop] ? e.elem[e.prop] = e.now : b.style(e.elem, e.prop, e.now + e.unit)
                    }
                }
            }, Ke.propHooks.scrollTop = Ke.propHooks.scrollLeft = {
                set: function(e) {
                    e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
                }
            }, b.easing = {
                linear: function(e) {
                    return e
                },
                swing: function(e) {
                    return .5 - Math.cos(e * Math.PI) / 2
                },
                _default: "swing"
            }, b.fx = Ke.prototype.init, b.fx.step = {};
            var Qe, Ze, et, tt, it = /^(?:toggle|show|hide)$/,
                nt = /queueHooks$/;

            function ot() {
                Ze && (!1 === n.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(ot) : e.setTimeout(ot, b.fx.interval), b.fx.tick())
            }

            function rt() {
                return e.setTimeout(function() {
                    Qe = void 0
                }), Qe = Date.now()
            }

            function at(e, t) {
                var i, n = 0,
                    o = {
                        height: e
                    };
                for (t = t ? 1 : 0; n < 4; n += 2 - t) o["margin" + (i = ie[n])] = o["padding" + i] = e;
                return t && (o.opacity = o.width = e), o
            }

            function st(e, t, i) {
                for (var n, o = (lt.tweeners[t] || []).concat(lt.tweeners["*"]), r = 0, a = o.length; r < a; r++)
                    if (n = o[r].call(i, t, e)) return n
            }

            function lt(e, t, i) {
                var n, o, r = 0,
                    a = lt.prefilters.length,
                    s = b.Deferred().always(function() {
                        delete l.elem
                    }),
                    l = function() {
                        if (o) return !1;
                        for (var t = Qe || rt(), i = Math.max(0, c.startTime + c.duration - t), n = 1 - (i / c.duration || 0), r = 0, a = c.tweens.length; r < a; r++) c.tweens[r].run(n);
                        return s.notifyWith(e, [c, n, i]), n < 1 && a ? i : (a || s.notifyWith(e, [c, 1, 0]), s.resolveWith(e, [c]), !1)
                    },
                    c = s.promise({
                        elem: e,
                        props: b.extend({}, t),
                        opts: b.extend(!0, {
                            specialEasing: {},
                            easing: b.easing._default
                        }, i),
                        originalProperties: t,
                        originalOptions: i,
                        startTime: Qe || rt(),
                        duration: i.duration,
                        tweens: [],
                        createTween: function(t, i) {
                            var n = b.Tween(e, c.opts, t, i, c.opts.specialEasing[t] || c.opts.easing);
                            return c.tweens.push(n), n
                        },
                        stop: function(t) {
                            var i = 0,
                                n = t ? c.tweens.length : 0;
                            if (o) return this;
                            for (o = !0; i < n; i++) c.tweens[i].run(1);
                            return t ? (s.notifyWith(e, [c, 1, 0]), s.resolveWith(e, [c, t])) : s.rejectWith(e, [c, t]), this
                        }
                    }),
                    u = c.props;
                for (! function(e, t) {
                        var i, n, o, r, a;
                        for (i in e)
                            if (o = t[n = z(i)], r = e[i], Array.isArray(r) && (o = r[1], r = e[i] = r[0]), i !== n && (e[n] = r, delete e[i]), (a = b.cssHooks[n]) && "expand" in a) {
                                r = a.expand(r), delete e[n];
                                for (i in r) i in e || (e[i] = r[i], t[i] = o)
                            } else t[n] = o
                    }(u, c.opts.specialEasing); r < a; r++)
                    if (n = lt.prefilters[r].call(c, e, u, c.opts)) return m(n.stop) && (b._queueHooks(c.elem, c.opts.queue).stop = n.stop.bind(n)), n;
                return b.map(u, st, c), m(c.opts.start) && c.opts.start.call(e, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), b.fx.timer(b.extend(l, {
                    elem: e,
                    anim: c,
                    queue: c.opts.queue
                })), c
            }
            b.Animation = b.extend(lt, {
                tweeners: {
                    "*": [function(e, t) {
                        var i = this.createTween(e, t);
                        return re(i.elem, e, te.exec(t), i), i
                    }]
                },
                tweener: function(e, t) {
                    m(e) ? (t = e, e = ["*"]) : e = e.match(D);
                    for (var i, n = 0, o = e.length; n < o; n++) i = e[n], lt.tweeners[i] = lt.tweeners[i] || [], lt.tweeners[i].unshift(t)
                },
                prefilters: [function(e, t, i) {
                    var n, o, r, a, s, l, c, u, d = "width" in t || "height" in t,
                        p = this,
                        h = {},
                        f = e.style,
                        m = e.nodeType && ne(e),
                        v = Y.get(e, "fxshow");
                    i.queue || (null == (a = b._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function() {
                        a.unqueued || s()
                    }), a.unqueued++, p.always(function() {
                        p.always(function() {
                            a.unqueued--, b.queue(e, "fx").length || a.empty.fire()
                        })
                    }));
                    for (n in t)
                        if (o = t[n], it.test(o)) {
                            if (delete t[n], r = r || "toggle" === o, o === (m ? "hide" : "show")) {
                                if ("show" !== o || !v || void 0 === v[n]) continue;
                                m = !0
                            }
                            h[n] = v && v[n] || b.style(e, n)
                        } if ((l = !b.isEmptyObject(t)) || !b.isEmptyObject(h)) {
                        d && 1 === e.nodeType && (i.overflow = [f.overflow, f.overflowX, f.overflowY], null == (c = v && v.display) && (c = Y.get(e, "display")), "none" === (u = b.css(e, "display")) && (c ? u = c : (se([e], !0), c = e.style.display || c, u = b.css(e, "display"), se([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === b.css(e, "float") && (l || (p.done(function() {
                            f.display = c
                        }), null == c && (u = f.display, c = "none" === u ? "" : u)), f.display = "inline-block")), i.overflow && (f.overflow = "hidden", p.always(function() {
                            f.overflow = i.overflow[0], f.overflowX = i.overflow[1], f.overflowY = i.overflow[2]
                        })), l = !1;
                        for (n in h) l || (v ? "hidden" in v && (m = v.hidden) : v = Y.access(e, "fxshow", {
                            display: c
                        }), r && (v.hidden = !m), m && se([e], !0), p.done(function() {
                            m || se([e]), Y.remove(e, "fxshow");
                            for (n in h) b.style(e, n, h[n])
                        })), l = st(m ? v[n] : 0, n, p), n in v || (v[n] = l.start, m && (l.end = l.start, l.start = 0))
                    }
                }],
                prefilter: function(e, t) {
                    t ? lt.prefilters.unshift(e) : lt.prefilters.push(e)
                }
            }), b.speed = function(e, t, i) {
                var n = e && "object" == typeof e ? b.extend({}, e) : {
                    complete: i || !i && t || m(e) && e,
                    duration: e,
                    easing: i && t || t && !m(t) && t
                };
                return b.fx.off ? n.duration = 0 : "number" != typeof n.duration && (n.duration in b.fx.speeds ? n.duration = b.fx.speeds[n.duration] : n.duration = b.fx.speeds._default), null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function() {
                    m(n.old) && n.old.call(this), n.queue && b.dequeue(this, n.queue)
                }, n
            }, b.fn.extend({
                fadeTo: function(e, t, i, n) {
                    return this.filter(ne).css("opacity", 0).show().end().animate({
                        opacity: t
                    }, e, i, n)
                },
                animate: function(e, t, i, n) {
                    var o = b.isEmptyObject(e),
                        r = b.speed(t, i, n),
                        a = function() {
                            var t = lt(this, b.extend({}, e), r);
                            (o || Y.get(this, "finish")) && t.stop(!0)
                        };
                    return a.finish = a, o || !1 === r.queue ? this.each(a) : this.queue(r.queue, a)
                },
                stop: function(e, t, i) {
                    var n = function(e) {
                        var t = e.stop;
                        delete e.stop, t(i)
                    };
                    return "string" != typeof e && (i = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function() {
                        var t = !0,
                            o = null != e && e + "queueHooks",
                            r = b.timers,
                            a = Y.get(this);
                        if (o) a[o] && a[o].stop && n(a[o]);
                        else
                            for (o in a) a[o] && a[o].stop && nt.test(o) && n(a[o]);
                        for (o = r.length; o--;) r[o].elem !== this || null != e && r[o].queue !== e || (r[o].anim.stop(i), t = !1, r.splice(o, 1));
                        !t && i || b.dequeue(this, e)
                    })
                },
                finish: function(e) {
                    return !1 !== e && (e = e || "fx"), this.each(function() {
                        var t, i = Y.get(this),
                            n = i[e + "queue"],
                            o = i[e + "queueHooks"],
                            r = b.timers,
                            a = n ? n.length : 0;
                        for (i.finish = !0, b.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                        for (t = 0; t < a; t++) n[t] && n[t].finish && n[t].finish.call(this);
                        delete i.finish
                    })
                }
            }), b.each(["toggle", "show", "hide"], function(e, t) {
                var i = b.fn[t];
                b.fn[t] = function(e, n, o) {
                    return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(at(t, !0), e, n, o)
                }
            }), b.each({
                slideDown: at("show"),
                slideUp: at("hide"),
                slideToggle: at("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function(e, t) {
                b.fn[e] = function(e, i, n) {
                    return this.animate(t, e, i, n)
                }
            }), b.timers = [], b.fx.tick = function() {
                var e, t = 0,
                    i = b.timers;
                for (Qe = Date.now(); t < i.length; t++)(e = i[t])() || i[t] !== e || i.splice(t--, 1);
                i.length || b.fx.stop(), Qe = void 0
            }, b.fx.timer = function(e) {
                b.timers.push(e), b.fx.start()
            }, b.fx.interval = 13, b.fx.start = function() {
                Ze || (Ze = !0, ot())
            }, b.fx.stop = function() {
                Ze = null
            }, b.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            }, b.fn.delay = function(t, i) {
                return t = b.fx && b.fx.speeds[t] || t, i = i || "fx", this.queue(i, function(i, n) {
                    var o = e.setTimeout(i, t);
                    n.stop = function() {
                        e.clearTimeout(o)
                    }
                })
            }, et = n.createElement("input"), tt = n.createElement("select").appendChild(n.createElement("option")), et.type = "checkbox", f.checkOn = "" !== et.value, f.optSelected = tt.selected, (et = n.createElement("input")).value = "t", et.type = "radio", f.radioValue = "t" === et.value;
            var ct, ut = b.expr.attrHandle;
            b.fn.extend({
                attr: function(e, t) {
                    return q(this, b.attr, e, t, arguments.length > 1)
                },
                removeAttr: function(e) {
                    return this.each(function() {
                        b.removeAttr(this, e)
                    })
                }
            }), b.extend({
                attr: function(e, t, i) {
                    var n, o, r = e.nodeType;
                    if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? b.prop(e, t, i) : (1 === r && b.isXMLDoc(e) || (o = b.attrHooks[t.toLowerCase()] || (b.expr.match.bool.test(t) ? ct : void 0)), void 0 !== i ? null === i ? void b.removeAttr(e, t) : o && "set" in o && void 0 !== (n = o.set(e, i, t)) ? n : (e.setAttribute(t, i + ""), i) : o && "get" in o && null !== (n = o.get(e, t)) ? n : null == (n = b.find.attr(e, t)) ? void 0 : n)
                },
                attrHooks: {
                    type: {
                        set: function(e, t) {
                            if (!f.radioValue && "radio" === t && A(e, "input")) {
                                var i = e.value;
                                return e.setAttribute("type", t), i && (e.value = i), t
                            }
                        }
                    }
                },
                removeAttr: function(e, t) {
                    var i, n = 0,
                        o = t && t.match(D);
                    if (o && 1 === e.nodeType)
                        for (; i = o[n++];) e.removeAttribute(i)
                }
            }), ct = {
                set: function(e, t, i) {
                    return !1 === t ? b.removeAttr(e, i) : e.setAttribute(i, i), i
                }
            }, b.each(b.expr.match.bool.source.match(/\w+/g), function(e, t) {
                var i = ut[t] || b.find.attr;
                ut[t] = function(e, t, n) {
                    var o, r, a = t.toLowerCase();
                    return n || (r = ut[a], ut[a] = o, o = null != i(e, t, n) ? a : null, ut[a] = r), o
                }
            });
            var dt = /^(?:input|select|textarea|button)$/i,
                pt = /^(?:a|area)$/i;

            function ht(e) {
                return (e.match(D) || []).join(" ")
            }

            function ft(e) {
                return e.getAttribute && e.getAttribute("class") || ""
            }

            function mt(e) {
                return Array.isArray(e) ? e : "string" == typeof e && e.match(D) || []
            }
            b.fn.extend({
                prop: function(e, t) {
                    return q(this, b.prop, e, t, arguments.length > 1)
                },
                removeProp: function(e) {
                    return this.each(function() {
                        delete this[b.propFix[e] || e]
                    })
                }
            }), b.extend({
                prop: function(e, t, i) {
                    var n, o, r = e.nodeType;
                    if (3 !== r && 8 !== r && 2 !== r) return 1 === r && b.isXMLDoc(e) || (t = b.propFix[t] || t, o = b.propHooks[t]), void 0 !== i ? o && "set" in o && void 0 !== (n = o.set(e, i, t)) ? n : e[t] = i : o && "get" in o && null !== (n = o.get(e, t)) ? n : e[t]
                },
                propHooks: {
                    tabIndex: {
                        get: function(e) {
                            var t = b.find.attr(e, "tabindex");
                            return t ? parseInt(t, 10) : dt.test(e.nodeName) || pt.test(e.nodeName) && e.href ? 0 : -1
                        }
                    }
                },
                propFix: {
                    for: "htmlFor",
                    class: "className"
                }
            }), f.optSelected || (b.propHooks.selected = {
                get: function(e) {
                    var t = e.parentNode;
                    return t && t.parentNode && t.parentNode.selectedIndex, null
                },
                set: function(e) {
                    var t = e.parentNode;
                    t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
                }
            }), b.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
                b.propFix[this.toLowerCase()] = this
            }), b.fn.extend({
                addClass: function(e) {
                    var t, i, n, o, r, a, s, l = 0;
                    if (m(e)) return this.each(function(t) {
                        b(this).addClass(e.call(this, t, ft(this)))
                    });
                    if ((t = mt(e)).length)
                        for (; i = this[l++];)
                            if (o = ft(i), n = 1 === i.nodeType && " " + ht(o) + " ") {
                                for (a = 0; r = t[a++];) n.indexOf(" " + r + " ") < 0 && (n += r + " ");
                                o !== (s = ht(n)) && i.setAttribute("class", s)
                            } return this
                },
                removeClass: function(e) {
                    var t, i, n, o, r, a, s, l = 0;
                    if (m(e)) return this.each(function(t) {
                        b(this).removeClass(e.call(this, t, ft(this)))
                    });
                    if (!arguments.length) return this.attr("class", "");
                    if ((t = mt(e)).length)
                        for (; i = this[l++];)
                            if (o = ft(i), n = 1 === i.nodeType && " " + ht(o) + " ") {
                                for (a = 0; r = t[a++];)
                                    for (; n.indexOf(" " + r + " ") > -1;) n = n.replace(" " + r + " ", " ");
                                o !== (s = ht(n)) && i.setAttribute("class", s)
                            } return this
                },
                toggleClass: function(e, t) {
                    var i = typeof e,
                        n = "string" === i || Array.isArray(e);
                    return "boolean" == typeof t && n ? t ? this.addClass(e) : this.removeClass(e) : m(e) ? this.each(function(i) {
                        b(this).toggleClass(e.call(this, i, ft(this), t), t)
                    }) : this.each(function() {
                        var t, o, r, a;
                        if (n)
                            for (o = 0, r = b(this), a = mt(e); t = a[o++];) r.hasClass(t) ? r.removeClass(t) : r.addClass(t);
                        else void 0 !== e && "boolean" !== i || ((t = ft(this)) && Y.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : Y.get(this, "__className__") || ""))
                    })
                },
                hasClass: function(e) {
                    var t, i, n = 0;
                    for (t = " " + e + " "; i = this[n++];)
                        if (1 === i.nodeType && (" " + ht(ft(i)) + " ").indexOf(t) > -1) return !0;
                    return !1
                }
            });
            var vt = /\r/g;
            b.fn.extend({
                val: function(e) {
                    var t, i, n, o = this[0];
                    return arguments.length ? (n = m(e), this.each(function(i) {
                        var o;
                        1 === this.nodeType && (null == (o = n ? e.call(this, i, b(this).val()) : e) ? o = "" : "number" == typeof o ? o += "" : Array.isArray(o) && (o = b.map(o, function(e) {
                            return null == e ? "" : e + ""
                        })), (t = b.valHooks[this.type] || b.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                    })) : o ? (t = b.valHooks[o.type] || b.valHooks[o.nodeName.toLowerCase()]) && "get" in t && void 0 !== (i = t.get(o, "value")) ? i : "string" == typeof(i = o.value) ? i.replace(vt, "") : null == i ? "" : i : void 0
                }
            }), b.extend({
                valHooks: {
                    option: {
                        get: function(e) {
                            var t = b.find.attr(e, "value");
                            return null != t ? t : ht(b.text(e))
                        }
                    },
                    select: {
                        get: function(e) {
                            var t, i, n, o = e.options,
                                r = e.selectedIndex,
                                a = "select-one" === e.type,
                                s = a ? null : [],
                                l = a ? r + 1 : o.length;
                            for (n = r < 0 ? l : a ? r : 0; n < l; n++)
                                if (((i = o[n]).selected || n === r) && !i.disabled && (!i.parentNode.disabled || !A(i.parentNode, "optgroup"))) {
                                    if (t = b(i).val(), a) return t;
                                    s.push(t)
                                } return s
                        },
                        set: function(e, t) {
                            for (var i, n, o = e.options, r = b.makeArray(t), a = o.length; a--;)((n = o[a]).selected = b.inArray(b.valHooks.option.get(n), r) > -1) && (i = !0);
                            return i || (e.selectedIndex = -1), r
                        }
                    }
                }
            }), b.each(["radio", "checkbox"], function() {
                b.valHooks[this] = {
                    set: function(e, t) {
                        if (Array.isArray(t)) return e.checked = b.inArray(b(e).val(), t) > -1
                    }
                }, f.checkOn || (b.valHooks[this].get = function(e) {
                    return null === e.getAttribute("value") ? "on" : e.value
                })
            }), f.focusin = "onfocusin" in e;
            var gt = /^(?:focusinfocus|focusoutblur)$/,
                yt = function(e) {
                    e.stopPropagation()
                };
            b.extend(b.event, {
                trigger: function(t, i, o, r) {
                    var a, s, l, c, u, p, h, f, g = [o || n],
                        y = d.call(t, "type") ? t.type : t,
                        C = d.call(t, "namespace") ? t.namespace.split(".") : [];
                    if (s = f = l = o = o || n, 3 !== o.nodeType && 8 !== o.nodeType && !gt.test(y + b.event.triggered) && (y.indexOf(".") > -1 && (y = (C = y.split(".")).shift(), C.sort()), u = y.indexOf(":") < 0 && "on" + y, (t = t[b.expando] ? t : new b.Event(y, "object" == typeof t && t)).isTrigger = r ? 2 : 3, t.namespace = C.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + C.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = o), i = null == i ? [t] : b.makeArray(i, [t]), h = b.event.special[y] || {}, r || !h.trigger || !1 !== h.trigger.apply(o, i))) {
                        if (!r && !h.noBubble && !v(o)) {
                            for (c = h.delegateType || y, gt.test(c + y) || (s = s.parentNode); s; s = s.parentNode) g.push(s), l = s;
                            l === (o.ownerDocument || n) && g.push(l.defaultView || l.parentWindow || e)
                        }
                        for (a = 0;
                            (s = g[a++]) && !t.isPropagationStopped();) f = s, t.type = a > 1 ? c : h.bindType || y, (p = (Y.get(s, "events") || {})[t.type] && Y.get(s, "handle")) && p.apply(s, i), (p = u && s[u]) && p.apply && X(s) && (t.result = p.apply(s, i), !1 === t.result && t.preventDefault());
                        return t.type = y, r || t.isDefaultPrevented() || h._default && !1 !== h._default.apply(g.pop(), i) || !X(o) || u && m(o[y]) && !v(o) && ((l = o[u]) && (o[u] = null), b.event.triggered = y, t.isPropagationStopped() && f.addEventListener(y, yt), o[y](), t.isPropagationStopped() && f.removeEventListener(y, yt), b.event.triggered = void 0, l && (o[u] = l)), t.result
                    }
                },
                simulate: function(e, t, i) {
                    var n = b.extend(new b.Event, i, {
                        type: e,
                        isSimulated: !0
                    });
                    b.event.trigger(n, null, t)
                }
            }), b.fn.extend({
                trigger: function(e, t) {
                    return this.each(function() {
                        b.event.trigger(e, t, this)
                    })
                },
                triggerHandler: function(e, t) {
                    var i = this[0];
                    if (i) return b.event.trigger(e, t, i, !0)
                }
            }), f.focusin || b.each({
                focus: "focusin",
                blur: "focusout"
            }, function(e, t) {
                var i = function(e) {
                    b.event.simulate(t, e.target, b.event.fix(e))
                };
                b.event.special[t] = {
                    setup: function() {
                        var n = this.ownerDocument || this,
                            o = Y.access(n, t);
                        o || n.addEventListener(e, i, !0), Y.access(n, t, (o || 0) + 1)
                    },
                    teardown: function() {
                        var n = this.ownerDocument || this,
                            o = Y.access(n, t) - 1;
                        o ? Y.access(n, t, o) : (n.removeEventListener(e, i, !0), Y.remove(n, t))
                    }
                }
            });
            var Ct = e.location,
                bt = Date.now(),
                xt = /\?/;
            b.parseXML = function(t) {
                var i;
                if (!t || "string" != typeof t) return null;
                try {
                    i = (new e.DOMParser).parseFromString(t, "text/xml")
                } catch (e) {
                    i = void 0
                }
                return i && !i.getElementsByTagName("parsererror").length || b.error("Invalid XML: " + t), i
            };
            var wt = /\[\]$/,
                Tt = /\r?\n/g,
                Et = /^(?:submit|button|image|reset|file)$/i,
                Pt = /^(?:input|select|textarea|keygen)/i;

            function Nt(e, t, i, n) {
                var o;
                if (Array.isArray(t)) b.each(t, function(t, o) {
                    i || wt.test(e) ? n(e, o) : Nt(e + "[" + ("object" == typeof o && null != o ? t : "") + "]", o, i, n)
                });
                else if (i || "object" !== C(t)) n(e, t);
                else
                    for (o in t) Nt(e + "[" + o + "]", t[o], i, n)
            }
            b.param = function(e, t) {
                var i, n = [],
                    o = function(e, t) {
                        var i = m(t) ? t() : t;
                        n[n.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == i ? "" : i)
                    };
                if (Array.isArray(e) || e.jquery && !b.isPlainObject(e)) b.each(e, function() {
                    o(this.name, this.value)
                });
                else
                    for (i in e) Nt(i, e[i], t, o);
                return n.join("&")
            }, b.fn.extend({
                serialize: function() {
                    return b.param(this.serializeArray())
                },
                serializeArray: function() {
                    return this.map(function() {
                        var e = b.prop(this, "elements");
                        return e ? b.makeArray(e) : this
                    }).filter(function() {
                        var e = this.type;
                        return this.name && !b(this).is(":disabled") && Pt.test(this.nodeName) && !Et.test(e) && (this.checked || !le.test(e))
                    }).map(function(e, t) {
                        var i = b(this).val();
                        return null == i ? null : Array.isArray(i) ? b.map(i, function(e) {
                            return {
                                name: t.name,
                                value: e.replace(Tt, "\r\n")
                            }
                        }) : {
                            name: t.name,
                            value: i.replace(Tt, "\r\n")
                        }
                    }).get()
                }
            });
            var At = /%20/g,
                St = /#.*$/,
                Mt = /([?&])_=[^&]*/,
                Bt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
                Ht = /^(?:GET|HEAD)$/,
                It = /^\/\//,
                kt = {},
                _t = {},
                Dt = "*/".concat("*"),
                Ot = n.createElement("a");

            function Lt(e) {
                return function(t, i) {
                    "string" != typeof t && (i = t, t = "*");
                    var n, o = 0,
                        r = t.toLowerCase().match(D) || [];
                    if (m(i))
                        for (; n = r[o++];) "+" === n[0] ? (n = n.slice(1) || "*", (e[n] = e[n] || []).unshift(i)) : (e[n] = e[n] || []).push(i)
                }
            }

            function Ut(e, t, i, n) {
                var o = {},
                    r = e === _t;

                function a(s) {
                    var l;
                    return o[s] = !0, b.each(e[s] || [], function(e, s) {
                        var c = s(t, i, n);
                        return "string" != typeof c || r || o[c] ? r ? !(l = c) : void 0 : (t.dataTypes.unshift(c), a(c), !1)
                    }), l
                }
                return a(t.dataTypes[0]) || !o["*"] && a("*")
            }

            function Ft(e, t) {
                var i, n, o = b.ajaxSettings.flatOptions || {};
                for (i in t) void 0 !== t[i] && ((o[i] ? e : n || (n = {}))[i] = t[i]);
                return n && b.extend(!0, e, n), e
            }
            Ot.href = Ct.href, b.extend({
                active: 0,
                lastModified: {},
                etag: {},
                ajaxSettings: {
                    url: Ct.href,
                    type: "GET",
                    isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Ct.protocol),
                    global: !0,
                    processData: !0,
                    async: !0,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    accepts: {
                        "*": Dt,
                        text: "text/plain",
                        html: "text/html",
                        xml: "application/xml, text/xml",
                        json: "application/json, text/javascript"
                    },
                    contents: {
                        xml: /\bxml\b/,
                        html: /\bhtml/,
                        json: /\bjson\b/
                    },
                    responseFields: {
                        xml: "responseXML",
                        text: "responseText",
                        json: "responseJSON"
                    },
                    converters: {
                        "* text": String,
                        "text html": !0,
                        "text json": JSON.parse,
                        "text xml": b.parseXML
                    },
                    flatOptions: {
                        url: !0,
                        context: !0
                    }
                },
                ajaxSetup: function(e, t) {
                    return t ? Ft(Ft(e, b.ajaxSettings), t) : Ft(b.ajaxSettings, e)
                },
                ajaxPrefilter: Lt(kt),
                ajaxTransport: Lt(_t),
                ajax: function(t, i) {
                    "object" == typeof t && (i = t, t = void 0), i = i || {};
                    var o, r, a, s, l, c, u, d, p, h, f = b.ajaxSetup({}, i),
                        m = f.context || f,
                        v = f.context && (m.nodeType || m.jquery) ? b(m) : b.event,
                        g = b.Deferred(),
                        y = b.Callbacks("once memory"),
                        C = f.statusCode || {},
                        x = {},
                        w = {},
                        T = "canceled",
                        E = {
                            readyState: 0,
                            getResponseHeader: function(e) {
                                var t;
                                if (u) {
                                    if (!s)
                                        for (s = {}; t = Bt.exec(a);) s[t[1].toLowerCase()] = t[2];
                                    t = s[e.toLowerCase()]
                                }
                                return null == t ? null : t
                            },
                            getAllResponseHeaders: function() {
                                return u ? a : null
                            },
                            setRequestHeader: function(e, t) {
                                return null == u && (e = w[e.toLowerCase()] = w[e.toLowerCase()] || e, x[e] = t), this
                            },
                            overrideMimeType: function(e) {
                                return null == u && (f.mimeType = e), this
                            },
                            statusCode: function(e) {
                                var t;
                                if (e)
                                    if (u) E.always(e[E.status]);
                                    else
                                        for (t in e) C[t] = [C[t], e[t]];
                                return this
                            },
                            abort: function(e) {
                                var t = e || T;
                                return o && o.abort(t), P(0, t), this
                            }
                        };
                    if (g.promise(E), f.url = ((t || f.url || Ct.href) + "").replace(It, Ct.protocol + "//"), f.type = i.method || i.type || f.method || f.type, f.dataTypes = (f.dataType || "*").toLowerCase().match(D) || [""], null == f.crossDomain) {
                        c = n.createElement("a");
                        try {
                            c.href = f.url, c.href = c.href, f.crossDomain = Ot.protocol + "//" + Ot.host != c.protocol + "//" + c.host
                        } catch (e) {
                            f.crossDomain = !0
                        }
                    }
                    if (f.data && f.processData && "string" != typeof f.data && (f.data = b.param(f.data, f.traditional)), Ut(kt, f, i, E), u) return E;
                    (d = b.event && f.global) && 0 == b.active++ && b.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !Ht.test(f.type), r = f.url.replace(St, ""), f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(At, "+")) : (h = f.url.slice(r.length), f.data && (f.processData || "string" == typeof f.data) && (r += (xt.test(r) ? "&" : "?") + f.data, delete f.data), !1 === f.cache && (r = r.replace(Mt, "$1"), h = (xt.test(r) ? "&" : "?") + "_=" + bt++ + h), f.url = r + h), f.ifModified && (b.lastModified[r] && E.setRequestHeader("If-Modified-Since", b.lastModified[r]), b.etag[r] && E.setRequestHeader("If-None-Match", b.etag[r])), (f.data && f.hasContent && !1 !== f.contentType || i.contentType) && E.setRequestHeader("Content-Type", f.contentType), E.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + Dt + "; q=0.01" : "") : f.accepts["*"]);
                    for (p in f.headers) E.setRequestHeader(p, f.headers[p]);
                    if (f.beforeSend && (!1 === f.beforeSend.call(m, E, f) || u)) return E.abort();
                    if (T = "abort", y.add(f.complete), E.done(f.success), E.fail(f.error), o = Ut(_t, f, i, E)) {
                        if (E.readyState = 1, d && v.trigger("ajaxSend", [E, f]), u) return E;
                        f.async && f.timeout > 0 && (l = e.setTimeout(function() {
                            E.abort("timeout")
                        }, f.timeout));
                        try {
                            u = !1, o.send(x, P)
                        } catch (e) {
                            if (u) throw e;
                            P(-1, e)
                        }
                    } else P(-1, "No Transport");

                    function P(t, i, n, s) {
                        var c, p, h, x, w, T = i;
                        u || (u = !0, l && e.clearTimeout(l), o = void 0, a = s || "", E.readyState = t > 0 ? 4 : 0, c = t >= 200 && t < 300 || 304 === t, n && (x = function(e, t, i) {
                            for (var n, o, r, a, s = e.contents, l = e.dataTypes;
                                "*" === l[0];) l.shift(), void 0 === n && (n = e.mimeType || t.getResponseHeader("Content-Type"));
                            if (n)
                                for (o in s)
                                    if (s[o] && s[o].test(n)) {
                                        l.unshift(o);
                                        break
                                    } if (l[0] in i) r = l[0];
                            else {
                                for (o in i) {
                                    if (!l[0] || e.converters[o + " " + l[0]]) {
                                        r = o;
                                        break
                                    }
                                    a || (a = o)
                                }
                                r = r || a
                            }
                            if (r) return r !== l[0] && l.unshift(r), i[r]
                        }(f, E, n)), x = function(e, t, i, n) {
                            var o, r, a, s, l, c = {},
                                u = e.dataTypes.slice();
                            if (u[1])
                                for (a in e.converters) c[a.toLowerCase()] = e.converters[a];
                            for (r = u.shift(); r;)
                                if (e.responseFields[r] && (i[e.responseFields[r]] = t), !l && n && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift())
                                    if ("*" === r) r = l;
                                    else if ("*" !== l && l !== r) {
                                if (!(a = c[l + " " + r] || c["* " + r]))
                                    for (o in c)
                                        if ((s = o.split(" "))[1] === r && (a = c[l + " " + s[0]] || c["* " + s[0]])) {
                                            !0 === a ? a = c[o] : !0 !== c[o] && (r = s[0], u.unshift(s[1]));
                                            break
                                        } if (!0 !== a)
                                    if (a && e.throws) t = a(t);
                                    else try {
                                        t = a(t)
                                    } catch (e) {
                                        return {
                                            state: "parsererror",
                                            error: a ? e : "No conversion from " + l + " to " + r
                                        }
                                    }
                            }
                            return {
                                state: "success",
                                data: t
                            }
                        }(f, x, E, c), c ? (f.ifModified && ((w = E.getResponseHeader("Last-Modified")) && (b.lastModified[r] = w), (w = E.getResponseHeader("etag")) && (b.etag[r] = w)), 204 === t || "HEAD" === f.type ? T = "nocontent" : 304 === t ? T = "notmodified" : (T = x.state, p = x.data, c = !(h = x.error))) : (h = T, !t && T || (T = "error", t < 0 && (t = 0))), E.status = t, E.statusText = (i || T) + "", c ? g.resolveWith(m, [p, T, E]) : g.rejectWith(m, [E, T, h]), E.statusCode(C), C = void 0, d && v.trigger(c ? "ajaxSuccess" : "ajaxError", [E, f, c ? p : h]), y.fireWith(m, [E, T]), d && (v.trigger("ajaxComplete", [E, f]), --b.active || b.event.trigger("ajaxStop")))
                    }
                    return E
                },
                getJSON: function(e, t, i) {
                    return b.get(e, t, i, "json")
                },
                getScript: function(e, t) {
                    return b.get(e, void 0, t, "script")
                }
            }), b.each(["get", "post"], function(e, t) {
                b[t] = function(e, i, n, o) {
                    return m(i) && (o = o || n, n = i, i = void 0), b.ajax(b.extend({
                        url: e,
                        type: t,
                        dataType: o,
                        data: i,
                        success: n
                    }, b.isPlainObject(e) && e))
                }
            }), b._evalUrl = function(e) {
                return b.ajax({
                    url: e,
                    type: "GET",
                    dataType: "script",
                    cache: !0,
                    async: !1,
                    global: !1,
                    throws: !0
                })
            }, b.fn.extend({
                wrapAll: function(e) {
                    var t;
                    return this[0] && (m(e) && (e = e.call(this[0])), t = b(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                        for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                        return e
                    }).append(this)), this
                },
                wrapInner: function(e) {
                    return m(e) ? this.each(function(t) {
                        b(this).wrapInner(e.call(this, t))
                    }) : this.each(function() {
                        var t = b(this),
                            i = t.contents();
                        i.length ? i.wrapAll(e) : t.append(e)
                    })
                },
                wrap: function(e) {
                    var t = m(e);
                    return this.each(function(i) {
                        b(this).wrapAll(t ? e.call(this, i) : e)
                    })
                },
                unwrap: function(e) {
                    return this.parent(e).not("body").each(function() {
                        b(this).replaceWith(this.childNodes)
                    }), this
                }
            }), b.expr.pseudos.hidden = function(e) {
                return !b.expr.pseudos.visible(e)
            }, b.expr.pseudos.visible = function(e) {
                return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
            }, b.ajaxSettings.xhr = function() {
                try {
                    return new e.XMLHttpRequest
                } catch (e) {}
            };
            var jt = {
                    0: 200,
                    1223: 204
                },
                Rt = b.ajaxSettings.xhr();
            f.cors = !!Rt && "withCredentials" in Rt, f.ajax = Rt = !!Rt, b.ajaxTransport(function(t) {
                var i, n;
                if (f.cors || Rt && !t.crossDomain) return {
                    send: function(o, r) {
                        var a, s = t.xhr();
                        if (s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                            for (a in t.xhrFields) s[a] = t.xhrFields[a];
                        t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                        for (a in o) s.setRequestHeader(a, o[a]);
                        i = function(e) {
                            return function() {
                                i && (i = n = s.onload = s.onerror = s.onabort = s.ontimeout = s.onreadystatechange = null, "abort" === e ? s.abort() : "error" === e ? "number" != typeof s.status ? r(0, "error") : r(s.status, s.statusText) : r(jt[s.status] || s.status, s.statusText, "text" !== (s.responseType || "text") || "string" != typeof s.responseText ? {
                                    binary: s.response
                                } : {
                                    text: s.responseText
                                }, s.getAllResponseHeaders()))
                            }
                        }, s.onload = i(), n = s.onerror = s.ontimeout = i("error"), void 0 !== s.onabort ? s.onabort = n : s.onreadystatechange = function() {
                            4 === s.readyState && e.setTimeout(function() {
                                i && n()
                            })
                        }, i = i("abort");
                        try {
                            s.send(t.hasContent && t.data || null)
                        } catch (e) {
                            if (i) throw e
                        }
                    },
                    abort: function() {
                        i && i()
                    }
                }
            }), b.ajaxPrefilter(function(e) {
                e.crossDomain && (e.contents.script = !1)
            }), b.ajaxSetup({
                accepts: {
                    script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
                },
                contents: {
                    script: /\b(?:java|ecma)script\b/
                },
                converters: {
                    "text script": function(e) {
                        return b.globalEval(e), e
                    }
                }
            }), b.ajaxPrefilter("script", function(e) {
                void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
            }), b.ajaxTransport("script", function(e) {
                var t, i;
                if (e.crossDomain) return {
                    send: function(o, r) {
                        t = b("<script>").prop({
                            charset: e.scriptCharset,
                            src: e.url
                        }).on("load error", i = function(e) {
                            t.remove(), i = null, e && r("error" === e.type ? 404 : 200, e.type)
                        }), n.head.appendChild(t[0])
                    },
                    abort: function() {
                        i && i()
                    }
                }
            });
            var qt, Wt = [],
                Gt = /(=)\?(?=&|$)|\?\?/;
            b.ajaxSetup({
                jsonp: "callback",
                jsonpCallback: function() {
                    var e = Wt.pop() || b.expando + "_" + bt++;
                    return this[e] = !0, e
                }
            }), b.ajaxPrefilter("json jsonp", function(t, i, n) {
                var o, r, a, s = !1 !== t.jsonp && (Gt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Gt.test(t.data) && "data");
                if (s || "jsonp" === t.dataTypes[0]) return o = t.jsonpCallback = m(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Gt, "$1" + o) : !1 !== t.jsonp && (t.url += (xt.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function() {
                    return a || b.error(o + " was not called"), a[0]
                }, t.dataTypes[0] = "json", r = e[o], e[o] = function() {
                    a = arguments
                }, n.always(function() {
                    void 0 === r ? b(e).removeProp(o) : e[o] = r, t[o] && (t.jsonpCallback = i.jsonpCallback, Wt.push(o)), a && m(r) && r(a[0]), a = r = void 0
                }), "script"
            }), f.createHTMLDocument = ((qt = n.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === qt.childNodes.length), b.parseHTML = function(e, t, i) {
                return "string" != typeof e ? [] : ("boolean" == typeof t && (i = t, t = !1), t || (f.createHTMLDocument ? ((o = (t = n.implementation.createHTMLDocument("")).createElement("base")).href = n.location.href, t.head.appendChild(o)) : t = n), r = S.exec(e), a = !i && [], r ? [t.createElement(r[1])] : (r = ge([e], t, a), a && a.length && b(a).remove(), b.merge([], r.childNodes)));
                var o, r, a
            }, b.fn.load = function(e, t, i) {
                var n, o, r, a = this,
                    s = e.indexOf(" ");
                return s > -1 && (n = ht(e.slice(s)), e = e.slice(0, s)), m(t) ? (i = t, t = void 0) : t && "object" == typeof t && (o = "POST"), a.length > 0 && b.ajax({
                    url: e,
                    type: o || "GET",
                    dataType: "html",
                    data: t
                }).done(function(e) {
                    r = arguments, a.html(n ? b("<div>").append(b.parseHTML(e)).find(n) : e)
                }).always(i && function(e, t) {
                    a.each(function() {
                        i.apply(this, r || [e.responseText, t, e])
                    })
                }), this
            }, b.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
                b.fn[t] = function(e) {
                    return this.on(t, e)
                }
            }), b.expr.pseudos.animated = function(e) {
                return b.grep(b.timers, function(t) {
                    return e === t.elem
                }).length
            }, b.offset = {
                setOffset: function(e, t, i) {
                    var n, o, r, a, s, l, c = b.css(e, "position"),
                        u = b(e),
                        d = {};
                    "static" === c && (e.style.position = "relative"), s = u.offset(), r = b.css(e, "top"), l = b.css(e, "left"), ("absolute" === c || "fixed" === c) && (r + l).indexOf("auto") > -1 ? (a = (n = u.position()).top, o = n.left) : (a = parseFloat(r) || 0, o = parseFloat(l) || 0), m(t) && (t = t.call(e, i, b.extend({}, s))), null != t.top && (d.top = t.top - s.top + a), null != t.left && (d.left = t.left - s.left + o), "using" in t ? t.using.call(e, d) : u.css(d)
                }
            }, b.fn.extend({
                offset: function(e) {
                    if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                        b.offset.setOffset(this, e, t)
                    });
                    var t, i, n = this[0];
                    return n ? n.getClientRects().length ? (t = n.getBoundingClientRect(), i = n.ownerDocument.defaultView, {
                        top: t.top + i.pageYOffset,
                        left: t.left + i.pageXOffset
                    }) : {
                        top: 0,
                        left: 0
                    } : void 0
                },
                position: function() {
                    if (this[0]) {
                        var e, t, i, n = this[0],
                            o = {
                                top: 0,
                                left: 0
                            };
                        if ("fixed" === b.css(n, "position")) t = n.getBoundingClientRect();
                        else {
                            for (t = this.offset(), i = n.ownerDocument, e = n.offsetParent || i.documentElement; e && (e === i.body || e === i.documentElement) && "static" === b.css(e, "position");) e = e.parentNode;
                            e && e !== n && 1 === e.nodeType && ((o = b(e).offset()).top += b.css(e, "borderTopWidth", !0), o.left += b.css(e, "borderLeftWidth", !0))
                        }
                        return {
                            top: t.top - o.top - b.css(n, "marginTop", !0),
                            left: t.left - o.left - b.css(n, "marginLeft", !0)
                        }
                    }
                },
                offsetParent: function() {
                    return this.map(function() {
                        for (var e = this.offsetParent; e && "static" === b.css(e, "position");) e = e.offsetParent;
                        return e || ye
                    })
                }
            }), b.each({
                scrollLeft: "pageXOffset",
                scrollTop: "pageYOffset"
            }, function(e, t) {
                var i = "pageYOffset" === t;
                b.fn[e] = function(n) {
                    return q(this, function(e, n, o) {
                        var r;
                        if (v(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === o) return r ? r[t] : e[n];
                        r ? r.scrollTo(i ? r.pageXOffset : o, i ? o : r.pageYOffset) : e[n] = o
                    }, e, n, arguments.length)
                }
            }), b.each(["top", "left"], function(e, t) {
                b.cssHooks[t] = je(f.pixelPosition, function(e, i) {
                    if (i) return i = Fe(e, t), Oe.test(i) ? b(e).position()[t] + "px" : i
                })
            }), b.each({
                Height: "height",
                Width: "width"
            }, function(e, t) {
                b.each({
                    padding: "inner" + e,
                    content: t,
                    "": "outer" + e
                }, function(i, n) {
                    b.fn[n] = function(o, r) {
                        var a = arguments.length && (i || "boolean" != typeof o),
                            s = i || (!0 === o || !0 === r ? "margin" : "border");
                        return q(this, function(t, i, o) {
                            var r;
                            return v(t) ? 0 === n.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (r = t.documentElement, Math.max(t.body["scroll" + e], r["scroll" + e], t.body["offset" + e], r["offset" + e], r["client" + e])) : void 0 === o ? b.css(t, i, s) : b.style(t, i, o, s)
                        }, t, a ? o : void 0, a)
                    }
                })
            }), b.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, t) {
                b.fn[t] = function(e, i) {
                    return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
                }
            }), b.fn.extend({
                hover: function(e, t) {
                    return this.mouseenter(e).mouseleave(t || e)
                }
            }), b.fn.extend({
                bind: function(e, t, i) {
                    return this.on(e, null, t, i)
                },
                unbind: function(e, t) {
                    return this.off(e, null, t)
                },
                delegate: function(e, t, i, n) {
                    return this.on(t, e, i, n)
                },
                undelegate: function(e, t, i) {
                    return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
                }
            }), b.proxy = function(e, t) {
                var i, n, o;
                if ("string" == typeof t && (i = e[t], t = e, e = i), m(e)) return n = r.call(arguments, 2), (o = function() {
                    return e.apply(t || this, n.concat(r.call(arguments)))
                }).guid = e.guid = e.guid || b.guid++, o
            }, b.holdReady = function(e) {
                e ? b.readyWait++ : b.ready(!0)
            }, b.isArray = Array.isArray, b.parseJSON = JSON.parse, b.nodeName = A, b.isFunction = m, b.isWindow = v, b.camelCase = z, b.type = C, b.now = Date.now, b.isNumeric = function(e) {
                var t = b.type(e);
                return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
            }, "function" == typeof define && define.amd && define("jquery", [], function() {
                return b
            });
            var Vt = e.jQuery,
                zt = e.$;
            return b.noConflict = function(t) {
                return e.$ === b && (e.$ = zt), t && e.jQuery === b && (e.jQuery = Vt), b
            }, t || (e.jQuery = e.$ = b), b
        })
    }, {}]
}, {}, [2, 3, 4, 5, 6, 30, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]);
