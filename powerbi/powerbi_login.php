<?php
function get_access_token(){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://login.microsoftonline.com/common/oauth2/token",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "client_id=a2da5cdd-55ea-4188-9afd-aace9185c12a&grant_type=password&resource=https%3A%2F%2Fanalysis.windows.net%2Fpowerbi%2Fapi&username=dionnald.beh%40reveronconsulting.com&password=Reveron0415%21",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/x-www-form-urlencoded",
    ),
    //CURLOPT_SSL_VERIFYPEER => false,
    //CURLOPT_SSL_VERIFYHOST => false
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    return "cURL Error #:" . $err;
  } else {
    // $response = json_decode($response);
    // // $json = json_decode($manage);
    // $response = json_encode($response);
    $data = json_decode($response, true);
    return $data["access_token"];
    // echo gettype($response);

    // echo "\n". $json;
  }
};

function generate_token(){
    $curl = curl_init();
    $auth_token = "Bearer ".get_access_token();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.powerbi.com/v1.0/myorg/groups/d4394867-790f-4f14-a4fe-b1246a4dfae0/reports/b37d3739-02bc-4e4d-9c5c-7fdc5ffcf824/GenerateToken",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "accessLevel=View&allowSaveAs=false",
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Authorization: ".$auth_token,
        "Cache-Control: no-cache",
        "Content-Type: application/x-www-form-urlencoded"
      ),
	//CURLOPT_SSL_VERIFYPEER => false,
    //    CURLOPT_SSL_VERIFYHOST => false
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      $data = json_decode($response, true);
      return $data["token"];
    }
};

if(isset($_GET)) {
  echo (generate_token());
}
?>
