

# read all folder in tilesets folder
$folders = Get-ChildItem -Path "./tilesets/" -Recurse -Filter "*_appData.json" | ForEach-Object { Resolve-Path $_.FullName -Relative }
$foldersString = $folders -join ","
$foldersString = $foldersString -replace ".\\tilesets", "tilesets"
$foldersString = $foldersString -replace "\\", "/"

# update all filepath to appData.json file.
$filepath = "index.html"
$file = [IO.File]::ReadAllText("$filepath")
$file -replace 'tilesets=(?s)".*"', "tilesets=`"$foldersString`"" | Set-Content $filePath