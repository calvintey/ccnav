<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Use correct character set. -->
    <meta charset="utf-8">
    <!-- Tell IE to use the latest, best version. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Make the application on mobile take up the full browser screen and disable user scaling. -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <title>ContextCapture Navigator</title>
    <script src="app/thirdParty/jquery/jquery-3.3.1.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="app/thirdParty/kendo/kendo.common.min.css"?rel="stylesheet"?type="text/css"/>
    <script src="app/thirdParty/kendo/kendo.all.min.js"></script>
    <script src="bimium/bimium.b82ae333736e94ef.js"></script>
    <link rel="stylesheet" href="bimium/bimium.1ae6cefcc3cfb658.css"/>
    <link rel="stylesheet" href="app/content/vivo.436fe8b539c35fc6.css"/>
    <link rel="stylesheet" href="helper.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script>

<script type="text/javascript" language="javascript" src="https://rawgit.com/Microsoft/PowerBI-JavaScript/master/dist/powerbi.min.js"></script>

    <style>
          html {
              height:100%;
              width:100%;
          }
    </style>
</head>
<body>
  <div id="biframe">
      <div id= "infoPopup" class = "popup">
        <div id = "infoRootNode"></div>
      </div>
		<div id="reportContainer" class="iframepopup"></div>
	 </div>
 </div>
  <div id="Info"/>
    <script id="vivoscript"
            src="index.e8f4c825fbfecf4f.js"
            tilesets="tilesets/CCNavTutorial/CCNavTutorial_AppData.json,tilesets/KL/KL_AppData.json">
    </script>
    <script src="helper.js"></script>
</body>
</html>
