var flagMarker = false; //flag true if to mark a billboard/location
var flagInfo = false; //flag true if info button is chosen
var flagEdit = false;
var flagSettings = false; //flag if settings button is chosen
var NumberOfEntities =0; // total number of entities
var currentEntityID =0;
var NumberOfFlextures =0; // total number of flextures
var distanceEntity =0; // keeps track of number of distance entities
var totalDistance =0; // total distance from first enity to last
var positions; // used to draw a line between distance entities
var oldlong, oldlat; // used for calculating distance between old and current long lat
var totalDistanceInMeters=0;
var myEntities=[]; //array of entitites details
var myFlextures =[]; //array of flexture details
var distEntities =[]; // array of distance entities -Cesium bill boards to be removed if deleted
var entitiesArray =[]; // array of Cesium billboard entities-- needed for delete purpose
var tableCellPicked; // to select the enitiy from table
var tableCellOldColor; // to save the old color of the cell
var tableRowPicked;  // to select the row of flexture picked
var tableRowOldColor; // to save the old color of the row
var isFlexturePicked = false;// true if flexure is picked in the table
var isEntityPicked = false; // true if enitiy is picked in the table
var entityIndex;// stores the index number of the the enitity picked to access the enitiy from array
var tableRowIndex; // stores the table row index picked -used to get the flexture from array
var isFlextureEdit = false; // indicates if the flexture is edited or the enitity
var boundingSphere; // used for setting the camera view
const myNavUrl = 'https://103.21.34.144/ws/v2.5/Repositories/Bentley.PW--Reveron-VM.REVERONCONSULTING.COM~3AREV01/Navigation/NavNode'
const myDocUrl = 'https://103.21.34.144/ws/v2.5/Repositories/Bentley.PW--Reveron-VM.REVERONCONSULTING.COM~3AREV01/PW_WSG/Document'
var myUrl="";
var NoInstanceFound = false;
var instanceId;
var parentGuid;
var setErrorFlag = false;
var myInstances =[];
var isGetFileUrl = false;
var filePath;
var getFileUrlEntity;
var folders = [];
var locationList =[];
var infoLocationList =[];
var currentLat;
var currentLng;
var selectedNodeId;
var selectedInfoEntity ;

function LoadJSON(callback){
	//used initially to load all the saved up data from mydata.json
	var myobj = new XMLHttpRequest();
	myobj.overrideMimeType("application/json");
	myobj.open('GET','load_file/getAssetdata.php', true);
	myobj.onreadystatechange = function(){
		if(myobj.readyState == 4 && myobj.status =="200"){
			callback(myobj.responseText);
		}
	};
	myobj.send(null);
}

function SaveMyData(){
	// this function is used to save the data to mydata.json
	folders = [];
	//var myJsonObj = {"entities": NumberOfEntities, "currentEntityID": currentEntityID,"records": myEntities, "flextures": NumberOfFlextures, "flextureList": myFlextures, "getFileUrlEntity": "none", "Paths":folders };
	var myJsonObj = {"entities": NumberOfEntities, "currentEntityID": currentEntityID,"records": myEntities};
	var str_json = JSON.stringify(myJsonObj);
	//alert (str_json);
	var request = new XMLHttpRequest();
	request.open("POST","load_file/saveAssetdata.php",true);
	request.setRequestHeader("Content-type", "application/json");
	request.send (str_json);
	request.onreadystatechange = function(){
		if(request.readyState == 4 && request.status =="200"){
			//alert(request.responseText);
			if(request.responseText){
				alert("Data is Saved");
			};
		};
	};
}
function SaveFileData(){
	// this function is needed to get the files information from projectwise and save them when the path is provided
	//if(!isGetFileUrl){
	//	getFileUrlEntity ="none";
	//	folders = [];
	//};
	var myJsonObj = {"entities": NumberOfEntities, "currentEntityID": currentEntityID,"records": myEntities, "getFileUrlEntity": getFileUrlEntity,"Paths":filePath};
	//isGetFileUrl = false;


	//alert (myJsonObj.flextureList[0].Name); Json object is correctly assigned
	var str_json = JSON.stringify(myJsonObj);
	//alert (str_json);
	var request = new XMLHttpRequest();
	request.open("POST","load_file/saveAssetfiledata.php",true);
	request.setRequestHeader("Content-type", "application/json");
	request.send (str_json);
	request.onreadystatechange = function(){
		if(request.readyState == 4 && request.status =="200"){
			//alert(request.responseText);
			var savedObj = request.responseText;
			alert(request.responseText);
			alert("Files have been fetched/refreshed from ProjectWise");
			var myJson = JSON.parse(savedObj);
			if(myJson){
				//alert (myJson);
				myEntities = myJson['records'];
				var j=0;
				while (j< NumberOfEntities){
					if( myEntities[j].Name == getFileUrlEntity){
						break;
					}
					else {
						j++;
					}
				};
				isGetFileUrl = false;
				for(var i=0; i<NumberOfEntities; i++){
					if (entitiesArray[i].name == getFileUrlEntity){
						var desc =  '<table ><tbody >';
						desc += '<tr><td>' + "Longitude : " +  myEntities[j].Long +'</td></tr>';
						desc += '<tr><td>' + "Latitude : " +  myEntities[j].Lat +'</td></tr>';
						desc += '<tr><td></td></tr>';
						desc +=  '<tr><td>' + "Documents :" + '</td></tr>';
						var myNode = $('#infoRootNode').jstree().get_json(myEntities[j].Name);
						//alert(myNode.id);
						//var childNodes = $('#infoRootNode').jstree().get_children_dom(myNode);
						//alert(myNode.children.length); it works
						for(var ch = 0; ch < myNode.children.length; ch++){
							$('#infoRootNode').jstree().delete_node(myNode.children[ch].id);
						};
						var nfolders =myEntities[j].Data.length;
						for(var k=0; k<nfolders; k++){
							var myname = myEntities[j].Name +":"+myEntities[j].Data[k].folder;
							//myname+ = ":"+ myEntities[j].Data[k].folder;
							infoLocationList.push({
								id: myname,
								parent: myEntities[j].Name,
								text : myEntities[j].Data[k].folder
							});
							var name ='#'+myEntities[j].Name;
							$('#infoRootNode').jstree().create_node(name,{
								"id": myname,
								"text" : myEntities[j].Data[k].folder}, 'last');

							var nfiles = myEntities[j].Data[k].fileData.length;
							for(var l=0; l<nfiles; l++){
								var url = myEntities[j].Data[k].fileData[l].Url;
								desc += '<tr><td><a href ='+url +' target =_blank">'+ myEntities[j].Data[k].fileData[l].fileName +'</a></td></tr>';
								infoLocationList.push({
									id:  myEntities[j].Data[k].fileData[l].fileName ,
									parent:myname,
									text :'<a href ='+url +' target =_blank">'+ myEntities[j].Data[k].fileData[l].fileName +'</a>'
								});
								//var name ='#'+myname;
								//$('#infoRootNode').jstree().create_node(name,{
								//	"id": myEntities[j].Data[k].fileData[l].fileName ,
								//	"text" :'<a href ='+url +' target =_blank">'+ myEntities[j].Data[k].fileData[l].fileName +'</a>'}, 'last');
							};
						};
						desc += '</tbody></table>';
						entitiesArray[i].description = desc;
					};
				};
			};
		};
	};
}


LoadJSON(function(response){
	// all the saved data is loaded at the start of the application
	var actual_JSON = JSON.parse(response);

	NumberOfEntities = actual_JSON.entities;
	currentEntityID = actual_JSON.currentEntityID;

	myEntities = actual_JSON['records'];
	locationList.push({
		id: "regions",
		parent: "#",
		text: "Regions"
	});
	infoLocationList.push({
		id: "regions",
		parent: "#",
		text: "Regions"
	});
	//alert("loaded");
	//NumberOfFlextures =actual_JSON.flextures;
	//myFlextures = actual_JSON['flextureList'];
		//alert(NumberOfEntities);
	//var tbody = document.getElementById('tbody');
	for(var i=0; i< NumberOfEntities; i++){
		var ID = myEntities[i].recid;
		var myname = myEntities[i].Name;
		var lon = myEntities[i].Long;
		var lat = myEntities[i].Lat;
		//var description = '<table class = "cesium-infoBox-defaultTable cesium-infoBox-defaultTable-lighter"><tbody>';
		//description += '<tr><th>' + "Longitude" + '</th><td>' + lon +'</td></tr>';
		//description += '<tr><th>' + "Latitude" + '</th><td>' + lat +'</td></tr>';
		//description += '</tbody></table>';
		//w2alert( ID +' '+name + ' ' +lon + ' ' + lat);
		var isRegion =false;
		var isSubregion= false;
		//alert (locationList.length);
		for(var j=0; j< locationList.length; j++){
			if(locationList[j].id == myEntities[i].Region){
				isRegion = true;
			};
			if (locationList[j].id == myEntities[i].Subregion){
				isSubregion = true;
			};
		};
		if(!isRegion ){
			locationList.push({
				id: myEntities[i].Region,
				parent: "regions",
				text: myEntities[i].Region
			});
			infoLocationList.push({
				id: myEntities[i].Region,
				parent: "regions",
				text: myEntities[i].Region
			});
		};
		if(!isSubregion ){
			locationList.push({
				id: myEntities[i].Subregion,
				parent: myEntities[i].Region,
				text: myEntities[i].Subregion
			});
			infoLocationList.push({
				id: myEntities[i].Subregion,
				parent: myEntities[i].Region,
				text: myEntities[i].Subregion
			});
		};


		locationList.push({
			id: myEntities[i].Name,
			parent: myEntities[i].Subregion,
			text: myEntities[i].Name
		});
		infoLocationList.push({
			id: myEntities[i].Name,
			parent: myEntities[i].Subregion,
			text: myEntities[i].Name
		});

		var desc =  '<table ><tbody >';

		desc += '<tr><td>' + "Longitude : " +  lon +'</td></tr>';
		desc += '<tr><td>' + "Latitude : " +  lat +'</td></tr>';
		desc += '<tr><td></td></tr>';
		desc +=  '<tr><td>' + "Documents :" + '</td></tr>';
		var nfolders =myEntities[i].Data.length;
		for(var k=0; k<nfolders; k++){
			var name = myEntities[i].Name +":"+myEntities[i].Data[k].folder;
			//name+ = ":"+ myEntities[i].Data[k].folder;
			infoLocationList.push({
				id: name,
				parent: myEntities[i].Name,
				text : myEntities[i].Data[k].folder
			});

			var nfiles = myEntities[i].Data[k].fileData.length;
			for(var l=0; l<nfiles; l++){
				var url = myEntities[i].Data[k].fileData[l].Url;
				desc += '<tr><td><a href ='+url +' target =_blank">'+ myEntities[i].Data[k].fileData[l].fileName +'</a></td></tr>';
				//infoLocationList.push({
				//	id:  myEntities[i].Data[k].fileData[l].fileName ,
				//	parent: name,
				//	text :'<a href ='+url +' target =_blank">'+ myEntities[i].Data[k].fileData[l].fileName +'</a>'
				//});

			};
		};
		desc += '</tbody></table>';
	//	var pinBuilder= new Cesium.PinBuilder();
		// entitiesArray.push(viewer.entities.add({
		// 	id :ID,
		// 	name: myname,
		// 	position : Cesium.Cartesian3.fromDegrees(lon,lat),
		// 	 billboard:{
		// 		 image: pinBuilder.fromColor(Cesium.Color.BLUE, 48).toDataURL(),
		// 		 verticalOrigin: Cesium.VerticalOrigin.BOTTOM
		// 	 },
		// 	 description: desc,
		// 	 label:{
		// 		 text: myname,
		// 		 font:'14pt monospace',
		// 		 style: Cesium.LabelStyle.FILL_AND_OUTLINE,
		// 		 outlineWidth: 2,
		// 		 verticalOrigin: Cesium.VerticalOrigin.TOP,
		// 		 pixelOffset : new Cesium.Cartesian2(0, -9)
		// 	 }
		//  })
   // );
   };

		/*for(var k=0; k < myEntities[i].Children.length; k++){
			infoLocationList.push({
				id: myEntities[i].Children[k].floor,
				parent: myEntities[i].Name,
				text: myEntities[i].Children[k].floor
			});
			for(var l=0; l < myEntities[i].Children[k].depts.length; l++){
				infoLocationList.push({
				id: myEntities[i].Children[k].depts[l],
				parent: myEntities[i].Children[k].floor,
				text: myEntities[i].Children[k].depts[l]
				});
			};
		};*/


		/*var row = tbody.insertRow();
		var cell = row.insertCell();
		cell.innerHTML = myname;*/

	// $('#rootNode').jstree({
	// 	'core': {
	// 		'data': locationList,
	// 		'check_callback': true
	// 	}
	// });

	$('#infoRootNode').jstree({
		'core':{
			'data':infoLocationList,
			'check_callback': true
		}
	});
});

document.getElementById('Info').onclick = function(){
	flagInfo=true;
	document.getElementById('Home').style.display ='none';
	document.getElementById('Earth').style.display ='none';
	document.getElementById('Distance').style.display ='none';
	document.getElementById('Marker').style.display = 'none';
	document.getElementById('WorkOrder').style.display ='none';
	document.getElementById('Info').style.display = 'none';
	var closeButton = document.getElementById('Close');
	closeButton.style.display ='inline';
	closeButton.style.float ='left';
	//document.getElementById('Close').style.display = 'inline';
	var myPopup = document.getElementById('infoPopup');
	myPopup.style.display ='block';
};

// $('#rootNode').on("select_node.jstree", function (e, data) {
// 	//alert("node_id: " + data.node.id);
// 	if( selectedNodeId=== data.node.id){
// 		data.instance.deselect_node(data.node);
// 		selectedNodeId = "";
// 		//alert("deselected");
// 		$('#rootNode').jstree(true).redraw();
// 		var i=0;
// 		while(i<myEntities.length){
// 			if(myEntities[i].Name == data.node.id){
// 				isEntityPicked =false;
// 				//viewer.click();
// 				viewer.selectedEntity ="";
//
//
// 				break;
// 			};
// 			i++;
// 		};
// 		//isEntityPicked = false;
// 		//viewer.selectedEntity = "";
// 	}else{
// 		var nodeId = data.node.id;
// 		selectedNodeId = data.node.id;
//
// 		//alert (nodeId);
// 		var i=0;
// 		while(i<myEntities.length){
// 			if(myEntities[i].Name == nodeId){
// 				entityIndex = i;
// 				isEntityPicked =true;
// 				var myEntity = entitiesArray[i];
// 				viewer.camera.flyTo({
// 					destination : Cesium.Cartesian3.fromDegrees(myEntities[i].Long, myEntities[i].Lat, 15000.0)
// 				});
//
// 				viewer.selectedEntity = myEntity;
// 				break;
// 			};
// 			i++;
// 		};
//     //powerbi dashboard filter
//     //setSlicerState(nodeId);
//
// 	};
//
// });

$('#infoRootNode').on("select_node.jstree", function (e, data) {
	//alert("node_id: " + data.node.id);
	if( selectedNodeId=== data.node.id){
		data.instance.deselect_node(data.node);
		selectedNodeId = "";
		//alert("deselected");
		$('#infoRootNode').jstree(true).redraw();
		var i=0;
		while(i<myEntities.length){
			if(myEntities[i].Name == data.node.id){
				isEntityPicked =false;
				//viewer.selectedEntity ="";
				break;
			};
			i++;
		};
		//isEntityPicked = false;
		//viewer.selectedEntity = "";
	}else{
		var nodeId;
		var folder;
		var flagFolder = false;
		var flagLoc = false;
		selectedNodeId = data.node.id;
		var pos = selectedNodeId.indexOf(":");
		if(pos > 0){
			nodeId = selectedNodeId.substr(0,pos);
			folder = selectedNodeId.substr(pos+1);
			flagFolder = true;
		}else{
			nodeId = selectedNodeId;
			flagLoc = true;
		};

		var i=0;
		while(i<myEntities.length){
			if(myEntities[i].Name == nodeId){
				//alert(nodeId);
				entityIndex = i;
				isEntityPicked =true;

				if(flagLoc){
					//alert("loc");
					var desc =  '<table ><tbody >';
						desc += '<tr><td>' + "Longitude : " +  myEntities[i].Long +'</td></tr>';
						desc += '<tr><td>' + "Latitude : " +  myEntities[i].Lat +'</td></tr>';
						desc += '<tr><td></td></tr>';
						desc +=  '<tr><td>' + "Documents :" + '</td></tr>';
					for(var k=0; k < myEntities[i].Data.length; k++){
						//desc += '<tr><td>'+myEntities[i].Data[k].folder+ '</td></tr>';
						var nfiles = myEntities[i].Data[k].fileData.length;
						for(var l=0; l<nfiles; l++){
							var url = myEntities[i].Data[k].fileData[l].Url;
							desc += '<tr><td><a href ='+ url +' target =_blank">'+ myEntities[i].Data[k].fileData[l].fileName + '</a></td></tr>';
						};
					};
					desc += '</tbody></table>';
					entitiesArray[i].description = desc;
					flagLoc = false;
				};
				if(selectedInfoEntity!= nodeId){
					var myEntity = entitiesArray[i];
					// viewer.camera.flyTo({
					// 	destination : Cesium.Cartesian3.fromDegrees(myEntities[i].Long, myEntities[i].Lat, 15000.0)
					// });

					//viewer.selectedEntity = myEntity;
					selectedInfoEntity = nodeId;
				};
					//alert(selectedInfoEntity);

				break;

			};
			i++;
		};
    //powerbi dashboard filter
    //setSlicerState(nodeId);

		if(flagFolder){
			var j=0;
			while (j< myEntities[i].Data.length){
				if(myEntities[i].Data[j].folder == folder){
					var desc =  '<table ><tbody >';
						desc += '<tr><td>' + "Longitude : " +  myEntities[i].Long +'</td></tr>';
						desc += '<tr><td>' + "Latitude : " +  myEntities[i].Lat +'</td></tr>';
						desc += '<tr><td></td></tr>';
						desc +=  '<tr><td>' + "Documents :" + '</td></tr>';
						desc += '<tr><td>'+myEntities[i].Data[j].folder+ '</td></tr>';
						var nfiles = myEntities[i].Data[j].fileData.length;
						for(var l=0; l<nfiles; l++){
							var url = myEntities[i].Data[j].fileData[l].Url;
							desc += '<tr><td><a href ='+url +' target =_blank">'+ myEntities[i].Data[j].fileData[l].fileName +'</a></td></tr>';
						};
						desc += '</tbody></table>';
						entitiesArray[i].description = desc;
						break;
				};
				j++;
			};
			flagFolder = false;
		};
	};
});


// Check every 50 ms whether Cesium is loaded or not
// function waitUntilCesiumLoaded(callback) {
//     if (typeof(isCesiumLoaded) == "undefined")
//         window.setTimeout(function() { waitUntilCesiumLoaded(callback); }, 50);
//     else
//         callback();
// }

function waitUntilToolLoaded(callback){
	if(typeof($(".bim-toolbar-tool")[0])=="undefined")
		window.setTimeout(function(){ waitUntilToolLoaded(callback);}, 50);
	else {
		callback();
	}
}

waitUntilToolLoaded(function(){
	var bim_tool_vertical = $(".bim-toolbar-horizontal")[0];
	var bi_tool = document.createElement("div");
	bi_tool.className = "bim-toolbar-tool";
	bim_tool_vertical.appendChild(bi_tool);
	bi_tool.addEventListener("click",toggle_biframe)
	function toggle_biframe(){
		let x = document.getElementsByClassName("iframepopup")[0]
		x.style.display = x.style.display == "block"? "none":"block";
	}
  var info_tool = document.createElement("div");
  info_tool.class="bim-toolbar-tool";
  bim_tool_vertical.appendChild(info_tool);
  info_tool.addEventListener("click",toggle_info);
  function toggle_info(){
    let x = document.getElementById("infoPopup");
    x.style.display = x.style.display == "block"? "none":"block";
  }

		// var tool = '<div class="bim-toolbar-tool"><i class="bim-icon-windowarea"></i></div>'
})

// Callback to handle errors and success
var callback = function(userError, detailedError) {
    if (Cesium.defined(userError)) {
        var message = userError;
        if (typeof(detailedError) == 'string') {
            message = message.concat(': ', detailedError);
        }
        document.getElementById('loading').style.display = 'none';
        document.getElementById('message').textContent = message;

    } else {
        startupDiv.style.display = 'none';
        cesiumDiv.style.display = 'block';
    }
};

// Wait until Cesium is loaded
// waitUntilCesiumLoaded(function() {
//
//     // Make sure that the requirejs config is loaded before using it
//     requirejs(['scripts/index'], function() {
//
//         // Load the main script and run it
//         requirejs(['scripts/App/App'], function(App) {
//
//             App.main.run('cesiumDiv', callback);
//         });
//     });
//
// });
//alert (token);
	$(document).ready(function(){
    $.get("powerbi/powerbi_login.php",function(response){
      console.log(response)
      var token = response;
      var models = window['powerbi-client'].models;
      var permissions = models.Permissions.All;

      var embedConfiguration = {
          type: 'report',
          id:"b37d3739-02bc-4e4d-9c5c-7fdc5ffcf824",
          permissions: permissions,
          embedUrl: "https://app.powerbi.com/reportEmbed?reportId=b37d3739-02bc-4e4d-9c5c-7fdc5ffcf824&groupId=d4394867-790f-4f14-a4fe-b1246a4dfae0&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly9XQUJJLVNPVVRILUVBU1QtQVNJQS1yZWRpcmVjdC5hbmFseXNpcy53aW5kb3dzLm5ldCJ9",
          tokenType: models.TokenType.Embed,
          accessToken: token
      };
      var $reportContainer = $('#reportContainer');
      var report = powerbi.embed($reportContainer.get(0), embedConfiguration);
      //var myElement = $('#reportContainer');
      var myReport = powerbi.get($reportContainer.get(0));
      console.log(myReport);
    });
		// var getEmbedToken = "INSERT_YOUR_AZURE_FUNCTION_URL_HERE";
		//
		// $.ajax({
		//     url: getEmbedToken,
		//     jsonpCallback: 'callback',
		//     contentType: 'application/javascript',
		//     dataType: "jsonp",
		//     success: function (json) {
    $(".iframepopup").draggable();
    $("#infoPopup").draggable();
		$(".iframepopup").resizable({
			resize:function(event,ui){
					$('iframe').css({"height":ui.size.height,"width":ui.size.width});
			}
		});
    $("#infoPopup").resizable({
			resize:function(event,ui){
					$('#infoRootNode').css({"height":ui.size.height,"width":ui.size.width});
			}
		});
		});
